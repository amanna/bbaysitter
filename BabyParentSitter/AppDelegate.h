//
//  AppDelegate.h
//  BabySitterHawaii
//
//  Created by debashisandria on 13/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <FacebookSDK/FacebookSDK.h>
#define deviceTokenKey @"devtok"
id globalDataObject;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property(nonatomic)NSString *strUserId;
@property(nonatomic)NSString *strUserName;
@property(nonatomic)NSString *strUserImage;
@property(nonatomic)NSString *strUserType;

@end

