//
//  CDSideBarController.m
//  CDSideBar
//
//  Created by Christophe Dellac on 9/11/14.
//  Copyright (c) 2014 Christophe Dellac. All rights reserved.
//

#import "CDSideBarController.h"


@implementation CDSideBarController

@synthesize menuColor = _menuColor;
@synthesize isOpen = _isOpen;

#pragma mark - 
#pragma mark Init

- (CDSideBarController*)initWithImages:(NSArray*)images labelName:(NSArray*)name
{
    _menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _menuButton.frame = CGRectMake(0, 0, 35, 35);
    [_menuButton setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    [_menuButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    
    _backgroundMenuView = [[UIView alloc] init];
    _menuColor = [UIColor whiteColor];
    _buttonList = [[NSMutableArray alloc] initWithCapacity:images.count];
    
    _labelList = [[NSMutableArray alloc] initWithCapacity:name.count];
    
    int index = 0;
    for (UIImage *image in [images copy])
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:image forState:UIControlStateNormal];
        
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:12];
        label.text = name[index];
        label.textColor = [UIColor whiteColor];

        if ([UIScreen mainScreen].bounds.size.height == 568)
        {
            button.frame = CGRectMake(30, 20 + (110 * index), 50, 50);
            label.frame = CGRectMake(20, button.frame.size.height+20 + (110 * index), 80, 20);
        }
        else if ([UIScreen mainScreen].bounds.size.height > 660.0 && [UIScreen mainScreen].bounds.size.height < 720.0)
        {
            button.frame = CGRectMake(80, 20 + (130 * index), 70, 70);
            label.frame = CGRectMake(70, button.frame.size.height+20 + (130 * index), 80, 20);
        }
        
        else if ([UIScreen mainScreen].bounds.size.height > 720.0)
        {
            button.frame = CGRectMake(115, 20 + (140 * index), 70, 80);
            label.frame = CGRectMake(110, button.frame.size.height+20 + (140 * index), 80, 20);
        }
        
        else
        {
            button.frame = CGRectMake(20, 20 + (95 * index), 50, 50);
            label.frame = CGRectMake(10, button.frame.size.height+20 + (95 * index), 80, 20);
        }
        
        button.tag = index;
        [button addTarget:self action:@selector(onMenuButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        label.textAlignment = NSTextAlignmentCenter;
        label.backgroundColor = [UIColor clearColor];
        [_buttonList addObject:button];
        [_labelList addObject:label];
        ++index;
    }
    return self;
}
- (void)insertMenuButtonOnView:(UIView*)view atPosition:(CGPoint)position
{
    _menuButton.frame = CGRectMake(position.x, position.y, _menuButton.frame.size.width, _menuButton.frame.size.height);
    [view addSubview:_menuButton];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissMenu)];
    singleTap.cancelsTouchesInView = NO;
    [view addGestureRecognizer:singleTap];
    
    for (UIButton *button in _buttonList)
    {
        [_backgroundMenuView addSubview:button];
    }
    
    for (UILabel *label in _labelList)
    {
        [_backgroundMenuView addSubview:label];
    }
    
    _backgroundMenuView.frame = CGRectMake(210-view.frame.size.width, 0, view.frame.size.width-210, view.frame.size.height);
    _backgroundMenuView.backgroundColor = [UIColor colorWithRed:(125/255.0) green:(29/255.0) blue:(227/255.0) alpha:1] ;
    [view addSubview:_backgroundMenuView];
}



- (void)deleteButton:(UIView*)view atPosition:(CGPoint)position
{
    for ( id subview in view.subviews) {
        if ( [subview isKindOfClass:[UIButton class]] ) {
            //do your code
            UIButton *b = (UIButton*)subview;
            [b removeFromSuperview];
        }
    }
    
    
//    _menuButton.frame = CGRectMake(position.x, position.y, _menuButton.frame.size.width, _menuButton.frame.size.height);
//    [view addSubview:_menuButton];
//    
//    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissMenu)];
//    singleTap.cancelsTouchesInView = NO;
//    [view addGestureRecognizer:singleTap];
//    
//    for (UIButton *button in _buttonList)
//    {
//        [_backgroundMenuView addSubview:button];
//    }
//
//    for (UILabel *label in _labelList)
//    {
//        [_backgroundMenuView addSubview:label];
//    }
//
//    _backgroundMenuView.frame = CGRectMake(210-view.frame.size.width, 0, view.frame.size.width-210, view.frame.size.height);
//    _backgroundMenuView.backgroundColor = [UIColor colorWithRed:(125/255.0) green:(29/255.0) blue:(227/255.0) alpha:1] ;
//    [view addSubview:_backgroundMenuView];
}

#pragma mark - 
#pragma mark Menu button action

- (void)dismissMenuWithSelection:(UIButton*)button
{
    [UIView animateWithDuration:0.3f
                          delay:0.0f
         usingSpringWithDamping:.2f
          initialSpringVelocity:10.f
                        options:0 animations:^{
                            button.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2, 1.2);
                        }
                     completion:^(BOOL finished) {
                         [self dismissMenu];
                     }];
}

- (void)dismissMenu
{
    if (_isOpen)
    {
        _isOpen = !_isOpen;
       [self performDismissAnimation];
    }
}

- (void)showMenu
{
    if (!_isOpen)
    {
        _isOpen = !_isOpen;
        [self performSelectorInBackground:@selector(performOpenAnimation) withObject:nil];
    }
}

- (void)onMenuButtonClick:(UIButton*)button
{
    if ([self.delegate respondsToSelector:@selector(menuButtonClicked:)])
        [self.delegate menuButtonClicked:button.tag];
    [self dismissMenuWithSelection:button];
}

#pragma mark -
#pragma mark - Animations

- (void)performDismissAnimation
{
    [UIView animateWithDuration:0.4 animations:^{
        _menuButton.alpha = 1.0f;
        _menuButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, 0);
        _backgroundMenuView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, 0);
    }];
}

- (void)performOpenAnimation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.4 animations:^{
            _menuButton.alpha = 0.0f;
            _menuButton.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 100, 0);
            _backgroundMenuView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 100, 0);
        }];
    });
    for (UIButton *button in _buttonList)
    {
        [NSThread sleepForTimeInterval:0.02f];
        dispatch_async(dispatch_get_main_queue(), ^{
            button.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 20, 0);
            [UIView animateWithDuration:0.3f
                                  delay:0.3f
                 usingSpringWithDamping:.3f
                  initialSpringVelocity:10.f
                                options:0 animations:^{
                                    button.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, 0);
                                }
                             completion:^(BOOL finished) {
                             }];
        });
    }
}

@end

// Copyright belongs to original author
// http://code4app.net (en) http://code4app.com (cn)
// From the most professional code share website: Code4App.net
