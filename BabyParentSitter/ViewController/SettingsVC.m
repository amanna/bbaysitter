//
//  SettingsVC.m
//  BabyParentSitter
//
//  Created by Amrita on 28/05/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "SettingsVC.h"
#import "TextNotificationVC.h"
#import "AccountVC.h"
#import "ContactVC.h"
#import "ShareCareVC.h"
#import "LegalVC.h"
#import "PrivacyVC.h"
#import "HomeViewController.h"
#import "CDSideBarController.h"
@interface SettingsVC ()<CDSideBarControllerDelegate>{
    NSArray *images;
    NSArray *labelText;
    CDSideBarController *sideBar;
    NSArray *nameList;
}
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *topLabelText;


@property (weak, nonatomic) IBOutlet UIImageView *userImageview;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end

@implementation SettingsVC

- (void)viewDidLoad {
     self.navigationItem.hidesBackButton = YES;
    images = [NSArray arrayWithObjects:@"SmartPhone",@"UserIcon",@"Key",@"Share",@"Chat",@"Legal",@"Power", nil];
    labelText = [NSArray arrayWithObjects:@"Text Notifications",@"Your Account",@"Your Privacy Settings",@"Sharing is Caring!",@"Contact Us",@"Legal",@"Logout", nil];
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Settings";
    
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    nameList = @[@"John Doe", @"Sitter", @"My Jobs",@"My Settings", @"Messages"];
    
    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
    sideBar.delegate = self;
    [super viewDidLoad];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
   [sideBar insertMenuButtonOnView:self.navigationController.view atPosition: CGPointMake(15, 25)];
   
}
- (void)viewDidDisappear:(BOOL)animated{
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [labelText count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    }
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    UILabel *label = (UILabel *)[cell viewWithTag:101];
    imageView.image = [UIImage imageNamed:images[indexPath.row]];
    label.text = labelText[indexPath.row];
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row ==0){
       
        [self performSegueWithIdentifier:@"segueNotification" sender:self];
    }else if (indexPath.row ==1){
        [self performSegueWithIdentifier:@"segueAccount" sender:self];
    }else if (indexPath.row ==2){
        [self performSegueWithIdentifier:@"segueprivacy" sender:self];
    }else if (indexPath.row==3){
        [self performSegueWithIdentifier:@"segueShareCare" sender:self];
    }else if (indexPath.row==4){
        [self performSegueWithIdentifier:@"segueConatct" sender:self];
    }else if (indexPath.row==5){
         [self performSegueWithIdentifier:@"seguelegal" sender:self];
        
    }else if (indexPath.row==6){
        NSArray *VCs = [self.navigationController viewControllers];
        for (UIViewController *VC in VCs)
        {
            if ([VC isKindOfClass:[HomeViewController class]]) {
                [self.navigationController popToViewController:VC animated:NO];
            }
        }
    }
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
