//
//  AvailabilityCell.h
//  BabySitterHawaii
//
//  Created by Amrita on 20/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvailabilityCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
