//
//  HeartCell.h
//  BabyParentSitter
//
//  Created by Amrita on 05/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeartCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgheart;
@property (weak, nonatomic) IBOutlet UILabel *lblheartTitle;

@end
