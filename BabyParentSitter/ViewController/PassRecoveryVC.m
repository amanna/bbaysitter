//
//  PassRecoveryVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "PassRecoveryVC.h"

@interface PassRecoveryVC ()
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
- (IBAction)btnSubmitAction:(id)sender;


@end

@implementation PassRecoveryVC

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.title = @"Forgot Password?";
    [self.txtEmail becomeFirstResponder];
     self.txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSubmitAction:(id)sender {
    
    [self performSegueWithIdentifier:@"segueAddr" sender:self];
}
@end
