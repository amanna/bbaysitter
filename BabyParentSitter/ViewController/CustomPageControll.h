//
//  CustomPageControll.h
//  Sitter
//
//  Created by debashisandria on 13/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomPageControll : UIPageControl {

    UIImage* activeImage;
    UIImage* inactiveImage;
}

@end
