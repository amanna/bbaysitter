//
//  FindMatchCell.h
//  BabyParentSitter
//
//  Created by Amrita on 27/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FindMatchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblJobTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblJobLoc;
@property (weak, nonatomic) IBOutlet UILabel *hourrate;
@property (weak, nonatomic) IBOutlet UILabel *lblJobType;


@end
