//
//  PopUpVC.h
//  BabySitterHawaii
//
//  Created by Amrita on 05/05/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypePop{
   kActionOk=0,
   kActionCancel=1
}eventPop;
typedef void (^EventCompletionHandler)(id object,NSUInteger eventPop);
@interface PopUpVC : UIViewController{
    EventCompletionHandler handler;
}
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandler)eventhandler;
@end
