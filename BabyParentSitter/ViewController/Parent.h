//
//  Parent.h
//  BabyParentSitter
//
//  Created by Amrita on 11/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parent : NSObject
@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strFname;
@property(nonatomic)NSString *strLname;
@property(nonatomic)NSString *strAddress;
@property(nonatomic)NSString *strKid;
@property(nonatomic)NSString *strProfileImg;
@end
