//
//  WelcomeViewController.h
//  BabySitterHawaii
//
//  Created by debashisandria on 17/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "CreatePasswordViewController.h"
@interface WelcomeViewController : UIViewController

@property (weak, nonatomic) NSString *image_url;
@property (weak, nonatomic) NSString *name;
@property (weak, nonatomic) NSString *age;
@property (weak, nonatomic) NSString *address;
@property (weak, nonatomic) NSString *gender;
@property (weak, nonatomic) NSString *email;
@property (weak, nonatomic) NSString *firstName;
@property (weak, nonatomic) NSString *lastName;
@property (weak, nonatomic) NSString *bday;
@property (weak, nonatomic) NSString *fbId;
@property (weak, nonatomic) IBOutlet UIImageView *profile_image;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lbldesc;
@property (strong,nonatomic)NSString *strUserType;


@end
