//
//  Msg.h
//  BabyParentSitter
//
//  Created by Amrita on 12/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Msg : NSObject
@property(nonatomic)NSString *strMsgId;
@property(nonatomic)NSString *strUname;
@property(nonatomic)NSString *strDesc;
@property(nonatomic)NSString *strProfileImg;
@end
