//
//  ParentJobs.h
//  BabyParentSitter
//
//  Created by Amrita on 27/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentJobs : UIViewController
@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strUserType;
@end
