//
//  ChatContactListVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "ChatContactListVC.h"
#import "ChatContactListCell.h"
#import "CDSideBarController.h"
#import <QuartzCore/QuartzCore.h>
#import "ParentProfileVC.h"
#import "DashboardVCParent.h"
#import "ParentJobs.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "AppDelegate.h"
#import "UtiltiyManager.h"
@interface ChatContactListVC ()<UITableViewDataSource,UITableViewDelegate,CDSideBarControllerDelegate>{
    CDSideBarController *sideBar;
    NSArray *nameList;
    POAcvityView *activity;
    AppDelegate *appdelegate;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@property(nonatomic)NSMutableArray *arrMsg;
@end

@implementation ChatContactListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.arrMsg = [[NSMutableArray alloc]init];
    self.navigationItem.hidesBackButton = YES;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"My Messages";
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    
    
    if([self.strUserType isEqualToString:@"parent"]){
        nameList = @[@"John Doe", @"Sitter", @"My Jobs",@"My Settings", @"Messages"];
    }else{
        nameList = @[@"John Doe", @"Find Jobs", @"Payments",@"My Settings", @"Messages"];
    }
    
    
    
    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
    sideBar.delegate = self;
    
    [activity showView];
    //call webservice
    if([UtiltiyManager isReachableToInternet]){
    if([appdelegate.strUserType isEqualToString:@"Parent"]){
        [AppServiceManager getMsgByParent:appdelegate.strUserId onCompletion:^(id object, NSError *error) {
            [activity hideView];
            if(object){
                self.arrMsg = object;
                [self.tblView reloadData];
            }else{
                [UtiltiyManager showAlertWithMessage:@"Some problem Occusrs!! Please try again later"];
            }
        }];
        
    }else{
        [AppServiceManager getMsgBySitter:appdelegate.strUserId onCompletion:^(id object, NSError *error) {
             [activity hideView];
            if(object){
                self.arrMsg = object;
                [self.tblView reloadData];
            }else{
                [UtiltiyManager showAlertWithMessage:@"No Internet conncetion"];
            }
        }];
        
    }
    }else{
        [UtiltiyManager showAlertWithMessage:@"No Internet conncetion"];
    }
    
    
    
    // Do any additional setup after loading the view.
}
- (void)menuButtonClicked:(int)index
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    if(index==1){
        DashboardVCParent *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVCParent"];
        dashboard.strUid = self.strUid;
        dashboard.segmentedControl.selectedSegmentIndex=1;
        [self showViewController:dashboard sender:self];
    }else if (index==2){
        ParentJobs *dashboard = [story instantiateViewControllerWithIdentifier:@"ParentJobs"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
    }else if (index==0){
        ParentProfileVC *dashboard = [story instantiateViewControllerWithIdentifier:@"ParentProfileVC"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
        //[self performSegueWithIdentifier:@"segueSitterProfileVC" sender:self];
    }else if (index==3){
        
    }else{
//        ChatContactListVC *dashboard = [story instantiateViewControllerWithIdentifier:@"ChatContactListVC"];
//        dashboard.strUid = self.strUid;
//        dashboard.strUserType
//        [self showViewController:dashboard sender:self];
        // [self performSegueWithIdentifier:@"segueChatMsg" sender:self];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrMsg.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier5 = @"Cell";
    ChatContactListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier5];
    cell.lblUnreadmsgcount.layer.cornerRadius=8;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
    singleTap.cancelsTouchesInView = NO;
    [cell addGestureRecognizer:singleTap];
    return cell;
   
}
- (void)singleTap{
    [self performSegueWithIdentifier:@"segueSendMsg" sender:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [sideBar insertMenuButtonOnView:self.navigationController.view atPosition: CGPointMake(15, 25)];
}
#pragma mark - CDSideBarController delegate



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
