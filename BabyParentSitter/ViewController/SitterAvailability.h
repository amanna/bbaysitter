//
//  SitterAvailability.h
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SitterAvailability : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *myScroll;

@property(nonatomic,strong)NSString *strUser;
@property (strong,nonatomic)NSString *strUserType;
@end
