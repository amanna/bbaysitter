//
//  RegisterSitterParent.m
//  BabySitterHawaii
//
//  Created by Amrita on 25/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "RegisterSitterParent.h"
#import "AppServiceManager.h"
#import "POAcvityView.h"
#import "UtiltiyManager.h"
#import "PopUpVC.h"
#import "AddrParentCountVC.h"
#import "AppDelegate.h"
#define kOFFSET_FOR_KEYBOARD 100.0
@interface RegisterSitterParent ()<UITextFieldDelegate>{
    BOOL genderFlag;
    POAcvityView *activity;
    AppDelegate *appdelegate;
    UITapGestureRecognizer *singleFingerTap;
}
@property (weak, nonatomic) IBOutlet UILabel *lblEmailValidation;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstValidation;
@property (weak, nonatomic) IBOutlet UILabel *lblLastValidation;
@property (weak, nonatomic) IBOutlet UILabel *lblDobValidation;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll;
@property(nonatomic)PopUpVC *popUp;

@property(nonatomic)BOOL isCapital;
@property(nonatomic)BOOL isLength;
@property(nonatomic)BOOL isSpcl;
@property(nonatomic)BOOL isNumber;
@property(nonatomic)NSInteger noOfGreenCount;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UIImageView *img4;
@property (weak, nonatomic) IBOutlet UIImageView *img3;
@property (weak, nonatomic) IBOutlet UIView *viewPass;


@end

@implementation RegisterSitterParent
- (void)viewDidDisappear:(BOOL)animated{
    singleFingerTap.cancelsTouchesInView = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewPass.hidden = YES;
    self.navigationController.navigationBarHidden = NO;
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.popUp = [main instantiateViewControllerWithIdentifier:@"PopUpVC"];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    if([self.strUserType isEqualToString:@"sitter"]){
        self.btnFemale.hidden = NO;
        self.btnMale.hidden = NO;

            }
    else{
//
        
        self.btnFemale.hidden = YES;
       self.btnMale.hidden = YES;
        
    }
        
    self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width, 1500);
    self.txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtFname.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtLName.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtBirth.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtPassword.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtEmail.leftPadding = 65.0;
     self.txtFname.leftPadding = 100.0;
    self.txtLName.leftPadding = 100.0;
    self.txtBirth.leftPadding = 90.0;
    self.txtPassword.leftPadding = 10.0;
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    
   
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
    
    self.navigationItem.title = @"Register";
 
   
   singleFingerTap =
   [[UITapGestureRecognizer alloc] initWithTarget:self
                                           action:@selector(handleSingleTap)];
    //singleFingerTap.cancelsTouchesInView = NO;
   [self.navigationController.view addGestureRecognizer:singleFingerTap];

  //Add back menu
  UIImage *buttonImageleft = [UIImage imageNamed:@"backN"];
  UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
  [buttonLeft setImage:buttonImageleft forState:UIControlStateNormal];
  buttonLeft.frame = CGRectMake(0, 0, 45, 20);
  [buttonLeft addTarget:self action:@selector(leftMenuclicked) forControlEvents:UIControlEventTouchUpInside];
  UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonLeft];
  self.navigationItem.leftBarButtonItem = leftBarItem;


    // Do any additional setup after loading the view.
}
- (void)leftMenuclicked{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)handleSingleTap{
   
    [self.view endEditing:YES];
     [self setViewC];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnMaleAction:(id)sender {
    genderFlag = 1;
    [self toggleMaleFemale];

}
- (IBAction)btnFemaleAction:(id)sender {
    genderFlag = 0;
    [self toggleMaleFemale];
    
}
- (void)toggleMaleFemale {
    [self.btnMale setBackgroundImage:[UIImage imageNamed:@"male"] forState:UIControlStateNormal];
    [self.btnFemale setBackgroundImage:[UIImage imageNamed:@"female"] forState:UIControlStateNormal];
    
    if (genderFlag==0) {
      
        [self.btnFemale setBackgroundImage:[UIImage imageNamed:@"female_checked"] forState:UIControlStateNormal];
    } else {
       [self.btnMale setBackgroundImage:[UIImage imageNamed:@"male_checked"] forState:UIControlStateNormal];
    }
}
- (void)convetdateToString{
   // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [dateFormat dateFromString:self.txtBirth.text];
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    self.strDob = [dateFormat1 stringFromDate:date];
    

}
- (void)methodRegistration{
    singleFingerTap.cancelsTouchesInView = NO;
    if ([self validData]){
        if(![self.txtBirth.text isEqualToString:@""]){
            [self convetdateToString];
        }
        [activity showView];
        if([self.strUserType isEqualToString:@"parent"]){
            [AppServiceManager registrationParent:self.txtEmail.text andFirstname:self.txtFname.text andLastname:self.txtLName.text andPassword:self.txtPassword.text andDob:self.strDob onCompletion:^(id object, NSError *error) {
                [activity hideView];
                if(object){
                    NSDictionary *dict = object;
                    NSInteger success =[[dict valueForKey:@"success"]integerValue];
                    if(success==1){
                        [self.view addSubview:self.popUp.view];
                        self.strUid = [dict valueForKey:@"userid"];
                        //set Appdelegate var
                        
                        appdelegate.strUserId = [dict valueForKey:@"userid"];
                        appdelegate.strUserName = [NSString stringWithFormat:@"%@ %@",self.txtFname.text,self.txtLName.text];
                       
                        appdelegate.strUserType = @"Parent";
                        [self.popUp refreshUIWithDatasource:nil onCompletion:^(id object, NSUInteger eventPop) {
                            if(eventPop==kActionOk){
                                [self.popUp.view removeFromSuperview];
                                [self performSegueWithIdentifier:@"mapsegue" sender:self];
                            }else{
                                [self.popUp.view removeFromSuperview];
                                
                            }
                        }];
                    }else if (success==2 || success==0){
                        [self.txtEmail becomeFirstResponder];
                        [self.txtEmail setKeyboardType:UIKeyboardTypeDefault];
                        self.lblEmailValidation.hidden=NO;
                        self.lblEmailValidation.text = @"This Email is already in use";
                        self.txtEmail.background =[UIImage imageNamed:@"email_selectedwithouttext"];
                    }
                    
                }else{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some problem occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
                
            }];
        }else{
            NSString *strGender;
            if(genderFlag==1){
                strGender = @"male";
            }else{
                strGender = @"female";
            }
            [AppServiceManager registrationSitter:self.txtEmail.text andFirstname:self.txtFname.text andLastname:self.txtLName.text andPassword:self.txtPassword.text andDob:self.strDob  andGender:strGender onCompletion:^(id object, NSError *error) {
                [activity hideView];
                if(object){
                    NSDictionary *dict = object;
                    NSInteger success =[[dict valueForKey:@"success"]integerValue];
                    if(success==1){
                        //Pop Up appear
                        [self.view addSubview:self.popUp.view];
                        self.strUid = [dict valueForKey:@"userid"];
                        
                        appdelegate.strUserId = [dict valueForKey:@"userid"];
                        appdelegate.strUserName = [NSString stringWithFormat:@"%@ %@",self.txtFname.text,self.txtLName.text];
                        
                        appdelegate.strUserType = @"Sitter";

                        
                        [self.popUp refreshUIWithDatasource:nil onCompletion:^(id object, NSUInteger eventPop) {
                            if(eventPop==kActionOk){
                                [self.popUp.view removeFromSuperview];
                                [self performSegueWithIdentifier:@"mapsegue" sender:self];
                            }else{
                                [self.popUp.view removeFromSuperview];
                                
                            }
                        }];
                    }else if (success==0 || success==2){
                        [self.txtEmail becomeFirstResponder];
                        [self.txtEmail setKeyboardType:UIKeyboardTypeDefault];
                        self.lblEmailValidation.hidden=NO;
                        self.lblEmailValidation.text = @"This Email is already in use";
                        self.txtEmail.background =[UIImage imageNamed:@"email_selectedwithouttext"];
                    }
                    
                }else{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some problem occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
                
            }];
        }
        
    }

}
- (IBAction)btnNextAction:(id)sender {
    [self methodRegistration];
//    self.strUid =@"1";
//     [self performSegueWithIdentifier:@"mapsegue" sender:self];
   //
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
   if ([segue.identifier isEqualToString:@"mapsegue"]) {
       AddrParentCountVC *rs =segue.destinationViewController;
       rs.strUserType = self.strUserType;
       rs.strUid = self.strUid;
   }
}


-(void)viewDidAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.myView.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.myView.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.myView.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.myView.frame.origin.y < 0)
    {
        [self setViewMovedUp:YES];
    }
}

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.myView.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.myView.frame = rect;
    
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    NSLog(@"%f",sender.frame.origin.y);
    UITextField *txt1 = (UITextField*)sender;
    if(txt1 == self.txtBirth){
        //[txt1 resignFirstResponder];
        UIDatePicker *datePicker = [[UIDatePicker alloc] init];
        
        datePicker.datePickerMode = UIDatePickerModeDate;
        //datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-31536000];
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        //[datePicker setDate:[NSDate date]];
        //self.txtBirth.text = [self formatDate:datePicker.date];
        self.txtBirth.inputView = datePicker;
    }
}
// Called when the date picker changes.
- (void)updateDateField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)self.txtBirth.inputView;
    self.txtBirth.text = [self formatDate:picker.date];
}


// Formats the date chosen with the date picker.
- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField==self.txtEmail){
       // [self emailValidation];
        [self.txtFname becomeFirstResponder];
        [self.txtEmail resignFirstResponder];
    }else if (textField==self.txtPassword){
        [self.txtPassword resignFirstResponder];
        self.noOfGreenCount = 0;
        NSString *specialCharacterString1 = @"0123456789";
        NSCharacterSet *specialCharacterSet1 = [NSCharacterSet
                                                characterSetWithCharactersInString:specialCharacterString1];
        
        if ([textField.text rangeOfCharacterFromSet:specialCharacterSet1].length) {
            self.isNumber = YES;
            self.noOfGreenCount = self.noOfGreenCount + 1;
        }else{
            self.isNumber = NO;
        }
        
        
        NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
        NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                               characterSetWithCharactersInString:specialCharacterString];
        
        if ([textField.text.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
            NSLog(@"contains special characters");
            self.isSpcl = YES;
            self.noOfGreenCount = self.noOfGreenCount + 1;
        }else{
            self.isSpcl = NO;
        }
        
        
        NSString *specialCharacterString2 = @"ABCDEFGHIJKLKMNOPQRSTUVWXYZ";
        NSCharacterSet *specialCharacterSet2 = [NSCharacterSet
                                                characterSetWithCharactersInString:specialCharacterString2];
        if ([textField.text rangeOfCharacterFromSet:specialCharacterSet2].length){
            self.isCapital = YES;
            self.noOfGreenCount = self.noOfGreenCount + 1;
        }else{
            self.isCapital = NO;
        }
        if(textField.text.length >=6){
            self.isLength = YES;
            self.noOfGreenCount = self.noOfGreenCount + 1;
        }else{
            self.isLength = NO;
        }
        NSLog(@"Green=%d",self.noOfGreenCount);
        [self colorImage];

        [self setViewMovedUp:YES];
    }else if (textField==self.txtFname){
      
        [self.txtFname resignFirstResponder];
        [self.txtLName becomeFirstResponder];
        [self setViewMovedUp:YES];
    }else if (textField==self.txtLName){
        [self.txtLName resignFirstResponder];
        [self.txtBirth becomeFirstResponder];
        [self setViewMovedUp:YES];
       
    }else{
        [self.txtBirth resignFirstResponder];
        [self.txtPassword becomeFirstResponder];
       
        
    }
    
    return true;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField == self.txtPassword){
    if(textField.text.length==0){
        self.noOfGreenCount = 0;
        NSString *specialCharacterString1 = @"0123456789";
        NSCharacterSet *specialCharacterSet1 = [NSCharacterSet
                                                characterSetWithCharactersInString:specialCharacterString1];
        
        if ([string rangeOfCharacterFromSet:specialCharacterSet1].length) {
            self.isNumber = YES;
            self.noOfGreenCount = self.noOfGreenCount + 1;
        }else{
            self.isNumber = NO;
        }

        NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
        NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                               characterSetWithCharactersInString:specialCharacterString];
        
        if ([string.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
            NSLog(@"contains special characters");
            self.isSpcl = YES;
            self.noOfGreenCount = self.noOfGreenCount + 1;
        }else{
            self.isSpcl = NO;
        }
        
        
        NSString *specialCharacterString2 = @"ABCDEFGHIJKLKMNOPQRSTUVWXYZ";
        NSCharacterSet *specialCharacterSet2 = [NSCharacterSet
                                                characterSetWithCharactersInString:specialCharacterString2];
        if ([textField.text rangeOfCharacterFromSet:specialCharacterSet2].length){
            self.isCapital = YES;
            self.noOfGreenCount = self.noOfGreenCount + 1;
        }else{
            self.isCapital = NO;
        }
        if(string.length >=6){
            self.isLength = YES;
            self.noOfGreenCount = self.noOfGreenCount + 1;
        }else{
            self.isLength = NO;
        }
        NSLog(@"Green=%d",self.noOfGreenCount);
        [self colorImage];

    }else{
        self.noOfGreenCount = 0;
        NSString *specialCharacterString1 = @"0123456789";
        NSCharacterSet *specialCharacterSet1 = [NSCharacterSet
                                               characterSetWithCharactersInString:specialCharacterString1];

        if ([textField.text rangeOfCharacterFromSet:specialCharacterSet1].length) {
            self.isNumber = YES;
            self.noOfGreenCount = self.noOfGreenCount + 1;
        }else{
             self.isNumber = NO;
        }
   
       
    NSString *specialCharacterString = @"!~`@#$%^&*-+();:={}[],.<>?\\/\"\'";
    NSCharacterSet *specialCharacterSet = [NSCharacterSet
                                           characterSetWithCharactersInString:specialCharacterString];
    
    if ([textField.text.lowercaseString rangeOfCharacterFromSet:specialCharacterSet].length) {
        NSLog(@"contains special characters");
        self.isSpcl = YES;
        self.noOfGreenCount = self.noOfGreenCount + 1;
    }else{
        self.isSpcl = NO;
    }
    
    
    NSString *specialCharacterString2 = @"ABCDEFGHIJKLKMNOPQRSTUVWXYZ";
    NSCharacterSet *specialCharacterSet2 = [NSCharacterSet
                                                characterSetWithCharactersInString:specialCharacterString2];
    if ([textField.text rangeOfCharacterFromSet:specialCharacterSet2].length){
        self.isCapital = YES;
        self.noOfGreenCount = self.noOfGreenCount + 1;
    }else{
        self.isCapital = NO;
    }
    if(textField.text.length >=6){
        self.isLength = YES;
        self.noOfGreenCount = self.noOfGreenCount + 1;
    }else{
         self.isLength = NO;
    }
    NSLog(@"Green=%d",self.noOfGreenCount);
    [self colorImage];
    }
    }
    return true;
}
- (void)colorImage{
    self.viewPass.hidden = NO;
   // pw_strenght_green
    if(self.noOfGreenCount==1){
        self.img1.image = [UIImage imageNamed:@"pw_strenght_green"];
        self.img2.image = [UIImage imageNamed:@"pw_strenght_grey"];
        self.img3.image = [UIImage imageNamed:@"pw_strenght_grey"];
        self.img4.image = [UIImage imageNamed:@"pw_strenght_grey"];

    }else if(self.noOfGreenCount==2){
        self.img1.image = [UIImage imageNamed:@"pw_strenght_green"];
        self.img2.image = [UIImage imageNamed:@"pw_strenght_green"];
        self.img3.image = [UIImage imageNamed:@"pw_strenght_grey"];
        self.img4.image = [UIImage imageNamed:@"pw_strenght_grey"];
    }else if(self.noOfGreenCount==3){
         self.img1.image = [UIImage imageNamed:@"pw_strenght_green"];
         self.img2.image = [UIImage imageNamed:@"pw_strenght_green"];
         self.img3.image = [UIImage imageNamed:@"pw_strenght_green"];
         self.img4.image = [UIImage imageNamed:@"pw_strenght_grey"];
        
    }else if(self.noOfGreenCount==4){
        self.img1.image = [UIImage imageNamed:@"pw_strenght_green"];
         self.img2.image = [UIImage imageNamed:@"pw_strenght_green"];
         self.img3.image = [UIImage imageNamed:@"pw_strenght_green"];
         self.img4.image = [UIImage imageNamed:@"pw_strenght_green"];
    }
}
-(BOOL) validData
{
    BOOL isValid = NO;
    if([self.txtEmail.text isEqualToString:@""]){
        [self.txtEmail becomeFirstResponder];
        [self.txtEmail setKeyboardType:UIKeyboardTypeDefault];
        self.lblEmailValidation.hidden=NO;
        self.lblEmailValidation.text = @"Please enter Email";
      //  self.txtEmail.background =[UIImage imageNamed:@"email_selectedwithouttext"];
    }else if ([self NSStringIsValidEmail:self.txtEmail.text]==NO){
        [self.txtEmail becomeFirstResponder];
        [self.txtEmail setKeyboardType:UIKeyboardTypeDefault];
        self.lblEmailValidation.hidden=NO;
        self.lblEmailValidation.text = @"Email Format is not right";
       // self.txtEmail.background =[UIImage imageNamed:@"email_selectedwithouttext"];
    }else if(self.txtPassword.text.length < 6){
        [self.txtPassword becomeFirstResponder];
        [self.txtPassword setKeyboardType:UIKeyboardTypeDefault];
        self.lblEmailValidation.hidden=YES;
        self.txtPassword.background =[UIImage imageNamed:@"email_selectedwithouttext"];
       //  self.txtEmail.background =[UIImage imageNamed:@"email"];
    }else if([self.txtFname.text isEqualToString:@""]){
        [self.txtFname becomeFirstResponder];
        [self.txtFname setKeyboardType:UIKeyboardTypeDefault];
        self.lblEmailValidation.hidden=YES;
        self.lblFirstValidation.hidden=NO;
        self.lblFirstValidation.text = @"Please enter First Name";
        self.txtFname.background =[UIImage imageNamed:@"email_selectedwithouttext"];
        self.txtPassword.background =[UIImage imageNamed:@"email"];
      //  self.txtEmail.background =[UIImage imageNamed:@"email"];

    }else if([self.txtLName.text isEqualToString:@""]){
        [self.txtLName becomeFirstResponder];
        [self.txtLName setKeyboardType:UIKeyboardTypeDefault];
        self.lblEmailValidation.hidden=YES;
        self.lblFirstValidation.hidden=YES;
        self.lblLastValidation.hidden=NO;
        self.lblLastValidation.text = @"Please enter Last Name";
        self.txtLName.background =[UIImage imageNamed:@"email_selectedwithouttext"];
        self.txtFname.background =[UIImage imageNamed:@"email"];
        self.txtPassword.background =[UIImage imageNamed:@"email"];
       // self.txtEmail.background =[UIImage imageNamed:@"email"];
    }else if([self.txtBirth.text isEqualToString:@""]){
        [self.txtBirth becomeFirstResponder];
        [self.txtBirth setKeyboardType:UIKeyboardTypeDefault];
        self.lblEmailValidation.hidden=YES;
        self.lblFirstValidation.hidden=YES;
        self.lblLastValidation.hidden=YES;
        self.lblDobValidation.hidden=NO;
        self.lblDobValidation.text = @"Please enter Birthday";
        self.txtBirth.background =[UIImage imageNamed:@"email_selectedwithouttext"];
        self.txtLName.background =[UIImage imageNamed:@"email"];
        self.txtFname.background =[UIImage imageNamed:@"email"];
        self.txtPassword.background =[UIImage imageNamed:@"email"];
      //  self.txtEmail.background =[UIImage imageNamed:@"email"];
    }else if ([self isTheStringDate:self.txtBirth.text]==NO){
        [self.txtBirth becomeFirstResponder];
        [self.txtBirth setKeyboardType:UIKeyboardTypeDefault];
        self.lblEmailValidation.hidden=YES;
        self.lblFirstValidation.hidden=YES;
        self.lblLastValidation.hidden=YES;
        self.lblDobValidation.hidden=NO;
        self.lblDobValidation.text = @"Please enter Birthday in MM/dd/yyyy format";
        self.txtBirth.background =[UIImage imageNamed:@"email_selectedwithouttext"];
        self.txtLName.background =[UIImage imageNamed:@"email"];
        self.txtFname.background =[UIImage imageNamed:@"email"];
        self.txtPassword.background =[UIImage imageNamed:@"email"];
      //  self.txtEmail.background =[UIImage imageNamed:@"email"];

    }else{
        return YES;
    }
    
    return isValid;
}
- (BOOL) isTheStringDate: (NSString*) theString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:theString];
    
    if (dateFromString !=nil) {
        return YES;
    }
    else {
        return NO;
    }
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (void)setViewC{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    CGRect rect = self.myView.frame;
    rect.origin.y = 0;
    self.myView.frame = rect;
    [UIView commitAnimations];
}
- (IBAction)btnTermAction:(id)sender {
    [self performSegueWithIdentifier:@"segueTermOfUse" sender:self];
}

- (IBAction)btnPrivacyPolicyAction:(id)sender {
    [self performSegueWithIdentifier:@"seguePolicy" sender:self];
}
@end
