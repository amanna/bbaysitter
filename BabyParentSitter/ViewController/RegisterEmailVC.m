//
//  RegisterEmailVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 25/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "RegisterEmailVC.h"
#import "RegisterSitterParent.h"
@interface RegisterEmailVC ()
- (IBAction)btnSitterClickedAction:(id)sender;
- (IBAction)btnParentClickedaction:(id)sender;
- (IBAction)btnBackAction:(id)sender;

@end

@implementation RegisterEmailVC

- (void)viewDidLoad {
    [super viewDidLoad];
     self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = YES;
    

    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSitterClickedAction:(id)sender {
      [self performSegueWithIdentifier:@"segueSitter" sender:self];
}

- (IBAction)btnParentClickedaction:(id)sender {
   
    [self performSegueWithIdentifier:@"segueParent" sender:self];
}

- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
   if ([segue.identifier isEqualToString:@"segueSitter"]) {
       RegisterSitterParent *rs =segue.destinationViewController;
       rs.strUserType = @"sitter";
   } else if ([segue.identifier isEqualToString:@"segueParent"]) {
       RegisterSitterParent *rs =segue.destinationViewController;
       rs.strUserType = @"parent";
   }
}
@end
