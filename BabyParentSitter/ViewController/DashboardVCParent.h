//
//  DashboardVCParent.h
//  BabyParentSitter
//
//  Created by Amrita on 05/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardVCParent : UIViewController
@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strUserType;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@end
