//
//  JobDetailsVC.h
//  BabyParentSitter
//
//  Created by Amrita on 11/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface JobDetailsVC : UIViewController<MKMapViewDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UIScrollView *aScrollView;
@property (strong, nonatomic) IBOutlet MKMapView *mapview;
@property (weak, nonatomic) IBOutlet UIImageView *profilepic;
@property (weak, nonatomic) IBOutlet UILabel *lblMileshow;
@property (weak, nonatomic) IBOutlet UIView *viewMilesShow;
@property (weak, nonatomic) IBOutlet UILabel *jobname;
@property (weak, nonatomic) IBOutlet UILabel *jobtime;

@property (weak, nonatomic) IBOutlet UILabel *lblprice;

@property (weak, nonatomic) IBOutlet UILabel *lblkids;
@property (weak, nonatomic) IBOutlet UITextView *jobdetailstextview;

@property (weak, nonatomic) IBOutlet UITableView *tblPreferences;
@property (weak, nonatomic) IBOutlet UITableView *tblAge;
- (IBAction)btnReadmoreAction:(id)sender;
- (IBAction)btnApplyAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblLanguage;
@property (weak, nonatomic) IBOutlet UITableView *tblSpclCare;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *avilView;
@property (weak, nonatomic) IBOutlet UIView *topView;

@end
