//
//  SitterExperienceVC.h
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTextField.h"
@interface SitterExperienceVC : UIViewController
@property (weak, nonatomic) IBOutlet CTextField *txtEducation;
@property (weak, nonatomic) IBOutlet CTextField *lan;
@property(nonatomic,strong)NSString *strUser;
@property (strong,nonatomic)NSString *strUserType;
@end
