//
//  SitterProfileEditVC.m
//  BabyParentSitter
//
//  Created by Amrita on 26/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "SitterProfileEditVC.h"
#import "AppDelegate.h"
#import "SingleCell.h"
#import "CDSideBarController.h"
#import "Sitter.h"
#import "AppServiceManager.h"
#import "Reachability.h"
#import "UtiltiyManager.h"
@interface SitterProfileEditVC ()<UITableViewDataSource,UITableViewDelegate>{
    CDSideBarController *sideBar;
    NSArray *nameList;
    NSMutableArray*arrListParameters;
     
}
- (IBAction)btnUpdateAction:(UIButton *)sender;
- (IBAction)btnEditPicAction:(UIButton *)sender;
- (IBAction)btnEditBioAction:(UIButton *)sender;
- (IBAction)btnEditPreferenceAction:(UIButton *)sender;
- (IBAction)btnEditQualificationAction:(UIButton *)sender;
- (IBAction)btnEditLangAction:(UIButton *)sender;
- (IBAction)btnEditEducationAction:(UIButton *)sender;
@property(nonatomic)NSMutableDictionary *dict;
@end

@implementation SitterProfileEditVC
- (void)viewDidLoad {
    [super viewDidLoad];
    //create Update dictionary
    self.dict = [[NSMutableDictionary alloc]init];
    
    NSLog(@"This is the objecct for sitter::::::::::%@",self.sitter);
   NSLog(@"This is the various datas::::::::%@",self.sitter.strUid);
//    NSLog(@"This is the various datas::::::::%@",self.sitter.strUid);
//    NSLog(@"This is the various datas::::::::%@",self.sitter.strUid);
//    NSLog(@"This is the various datas::::::::%@",self.sitter.strUid);
//    NSLog(@"This is the various datas::::::::%@",self.sitter.strUid);
//
    arrListParameters=[[NSMutableArray alloc] init];
    
    
    [self.dict setObject:self.sitter.strUid forKey:@"siiter_id"];
    [self.dict setObject:self.sitter.strBackfgroundCheck forKey:@"background_check_complete"];
    [self.dict setObject:self.sitter.strDrivingComplete forKey:@"Driving_complete"];
    [self.dict setObject:self.sitter.strUid forKey:@"profile_image"];
    [self.dict setObject:self.sitter.strUid forKey:@"profile_video"];
    [self.dict setObject:self.sitter.strUid forKey:@"video_thumb_path"];
     [self.dict setObject:self.sitter.strAvailableText forKey:@"available_text"];
    [self.dict setObject:self.sitter.strAvailableBackUp forKey:@"available_backup"];
    
    [self.dict setObject:self.sitter.strFullTime forKey:@"fulltime"];
    
     [self.dict setObject:self.sitter.strNight forKey:@"nights"];
     [self.dict setObject:self.sitter.strWeekend forKey:@"weekends"];
     [self.dict setObject:self.sitter.strBforeSchll forKey:@"before_shool"];
     [self.dict setObject:self.sitter.strafterSchll forKey:@"after_school"];
     [self.dict setObject:self.sitter.strAcceptCredit forKey:@"accept_credit_card"];
     [self.dict setObject:self.sitter.strAcceptCash forKey:@"accept_cash"];
     [self.dict setObject:self.sitter.strAcceptPaypal forKey:@"accept_paypal"];
     [self.dict setObject:self.sitter.strDistanceTravel forKey:@"distence_willing_travel"];
     [self.dict setObject:self.sitter.strExperience forKey:@"experience_no_years"];
    
     [self.dict setObject:self.sitter.strNoOfChild forKey:@"no_of_children_handle"];
     //[self.dict setObject:self.sitter.stre forKey:@"education"];
     [self.dict setObject:self.sitter.strLang forKey:@"languages"];
     [self.dict setObject:self.sitter.strBio forKey:@"bio"];
     [self.dict setObject:self.sitter.strAcceptCheQue forKey:@"accept_personal_checks"];
    
    
    [self.dict setObject:self.sitter.strtransportable forKey:@"transportable"];
    [self.dict setObject:self.sitter.strNonsmoker forKey:@"non_smoker"];
    [self.dict setObject:self.sitter.strCareSickkid forKey:@"care_sick_kids"];
    [self.dict setObject:self.sitter.strComfortPet forKey:@"comfortable_with_pets"];
    [self.dict setObject:self.sitter.strToddlers forKey:@"tooders"];

    
    
    [self.dict setObject:self.sitter.strearlySchool forKey:@"early_school"];
    [self.dict setObject:self.sitter.strelementrySchool forKey:@"elementry_school"];
    [self.dict setObject:self.sitter.strTeens forKey:@"teens"];
    [self.dict setObject:self.sitter.strTweensOrMultiple forKey:@"tween_or_multiplechilds"];
    [self.dict setObject:self.sitter.strNoOfChild forKey:@"no_of_children_handle"];
    
    [self.dict setObject:self.sitter.strSpclNeed forKey:@"special_needs"];
    [self.dict setObject:self.sitter.strSchoolLabel forKey:@"school_lable"];
    [self.dict setObject:self.sitter.strHighschoolDegree forKey:@"high_school_degree"];
    
    [self.dict setObject:self.sitter.strCollegeLabel forKey:@"college_label"];
     [self.dict setObject:self.sitter.strCollegeDegree forKey:@"college_degree"];
    
     [self.dict setObject:self.sitter.strgraduateLabel forKey:@"graduate_label"];
    
     [self.dict setObject:self.sitter.strgraduatedegree forKey:@"graduate_degree"];
     [self.dict setObject:self.sitter.strcertified_teacher forKey:@"certified_teacher"];
     [self.dict setObject:self.sitter.strcpr_certified forKey:@"cpr_certtified"];
     [self.dict setObject:self.sitter.strFirstaid_train forKey:@"firstaid_train"];
     [self.dict setObject:self.sitter.strSpclNeedcare forKey:@"spcl_need_care"];
    
    //confudion
     [self.dict setObject:self.sitter.strUid forKey:@"extra_need"];
    
    NSLog(@"dict=%@",self.dict);
    NSString *strDict=@"";
    for (NSString *key in self.dict)
    {
        NSString *value = [self.dict objectForKey:key];
        NSString *strKeyVal = [NSString stringWithFormat:@"%@=%@&",key,value];
        strDict = [strDict stringByAppendingString:strKeyVal];
    }
    [self.dict setValue:@"123" forKey:@"extra_need"];
    self.navigationItem.hidesBackButton = YES;
    UIImage *buttonImageRight = [UIImage imageNamed:@"Message"];
    UIButton *buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonRight setImage:buttonImageRight forState:UIControlStateNormal];
    buttonRight.frame = CGRectMake(0, 0, buttonImageRight.size.width, buttonImageRight.size.height);
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonRight];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    nameList = @[@"John Doe", @"Find Jobs", @"Payments",@"My Settings", @"Messages"];
    
    
    
    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
    sideBar.delegate = self;
    
    //[self.dict objectForKey:@"non_smoker"]
    self.arrPreference = [[NSMutableArray alloc]initWithObjects:@"Willing to travel",@"Comfortable caring for",@"Has own transporation",@"Accepts credit cards",@"Non smoker",@"Will care for sick kids", @"Before school",@"After school",@"Weekend babysitting",@"Comfortable with pets",nil];
    
    arrListParameters=[[NSMutableArray alloc] initWithObjects:@"HEllo",@"WOrld",[self.dict objectForKey:@"transportable"],[self.dict objectForKey:@"accept_credit_card"],[self.dict objectForKey:@"non_smoker"],[self.dict objectForKey:@"care_sick_kids"],[self.dict objectForKey:@"before_shool"],[self.dict objectForKey:@"after_school"],[self.dict objectForKey:@"weekends"],[self.dict objectForKey:@"comfortable_with_pets"], nil];
    NSLog(@"THis is hte list of hte preferences::::::::%@",arrListParameters);
    
    self.arrExperience = [[NSMutableArray alloc]initWithObjects:@"Newborn (0-6 months)",@"Toddlers (7 mon-3 years)",@"Early School age(4-6 years)",@"Elementry School(7-11 years)",@"Pre-Teens/Teenagers(12- 15+years)",@"Special Needs", @"Twins/Multipules",nil];
    
    
    self.arrqualfication = [[NSMutableArray alloc]initWithObjects:@"Certified Teacher",@"CPR Certified",@"First Aid Training",@"Special Need care",nil];
    self.arrEducation = [[NSMutableArray alloc]initWithObjects:@"Some High School",@"High School degree",@"Some College",@"College Degree",@"Some graduate School",@"Graduate Degree",nil];
    // Do any additional setup after loading the view.
    //Tableview rounded corner
    
    self.tblpreference.layer.cornerRadius = 5;
    self.tblEducation.layer.cornerRadius = 5;
    self.tblExperience.layer.cornerRadius = 5;
    self.tblLang.layer.cornerRadius = 5;
    self.tblqualfication.layer.cornerRadius = 5;

    // Do any additional setup after loading the view.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [sideBar insertMenuButtonOnView:self.navigationController.view atPosition: CGPointMake(15, 25)];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag==100){
        return self.arrPreference.count;
    }else if (tableView.tag==101){
        return self.arrExperience.count;
    }else if (tableView.tag==102){
        return self.arrLang.count;
    }else if (tableView.tag==103){
        return self.arrqualfication.count;
    }else{
        return self.arrEducation.count;
    }
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag==100){
        SingleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.lblLeft.text =[self.arrPreference objectAtIndex:indexPath.row];
        if(indexPath.row ==0 ||indexPath.row ==1)
        {
            cell.lblRight.text = @"sfsffsf";
        }else{
               cell.lblRight.text = @"";
            cell.accessoryType=UITableViewCellAccessoryNone;
            if ([[arrListParameters objectAtIndex:indexPath.row]isEqualToString:@"1"])
            {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
           
        }
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        return cell;
    }else if (tableView.tag==101){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrExperience objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        return cell;
    }else if (tableView.tag==102){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrLang objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        return cell;
    }else if (tableView.tag==103){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrqualfication objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        return cell;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrEducation objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        return cell;
    }
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    if (tableView.tag==100)
    {
        if ([[arrListParameters objectAtIndex:indexPath.row] isEqualToString:@"1"])
        {
            [arrListParameters replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%@",@"0"]];
        }
        else if ([[arrListParameters objectAtIndex:indexPath.row] isEqualToString:@"0"])
        {
            [arrListParameters replaceObjectAtIndex:indexPath.row withObject:[NSString stringWithFormat:@"%@",@"1"]];
        }
        [tableView reloadData];

    }
    /*
    if(tableView.tag==100){
        SingleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.lblLeft.text =[self.arrPreference objectAtIndex:indexPath.row];
        if(indexPath.row ==0 ||indexPath.row ==1){
            cell.lblRight.text = @"sfsffsf";
            
        }else{
            cell.lblRight.text = @"";
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
    }else if (tableView.tag==101){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrExperience objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
     
      }
    else if (tableView.tag==102)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrLang objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else if (tableView.tag==103)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrqualfication objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrEducation objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
     */
    
}
- (IBAction)btnUpdateAction:(UIButton *)sender
{
    NSLog(@"dict=%@",self.dict);
    NSString *strDict=@"";
    for (NSString *key in self.dict)
    {
        NSString *value = [self.dict objectForKey:key];
        NSString *strKeyVal = [NSString stringWithFormat:@"%@=%@&",key,value];
        strDict = [strDict stringByAppendingString:strKeyVal];
    }

    
    if([UtiltiyManager isReachableToInternet]){
        [AppServiceManager updateSitterProfile:strDict onCompletion:^(id object, NSError *error) {
            if(object){
                
            }else{
                
            }
        }];
    }else{
        [UtiltiyManager showAlertWithMessage:@"NO Internet Connection"];
    }
}

- (IBAction)btnEditPicAction:(UIButton *)sender {
}

- (IBAction)btnEditBioAction:(UIButton *)sender {
}

- (IBAction)btnEditPreferenceAction:(UIButton *)sender {
}

- (IBAction)btnEditQualificationAction:(UIButton *)sender {
}

- (IBAction)btnEditLangAction:(UIButton *)sender {
}

- (IBAction)btnEditEducationAction:(UIButton *)sender {
}
@end
