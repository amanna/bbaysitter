//
//  UploadPhoto.m
//  BabySitterHawaii
//
//  Created by Amrita on 30/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "UploadPhoto.h"
#import <QuartzCore/QuartzCore.h>
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
#import "UploadVideo.h"
#import "DashboardVCParent.h"
@interface UploadPhoto ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgCam;

@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
- (IBAction)btnSkipAction:(id)sender;

@end

@implementation UploadPhoto
- (UIImage *)getRoundedRectImageFromImage :(UIImage *)image onReferenceView :(UIImageView*)imageView withCornerRadius :(float)cornerRadius
{
    UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, NO, 1.0);
    [[UIBezierPath bezierPathWithRoundedRect:imageView.bounds
                                cornerRadius:cornerRadius] addClip];
    [image drawInRect:imageView.bounds];
    UIImage *finalImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return finalImage;
}

- (IBAction)plusBtnAction:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}
-(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}
#pragma mark ImagePicker delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([UtiltiyManager isReachableToInternet]) {
        [activity showView];
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
        NSData *imageData = UIImageJPEGRepresentation(image,0.5f);
        
        NSString *urlString = @"http://www.efriendz.in/Anirbansite/index.php/user_registration/user_image_upload";
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        
        
        NSMutableData *body = [NSMutableData data];
        
        NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        int len = 64;
        //Creates a random boundary
        NSMutableString *boundary = [NSMutableString stringWithCapacity: len];
        
        for (int i=0; i<len; i++) {
            [boundary appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
        }
        [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
        NSData *boundaryData = [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding];
        [body appendData:boundaryData];
        
        
        //int i = 101;
        NSString *str = [self randomStringWithLength:4];
        
        [body appendData: [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"profile_image\"; filename=\"%@\"\r\n\r\n",    [NSString stringWithFormat:@"image%@.jpg",str]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"%@" , self.strUser] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        
        NSOperationQueue *mainqueue = [NSOperationQueue mainQueue];
        
        [NSURLConnection sendAsynchronousRequest:request queue:mainqueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if(data)
            {
                //NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                NSDictionary *res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                
                NSInteger success = [[res objectForKey:@"success"]integerValue];
                [activity hideView];
                self.imgCam.image = image;
                self.imgCam.layer.cornerRadius = self.imgCam.frame.size.width / 2;
                self.imgCam.clipsToBounds = YES;
                [self.btnSkip setBackgroundImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];

                
            }else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
            
        }];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }

    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    
    // nivendru gavaskar
        
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Profile Picture";    // Do any additional setup after loading the view.
    

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSkipAction:(id)sender {
    if([self.strUserType isEqualToString:@"parent"]){
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
        DashboardVCParent *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVCParent"];
        [self showViewController:dashboard sender:self];
    }else{
        [self performSegueWithIdentifier:@"uploadVideo" sender:self];
    }
   
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"uploadVideo"]) {
        UploadVideo *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.strUser = self.strUser;
    }
}

@end
