//
//  Additional.h
//  BabyParentSitter
//
//  Created by Amrita on 06/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Additional : NSObject
@property(nonatomic)NSString *strSid;
@property(nonatomic)NSString *strSName;
@property(nonatomic)NSString *strSUrl;
@end
