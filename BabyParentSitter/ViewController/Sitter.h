//
//  Sitter.h
//  BabyParentSitter
//
//  Created by Amrita on 11/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sitter : NSObject
@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strFname;
@property(nonatomic)NSString *strLname;
@property(nonatomic)NSString *strEmail;
@property(nonatomic)NSString *strAddress;
@property(nonatomic)NSString *strProfileImage;
@property(nonatomic)NSString *strProfileVideo;
@property(nonatomic)NSString *strProfileVideoThumb;
@property(nonatomic)NSString *strBackfgroundCheck;
@property(nonatomic)NSString *strDrivingComplete;

@property(nonatomic)NSString *strAcceptCredit;
@property(nonatomic)NSString *strAcceptCash;
@property(nonatomic)NSString *strAcceptPaypal;
@property(nonatomic)NSString *strAcceptCheQue;
@property(nonatomic)NSString *strDistanceTravel;
@property(nonatomic)NSString *strNoOfChild;
@property(nonatomic)NSString *strExperience;
@property(nonatomic)NSString *strLang;
@property(nonatomic)NSString *strBio;


@property(nonatomic)NSString *strtransportable;
@property(nonatomic)NSString *strNonsmoker;
@property(nonatomic)NSString *strCareSickkid;
@property(nonatomic)NSString *strComfortPet;
@property(nonatomic)NSString *strToddlers;
@property(nonatomic)NSString *strearlySchool;
@property(nonatomic)NSString *strelementrySchool;
@property(nonatomic)NSString *strTeens;
@property(nonatomic)NSString *strTweensOrMultiple;
@property(nonatomic)NSString *strSpclNeed;
@property(nonatomic)NSString *strSchoolLabel;
@property(nonatomic)NSString *strHighschoolDegree;
@property(nonatomic)NSString *strCollegeLabel;
@property(nonatomic)NSString *strCollegeDegree;
@property(nonatomic)NSString *strgraduateLabel;
@property(nonatomic)NSString *strgraduatedegree;


@property(nonatomic)NSString *strAvailableText;
@property(nonatomic)NSString *strAvailableBackUp;
@property(nonatomic)NSString *strFullTime;
@property(nonatomic)NSString *strNight;
@property(nonatomic)NSString *strWeekend;
@property(nonatomic)NSString *strBforeSchll;
@property(nonatomic)NSString *strafterSchll;


@property(nonatomic)NSString *strcertified_teacher;
@property(nonatomic)NSString *strcpr_certified;
@property(nonatomic)NSString *strFirstaid_train;
@property(nonatomic)NSString *strSpclNeedcare;

@property(nonatomic)NSString *strAdditional;
@end
