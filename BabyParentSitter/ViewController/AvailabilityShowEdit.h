//
//  AvailabilityShowEdit.h
//  BabyParentSitter
//
//  Created by Amrita on 31/05/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypePop{
    kActionSave=0,
    kActionCncl=1
}eventPopAvail;
typedef void (^EventCompletionHandler)(id object,NSUInteger eventPopAvail);

@interface AvailabilityShowEdit : UIViewController{
    BOOL isEdit;
     EventCompletionHandler handler;
}
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandler)eventhandler;
@end
