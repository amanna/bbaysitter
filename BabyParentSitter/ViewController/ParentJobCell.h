//
//  ParentJobCell.h
//  BabyParentSitter
//
//  Created by Amrita on 27/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParentJobCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblJobTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblJobLoc;
@property (weak, nonatomic) IBOutlet UILabel *lblHourlyRate;
@property (weak, nonatomic) IBOutlet UILabel *lblJobType;
@property (weak, nonatomic) IBOutlet UIButton *btnArrow;
@property (weak, nonatomic) IBOutlet UIButton *btnTrash;
@property (weak, nonatomic) IBOutlet UIButton *btnCopy;

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;


@end
