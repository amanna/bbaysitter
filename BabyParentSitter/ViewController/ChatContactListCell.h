//
//  ChatContactListCell.h
//  BabySitterHawaii
//
//  Created by Amrita on 20/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatContactListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblMsgDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblUnreadmsgcount;

@end
