//
//  JobPostStep2.m
//  BabySitterHawaii
//
//  Created by Amrita on 09/05/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "JobPostStep2.h"
#import <MapKit/MapKit.h>
#import "CTextField.h"
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
#import <CoreLocation/CoreLocation.h>
#import "POAcvityView.h"
#import "PostJobStep3.h"
#import "POAcvityView.h"
#define METERS_PER_MILE 1609.344
@interface JobPostStep2 ()<MKMapViewDelegate,CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet CTextField *txtTitle;
@property (weak, nonatomic) IBOutlet UITextView *txtDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UIView *transView;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
- (IBAction)postjobaction:(id)sender;
@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) NSString *strLatt;
@property (strong, nonatomic) NSString *strLongi;

@end

@implementation JobPostStep2

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    [activity showView];
    self.txtDesc.clipsToBounds = YES;
     self.txtDesc.layer.cornerRadius = 5.0f;
     self.txtTitle.autocorrectionType = UITextAutocorrectionTypeNo;
     self.txtDesc.autocorrectionType = UITextAutocorrectionTypeNo;
     self.txtTitle.leftPadding = 10.0;
     self.transView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    
    //Add back menu
    UIImage *buttonImageleft = [UIImage imageNamed:@"backicon"];
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLeft setImage:buttonImageleft forState:UIControlStateNormal];
    buttonLeft.frame = CGRectMake(0, 0, buttonImageleft.size.width, buttonImageleft.size.height);
    [buttonLeft addTarget:self action:@selector(leftMenuclicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonLeft];
    self.navigationItem.leftBarButtonItem = leftBarItem;

    
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Job Description";
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap)];
    singleFingerTap.cancelsTouchesInView = NO;
    [self.navigationController.view addGestureRecognizer:singleFingerTap];
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
        // Or [self.locationManager requestWhenInUseAuthorization];
    }
    if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
        // Or [self.locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    
    self.strLatt = [NSString stringWithFormat:@"%.8f",locationManager.location.coordinate.latitude];
    self.strLongi = [NSString stringWithFormat:@"%.8f",locationManager.location.coordinate.longitude];
    _mapView.delegate = self;
    [locationManager stopUpdatingLocation];
    [self findAddrss];


    // Do any additional setup after loading the view.
}
- (void)findAddrss{
    if ([UtiltiyManager isReachableToInternet]) {
        [AppServiceManager findAddress:self.strLatt andLon:self.strLongi onCompletion:^(id object, NSError *error) {
            [activity hideView];
            if(object){
                NSDictionary *dict = object;
                NSInteger success =[[dict valueForKey:@"success"]integerValue];
                if(success==1){
                    self.lblAddress.text = [dict valueForKey:@"address"];
                    
                    
                }
            }else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some problem occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
            }
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        
    }
    //map update
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = locationManager.location.coordinate.latitude;
    zoomLocation.longitude= locationManager.location.coordinate.longitude;
    
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    
    /*MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
     point.coordinate = newLocation.coordinate;
     point.title = @"Where am I?";
     point.subtitle = @"I'm here!!!";*/
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
    [point setCoordinate:self.location.coordinate];
    
    [self.mapView addAnnotation:point];
    
    [locationManager stopUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.location = locations.lastObject;
    NSLog(@"%@", locations);
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = self.location.coordinate.latitude;
    zoomLocation.longitude= self.location.coordinate.longitude;
    
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    
    /*MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
     point.coordinate = newLocation.coordinate;
     point.title = @"Where am I?";
     point.subtitle = @"I'm here!!!";*/
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
    [point setCoordinate:self.location.coordinate];
    
    [self.mapView addAnnotation:point];
    
    [locationManager stopUpdatingLocation];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = newLocation.coordinate.latitude;
    zoomLocation.longitude= newLocation.coordinate.longitude;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    
    /*MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
     point.coordinate = newLocation.coordinate;
     point.title = @"Where am I?";
     point.subtitle = @"I'm here!!!";*/
    
//    MyCustomAnnotation *point = [[MyCustomAnnotation alloc]initWithLocation:newLocation.coordinate];
//    
//    
//    [self.mapView addAnnotation:point];
    MKPointAnnotation *point = [[MKPointAnnotation alloc]init];
    [point setCoordinate:self.location.coordinate];
    
    [self.mapView addAnnotation:point];

    
    [locationManager stopUpdatingLocation];
    
    
    
    
}

-(void)leftMenuclicked{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)handleSingleTap{
    [self.view endEditing:YES];
    
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField==self.txtTitle){
        [self.txtTitle resignFirstResponder];
        [self.txtDesc becomeFirstResponder];
    }
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Add a brief job Description"]) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor]; //optional
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Add a brief job Description";
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)postjobaction:(id)sender {
   // parent_id(type int),job_id(type int),location(text),job_title(text),job_description(text)
//    {
//        jobid = 6;
//        msg = "job detail Successfully updated";
//        "parent_id" = 2;
//        success = 1;
//    }
    [activity showView];
    NSString *strJobInfo = [NSString stringWithFormat:@"parent_id=%@&job_id=%@&location=%@&job_title=%@&job_description=%@",self.strUid,self.strJobId,self.lblAddress.text,self.txtTitle.text,self.txtDesc.text];
    if([UtiltiyManager isReachableToInternet]){
    [AppServiceManager PostJobStep2:strJobInfo onCompletion:^(id object, NSError *error) {
        if(object){
            NSInteger success = [[object valueForKey:@"success"]integerValue];
            if(success==1){
                 self.strJobId = [object valueForKey:@"jobid"];
                [activity hideView];
                [self performSegueWithIdentifier:@"seguePostjob3" sender:self];
            }
        }else{
            
        }
        
    }];
    }else{
        
    }
   
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"seguePostjob3"]) {
        PostJobStep3 *rs =segue.destinationViewController;
        rs.strUser = self.strUid;
        rs.strJobId = self.strJobId;
    }
}
@end
