//
//  HomeViewController.h
//  BabySitterHawaii
//
//  Created by debashisandria on 13/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomPageControll.h"
#import "WelcomeViewController.h"
@interface HomeViewController : UIViewController <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *loginSeg;
@property (weak, nonatomic) IBOutlet CustomPageControll *pc;
@property (weak, nonatomic) IBOutlet UIScrollView *scr;
@property (weak, nonatomic) IBOutlet UIImageView *slider;

- (IBAction)SignAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewFirst;

@end
