//
//  PopUpVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 05/05/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "PopUpVC.h"

@interface PopUpVC ()
- (IBAction)btnCancelAction:(id)sender;
- (IBAction)btnOkAction:(id)sender;

@end

@implementation PopUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.8];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandler)eventhandler{
    handler = eventhandler;
}
- (IBAction)btnCancelAction:(id)sender {
    handler(nil,kActionCancel);
}

- (IBAction)btnOkAction:(id)sender {
    handler(nil,kActionOk);
}
@end
