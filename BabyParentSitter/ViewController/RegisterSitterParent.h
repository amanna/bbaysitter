//
//  RegisterSitterParent.h
//  BabySitterHawaii
//
//  Created by Amrita on 25/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTextField.h"
@interface RegisterSitterParent : UIViewController
@property (weak, nonatomic) IBOutlet CTextField *txtEmail;
@property (weak, nonatomic) IBOutlet CTextField *txtPassword;
@property (weak, nonatomic) IBOutlet CTextField *txtFname;
@property (weak, nonatomic) IBOutlet CTextField *txtLName;
@property (weak, nonatomic) IBOutlet CTextField *txtBirth;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailverify;
@property (weak, nonatomic) IBOutlet UIButton *btnMale;
- (IBAction)btnMaleAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnFemale;
- (IBAction)btnFemaleAction:(id)sender;
- (IBAction)btnNextAction:(id)sender;
@property (strong,nonatomic)NSString *strUserType;
@property (weak, nonatomic) IBOutlet UILabel *lblPolicyParent;
@property (weak, nonatomic) IBOutlet UILabel *lblPolicysitter;
- (void)toggleMaleFemale;
@property (weak, nonatomic) IBOutlet UIView *myView;
@property (strong,nonatomic)NSString *strDob;
@property(nonatomic,strong)NSString *strUid;
- (IBAction)btnTermAction:(id)sender;
- (IBAction)btnPrivacyPolicyAction:(id)sender;


@end
