//
//  DashboardVCParent.m
//  BabyParentSitter
//
//  Created by Amrita on 05/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "DashboardVCParent.h"
#import "CDSideBarController.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
#import "ParentProfileVC.h"
#import "ParentJobs.h"
#import "ChatContactListVC.h"
#import "SettingsVC.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Sitter.h"
#import "AppDelegate.h"
#import "JobPostStep1.h"
#define METERS_PER_MILE 1609.344
@interface DashboardVCParent ()<CDSideBarControllerDelegate,CLLocationManagerDelegate, MKAnnotation, MKMapViewDelegate>{
    CDSideBarController *sideBar;
    NSArray *nameList;
    POAcvityView *activity;
    CLLocationManager *locationManager;
    AppDelegate *appdelegate;
    
    __weak IBOutlet UITableView *tblVC;
}
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *topLabelText;
- (IBAction)segmentAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
- (IBAction)btnGpsAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *locationView;
- (IBAction)postJobAction:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *myMap;
@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) NSString *strLatt;
@property (strong, nonatomic) NSString *strLongi;

@property (strong, nonatomic) NSString *strType;
@property (strong, nonatomic) NSString *strMsg;
@property(nonatomic)NSMutableArray *arrSittters;
@property (strong, nonatomic) NSString *SitterId;
@end

@implementation DashboardVCParent

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrSittters = [[NSMutableArray alloc]init];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];

    //[activity showView];
    self.navigationItem.hidesBackButton = YES;
    UIImage *buttonImageRight = [UIImage imageNamed:@"Message"];
    UIButton *buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonRight setImage:buttonImageRight forState:UIControlStateNormal];
    buttonRight.frame = CGRectMake(0, 0, buttonImageRight.size.width, buttonImageRight.size.height);
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonRight];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    NSString *strUname = appdelegate.strUserName;
    nameList = @[strUname, @"Sitter", @"My Jobs",@"My Settings", @"Messages"];
    
    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
    sideBar.delegate = self;
    // Do any additional setup after loading the view.
    //[self getMyProfile];
    
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
        // Or [self.locationManager requestWhenInUseAuthorization];
    }
    if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
        // Or [self.locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    
    self.strLatt = [NSString stringWithFormat:@"%.8f",locationManager.location.coordinate.latitude];
    self.strLongi = [NSString stringWithFormat:@"%.8f",locationManager.location.coordinate.longitude];
    self.myMap.delegate = self;
    [locationManager stopUpdatingLocation];
    self.locationView.hidden = YES;
    self.segmentedControl.selectedSegmentIndex = 1;
    
    self.strType = @"new";
    
    [self getSitter];
   
    

}
- (IBAction)segmentAction:(id)sender {
    if(self.segmentedControl.selectedSegmentIndex == 0){
        self.strType = @"location";
        self.locationView.hidden = NO;
        [self findAddrss];
    }else if (self.segmentedControl.selectedSegmentIndex == 1){
         self.strType = @"new";
        self.locationView.hidden = YES;
        [self getSitter];
    }else if (self.segmentedControl.selectedSegmentIndex == 2){
         self.strType = @"fav";
        self.locationView.hidden = YES;
         [self getSitter];
    }
    
}
- (void)getSitter{
    if([self.strType isEqualToString:@"location"]){
        self.strMsg = [NSString stringWithFormat:@"parentid=%@&lat=%@&long=%@",self.strUid,self.strLatt,self.strLongi];
    }else if ([self.strType isEqualToString:@"new"]){
        self.strMsg = [NSString stringWithFormat:@"parentid=%@",self.strUid];
    }else if ([self.strType isEqualToString:@"fav"]){
         self.strMsg = [NSString stringWithFormat:@"parentid=%@",self.strUid];
    }
   if ([UtiltiyManager isReachableToInternet]) {
        [activity showView];
        
        [AppServiceManager SitterList:self.strMsg withType:self.strType onCompletion:^(id object, NSError *error) {
            if (object) {
                self.arrSittters = object;
                [tblVC reloadData];
                [activity hideView];
            }else{
                [activity hideView];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                

            }
            
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];

    }

}
- (void)findAddrss{
    if ([UtiltiyManager isReachableToInternet]) {
        [activity showView];
        [AppServiceManager findAddress:self.strLatt andLon:self.strLongi onCompletion:^(id object, NSError *error) {
            [activity hideView];
            if(object){
                NSDictionary *dict = object;
                NSInteger success =[[dict valueForKey:@"success"]integerValue];
                if(success==1){
                    self.txtAddress.text = [dict valueForKey:@"address"];
                    
                    [AppServiceManager updateAddress:self.strUid andAddress:self.txtAddress.text andLatt:self.strLatt andLong:self.strLongi onCompletion:^(id object, NSError *error) {
                        [activity hideView];
                        if(object){
                            NSDictionary *dict = object;
                            NSInteger success =[[dict valueForKey:@"success"]integerValue];
                            if(success==1){
                                [AppServiceManager SitterList:self.strMsg withType:self.strType onCompletion:^(id object, NSError *error) {
                                    if (object) {
                                        self.arrSittters = object;
                                        [tblVC reloadData];
                                        [activity hideView];
                                    }else{
                                        [activity hideView];
                                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some problem occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                        [alert show];
                                    }
                                    
                                }];
                            }
                            
                        }else{
                            NSLog(@"error=%@",error.debugDescription);
                        }
                        
                    }];
                    
                }
            }else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some problem occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
            }
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        
    }
    //map update
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = locationManager.location.coordinate.latitude;
    zoomLocation.longitude= locationManager.location.coordinate.longitude;
    
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [self.myMap setRegion:viewRegion animated:YES];
    
    /*MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
     point.coordinate = newLocation.coordinate;
     point.title = @"Where am I?";
     point.subtitle = @"I'm here!!!";*/
    
   
    
    [locationManager stopUpdatingLocation];
}




- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [sideBar insertMenuButtonOnView:self.navigationController.view atPosition: CGPointMake(15, 25)];
}

#pragma mark -
#pragma mark - CDSideBarController delegate

- (void)menuButtonClicked:(int)index
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    if(index==1){
        self.segmentedControl.selectedSegmentIndex=1;
    }else if (index==2){
        ParentJobs *dashboard = [story instantiateViewControllerWithIdentifier:@"ParentJobs"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
    }else if (index==0){
        ParentProfileVC *dashboard = [story instantiateViewControllerWithIdentifier:@"ParentProfileVC"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
        //[self performSegueWithIdentifier:@"segueSitterProfileVC" sender:self];
    }else if (index==3){
        
        //[self performSegueWithIdentifier:@"segueSettings" sender:self];
        SettingsVC *dashboard = [story instantiateViewControllerWithIdentifier:@"SettingsVC"];
        [self.navigationController pushViewController:dashboard animated:YES];
//        dashboard.strUid = self.strUid;
//        dashboard.strUserType = self.strUserType;
        //[self presentViewController:dashboard animated:YES completion:nil];
       // [self showViewController:dashboard sender:self];
    }else{
        ChatContactListVC *dashboard = [story instantiateViewControllerWithIdentifier:@"ChatContactListVC"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
       // [self performSegueWithIdentifier:@"segueChatMsg" sender:self];
    }
    
}
//naresh
#pragma mark -
#pragma mark - UITableView datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrSittters.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d", indexPath.row); // you can see selected row number in your console;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        
    }
    
    UIImageView *profileImage = (UIImageView *)[cell viewWithTag:100];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:101];
    UILabel *typeLabel = (UILabel *)[cell viewWithTag:102];
    UILabel *locationLabel = (UILabel *)[cell viewWithTag:103];
    UILabel *expLabel = (UILabel *)[cell viewWithTag:104];
    
    UIButton *button1 = (UIButton *)[cell viewWithTag:105];
    [button1 addTarget:self action:@selector(button1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *button2 = (UIButton *)[cell viewWithTag:106];
    [button2 addTarget:self action:@selector(button2Clicked:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *button3 = (UIButton *)[cell viewWithTag:107];
    [button3 addTarget:self action:@selector(button3Clicked:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *button4 = (UIButton *)[cell viewWithTag:108];
    [button4 addTarget:self action:@selector(button4Clicked:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *button5 = (UIButton *)[cell viewWithTag:109];
    [button5 addTarget:self action:@selector(button5Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *button6 = (UIButton *)[cell viewWithTag:110];
    [button6 addTarget:self action:@selector(button6Clicked:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *button7 = (UIButton *)[cell viewWithTag:111];
    [button7 addTarget:self action:@selector(button7Clicked:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *button8 = (UIButton *)[cell viewWithTag:112];
    [button8 addTarget:self action:@selector(button8Clicked:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *button9 = (UIButton *)[cell viewWithTag:113];
    [button9 addTarget:self action:@selector(button9Clicked:) forControlEvents:UIControlEventTouchUpInside];
    UIButton *button10 = (UIButton *)[cell viewWithTag:114];
    [button10 addTarget:self action:@selector(button10Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if (indexPath.row % 2) {
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"Stars"] forState:UIControlStateNormal];
        [button2 setBackgroundImage:[UIImage imageNamed:@"Stars"] forState:UIControlStateNormal];
        [button3 setBackgroundImage:[UIImage imageNamed:@"Stars"] forState:UIControlStateNormal];
        [button4 setBackgroundImage:[UIImage imageNamed:@"Stars"] forState:UIControlStateNormal];
        [button5 setBackgroundImage:[UIImage imageNamed:@"Stars"] forState:UIControlStateNormal];
    }
    
    else{
        
        [button1 setBackgroundImage:[UIImage imageNamed:@"EmptyStar"] forState:UIControlStateNormal];
        [button2 setBackgroundImage:[UIImage imageNamed:@"EmptyStar"] forState:UIControlStateNormal];
        [button3 setBackgroundImage:[UIImage imageNamed:@"EmptyStar"] forState:UIControlStateNormal];
        [button4 setBackgroundImage:[UIImage imageNamed:@"EmptyStar"] forState:UIControlStateNormal];
        [button5 setBackgroundImage:[UIImage imageNamed:@"EmptyStar"] forState:UIControlStateNormal];
    }
    
    [button6 setBackgroundImage:[UIImage imageNamed:@"Bg_Sheild"] forState:UIControlStateNormal];
    [button7 setBackgroundImage:[UIImage imageNamed:@"CarIcon"] forState:UIControlStateNormal];
    [button8 setBackgroundImage:[UIImage imageNamed:@"Cpr"] forState:UIControlStateNormal];
    [button9 setBackgroundImage:[UIImage imageNamed:@"First_Aid"] forState:UIControlStateNormal];
    [button10 setBackgroundImage:[UIImage imageNamed:@"SocialConnection"] forState:UIControlStateNormal];
    
    
    //Populate Date
    
    Sitter *sitter = [self.arrSittters objectAtIndex:indexPath.row];
    if([sitter.strProfileImage isEqualToString:@""]){
        profileImage.image = [UIImage imageNamed:@"ActorImageBG"];
    }else{
        
    }
    titleLabel.text = [NSString stringWithFormat:@"%@ %@",sitter.strFname, sitter.strLname];
    typeLabel.text = @"NEW";
    locationLabel.text = [NSString stringWithFormat:@"%@ |",sitter.strAddress];
    expLabel.text = sitter.strExperience;
    
    self.SitterId = sitter.strUid;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapSitter)];
    singleTap.cancelsTouchesInView = NO;
    [cell addGestureRecognizer:singleTap];
    
    return cell;
}
- (void)singleTapSitter{
 
}
- (IBAction)button1Clicked:(id)sender {
  //  [self alertviewstring:@"Star 1"];
}

- (IBAction)button2Clicked:(id)sender {
    //[self alertviewstring:@"Star 2"];
}

- (IBAction)button3Clicked:(id)sender {
   // [self alertviewstring:@"Star 3"];
}

- (IBAction)button4Clicked:(id)sender {
   // [self alertviewstring:@"Star 4"];
}

- (IBAction)button5Clicked:(id)sender {
   // [self alertviewstring:@"Star 5"];
}

- (IBAction)button6Clicked:(id)sender {
  //  [self alertviewstring:@"Sheild Icon"];
}

- (IBAction)button7Clicked:(id)sender {
   // [self alertviewstring:@"Car Icon"];
}

- (IBAction)button8Clicked:(id)sender {
   // [self alertviewstring:@"CPR Icon"];
}

- (IBAction)button9Clicked:(id)sender {
    //[self alertviewstring:@"FirstAid Icon"];
}

- (IBAction)button10Clicked:(id)sender {
    //[self alertviewstring:@"Social Icon"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)postJobAction:(id)sender {
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    JobPostStep1 *step1 = [story instantiateViewControllerWithIdentifier:@"JobPostStep1"];
    step1.strUserType = appdelegate.strUserType;
    step1.strUser = appdelegate.strUserId;
    

    [self showViewController:step1 sender:self];
    
}
- (IBAction)btnGpsAction:(id)sender {
}
@end
