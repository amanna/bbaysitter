//
//  DatepickerPop.m
//  BabyParentSitter
//
//  Created by Amrita on 03/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "DatepickerPop.h"

@interface DatepickerPop ()
- (IBAction)btnCancelAction:(id)sender;
- (IBAction)btnDoneAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *picker;
@property(nonatomic)NSString *selectedDate;
- (IBAction)valchangedBydatepicker:(id)sender;

@end

@implementation DatepickerPop

- (void)viewDidLoad {
    [super viewDidLoad];
    self.picker.datePickerMode = UIDatePickerModeDateAndTime;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandler)eventhandler{
    handler = eventhandler;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCancelAction:(id)sender {
     handler(self.selectedDate,kActionCancel);
}

- (IBAction)btnDoneAction:(id)sender {
    handler(self.selectedDate,kActionDone);
}
- (IBAction)valchangedBydatepicker:(id)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"EEE.MMM dd hh:mm a"];
    self.selectedDate = [dateFormatter stringFromDate:self.picker.date];
    
}
@end
