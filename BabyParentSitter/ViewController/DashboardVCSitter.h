//
//  DashboardVC.h
//  BabySitterHawaii
//
//  Created by Amrita on 11/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardVCSitter : UIViewController
@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strUserType;

@end
