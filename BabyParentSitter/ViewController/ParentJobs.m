//
//  ParentJobs.m
//  BabyParentSitter
//
//  Created by Amrita on 27/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "ParentJobs.h"
#import "ParentJobCell.h"
#import "CDSideBarController.h"
#import "ChatContactListVC.h"
#import "ParentProfileVC.h"
#import "DashboardVCParent.h"
#import "SettingsVC.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
#import "Job.h"
@interface ParentJobs ()<UITableViewDelegate,UITableViewDataSource,CDSideBarControllerDelegate>{
    CDSideBarController *sideBar;
    NSArray *nameList;
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UITableView *tblJobs;
@property(nonatomic)NSMutableArray *arrJobs;
@property(nonatomic)NSString *strJobType;
@end

@implementation ParentJobs

- (void)viewDidLoad {
    [super viewDidLoad];
      activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.navigationItem.hidesBackButton = YES;
    UIImage *buttonImageRight = [UIImage imageNamed:@"Message"];
    UIButton *buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonRight setImage:buttonImageRight forState:UIControlStateNormal];
    buttonRight.frame = CGRectMake(0, 0, buttonImageRight.size.width, buttonImageRight.size.height);
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonRight];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"My Jobs";
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    nameList = @[@"John Doe", @"Sitter", @"My Jobs",@"My Settings", @"Messages"];
    
    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
    sideBar.delegate = self;

    self.arrJobs = [[NSMutableArray alloc]init];
    //Active,Expired,Completed
    self.strJobType = @"Active";
    [self getJobs];
    
    // Do any additional setup after loading the view.
}
- (void)getJobs{
    [activity showView];
    if([UtiltiyManager isReachableToInternet]){
       [AppServiceManager getParentJobs:self.strUid andJobType:self.strJobType onCompletion:^(id object, NSError *error) {
           if(object){
               self.arrJobs = object;
               [activity hideView];
               [self.tblJobs reloadData];
           }else{
               [activity hideView];
               [UtiltiyManager showAlertWithMessage:@"Some Problem Occurs!!Please try again later"];
           }
       }];
    }else{
        [UtiltiyManager showAlertWithMessage:@"NO INTERNET CONNECTION"];
    }

    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [sideBar insertMenuButtonOnView:self.navigationController.view atPosition: CGPointMake(15, 25)];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //return self.arrJobs.count;
    return self.arrJobs.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ParentJobCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    return cell;
    
}
#pragma mark - CDSideBarController delegate

#pragma mark - CDSideBarController delegate

- (void)menuButtonClicked:(int)index
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    if(index==1){
        DashboardVCParent *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVCParent"];
        dashboard.strUid = self.strUid;
        dashboard.segmentedControl.selectedSegmentIndex=1;
        [self showViewController:dashboard sender:self];
    }else if (index==2){
        ParentJobs *dashboard = [story instantiateViewControllerWithIdentifier:@"ParentJobs"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
    }else if (index==0){
        ParentProfileVC *dashboard = [story instantiateViewControllerWithIdentifier:@"ParentProfileVC"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
        //[self performSegueWithIdentifier:@"segueSitterProfileVC" sender:self];
    }else if (index==3){
        SettingsVC *dashboard = [story instantiateViewControllerWithIdentifier:@"SettingsVC"];
        dashboard.strUid = self.strUid;
        dashboard.strUserType = self.strUserType;
        [self showViewController:dashboard sender:self];

    }else{
        ChatContactListVC *dashboard = [story instantiateViewControllerWithIdentifier:@"ChatContactListVC"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
        // [self performSegueWithIdentifier:@"segueChatMsg" sender:self];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
