//
//  SendMsgVC.m
//  BabyParentSitter
//
//  Created by Amrita on 29/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "SendMsgVC.h"
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
#import "DashboardVCParent.h"
#import "DashboardVCSitter.h"
#import "ParentProfileVC.h"
#import "SitterProfileVC.h"
#import "ParentJobs.h"
#import "ChatContactListVC.h"

@interface SendMsgVC ()

@end

@implementation SendMsgVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    [self.txtMsg becomeFirstResponder];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSendMsgAction:(id)sender {
   [self performSegueWithIdentifier:@"segueMyMsg" sender:self];
}
@end
