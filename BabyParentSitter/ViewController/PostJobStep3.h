//
//  ViewController.h
//  Screen2
//
//  Created by Star Mobileoid2 Technologies  on 24/05/15.
//  Copyright (c) 2015 Naresh Jain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostJobStep3 : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *colletionView;
@property (strong,nonatomic)NSString *strUserType;
@property (strong,nonatomic)NSString *strUser;
@property (strong,nonatomic)NSString *strJobId;
@end

