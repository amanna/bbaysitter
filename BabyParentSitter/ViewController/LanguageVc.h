//
//  LanguageVc.h
//  BabyParentSitter
//
//  Created by Amrita on 02/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeLang{
    kActionSave=0,
    kActionCancell=1
}eventPopLang;
typedef void (^EventCompletionHandlerL)(id object,id object1,NSUInteger eventPop);
@interface LanguageVc : UIViewController{
    EventCompletionHandlerL handler;
}
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandlerL)eventhandler;
@end
