//
//  LanguageVc.m
//  BabyParentSitter
//
//  Created by Amrita on 02/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "LanguageVc.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
@interface LanguageVc ()<UITableViewDataSource,UITableViewDelegate>{
    POAcvityView *activity;
}
- (IBAction)btnSaveAction:(id)sender;
- (IBAction)btnCancelAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic)NSMutableArray *arrLang;
@property(nonatomic)NSMutableArray *arrSelected;
@property(nonatomic)NSMutableArray *arrSelectedNmae;
@property(nonatomic)NSString *strSelected;
@property(nonatomic)NSString *strSelectedName;
@end

@implementation LanguageVc

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrLang = [[NSMutableArray alloc]init];
    self.arrSelected = [[NSMutableArray alloc]init];
    self.arrSelectedNmae = [[NSMutableArray alloc]init];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    if([UtiltiyManager isReachableToInternet]){
    [AppServiceManager FindAllLanguage:^(id object, NSError *error) {
        if(object){
            self.arrLang = object;
            [self.tblView reloadData];
        }else{
            [UtiltiyManager showAlertWithMessage:@"Some Problem Occurs!!!Please try again later"];
        }
    }];}
    else{
        [UtiltiyManager showAlertWithMessage:@"No Internet connection"];
    }
    // Do any additional setup after loading the view.
}
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandlerL)eventhandler{
    handler = eventhandler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrLang.count;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    NSDictionary *dict = [self.arrLang objectAtIndex:indexPath.row];
    cell.textLabel.text = [dict valueForKey:@"lnagname"];
    NSString *strId = [dict valueForKey:@"langid"];
    bool isFound = NO;
    for (NSString *strItem in self.arrSelected){
        if([strItem isEqualToString:strId]){
            isFound = YES;
            break;
        }
    }
    if(isFound==NO){
        //Gray
      cell.tintColor = [UIColor colorWithRed:152/255.0 green:152/255.0 blue:152/255.0 alpha:1];
    }else{
        //Grenn
        cell.tintColor = [UIColor colorWithRed:69/255.0 green:211/255.0 blue:6/255.0 alpha:1];
    }
    cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    cell.userInteractionEnabled = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
- (void)btnLangAction:(UIButton *)sender{
    NSLog(@"%d",sender.tag);
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dict = [self.arrLang objectAtIndex:indexPath.row];
    NSString *strId = [dict valueForKey:@"langid"];
    NSString *strLan = [dict valueForKey:@"lnagname"];
    bool isFound = NO;
    if(self.arrSelected.count >0){
    for (NSString *strItem in self.arrSelected){
        if([strItem isEqualToString:strId]){
            isFound = YES;
            break;
        }
    }}
    else{
            isFound = NO;
    }
    if(isFound ==NO){
        [self.arrSelected addObject:strId];
        [self.arrSelectedNmae addObject:strLan];
    }else{
         [self.arrSelected removeObject:strId];
         [self.arrSelectedNmae addObject:strLan];
    }
    [tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSaveAction:(id)sender {
    self.strSelected = [self.arrSelected componentsJoinedByString:@","];
    self.strSelectedName = [self.arrSelectedNmae componentsJoinedByString:@","];
     handler(self.strSelected,self.strSelectedName,kActionSave);
}

- (IBAction)btnCancelAction:(id)sender {
     handler(nil,nil,kActionCancell);
}
@end
