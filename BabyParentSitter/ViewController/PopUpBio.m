//
//  PopUpBio.m
//  BabyParentSitter
//
//  Created by Amrita on 30/08/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "PopUpBio.h"

@interface PopUpBio ()
- (IBAction)btnCloseAction:(UIButton *)sender;

@end

@implementation PopUpBio

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandler)eventhandler{
    handler = eventhandler;
}
- (IBAction)btnCloseAction:(UIButton *)sender {
     handler(nil,kActionClose);
}
@end
