//
//  CustomAnnotation.m
//  BabySitterHawaii
//
//  Created by debashisandria on 28/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "MyCustomAnnotation.h"
@implementation MyCustomAnnotation
@synthesize coordinate, title;

- (id)initWithLocation:(CLLocationCoordinate2D)coord {
    self = [super init];
    if (self) {
        coordinate = coord;
    }
    return self;
}

-(id)initWithLocationAndTitle:(CLLocationCoordinate2D)coord titile:(NSString *)titleString {
    self = [super init];
    if (self) {
        coordinate = coord;
        title = @"I am here";
    }
    return self;
}
@end
