//
//  SitterPaymentVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "SitterPaymentVC.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "SitterAvailability.h"
#import "UtiltiyManager.h"
@interface SitterPaymentVC ()<UIScrollViewDelegate>{
    NSInteger creditFlag;
    NSInteger cashFlag;
    NSInteger paypalFlag;
    NSInteger personalcheckFlag;
    NSInteger transportFlag;
    NSInteger nonsmokerFlag;
    NSInteger kidFlag;
    NSInteger petFlag;
    NSInteger noOfdist;
    NSInteger noOfChild;
    POAcvityView *activity;
}
- (IBAction)btnNextAction:(id)sender;
- (IBAction)DistanceChange:(id)sender;

- (IBAction)ChildrenChange:(id)sender;
- (IBAction)btnCreditCardAction:(id)sender;
- (IBAction)btnCashAction:(id)sender;
- (IBAction)btnPaypalAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnCredit;
@property (weak, nonatomic) IBOutlet UIButton *btnCash;
@property (weak, nonatomic) IBOutlet UIButton *btnPaypal;
@property (weak, nonatomic) IBOutlet UIButton *btnPersonalcheck;

- (IBAction)btnPersonalCheckaction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnTransport;
- (IBAction)btnTransPortAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNonsmoker;
- (IBAction)btnNonsmokerAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnkid;

@property (weak, nonatomic) IBOutlet UIButton *btnPet;
- (IBAction)btnKidAction:(id)sender;
- (IBAction)btnPetAction:(id)sender;

- (IBAction)ss:(id)sender;


@end

@implementation SitterPaymentVC
- (BOOL)touchesShouldCancelInContentView:(UIView *)view {
    return NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.distanceSlider.sliderUnit = sliderMiles;
    self.childslider.sliderUnit = sliderInteger;
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    noOfdist = [self.distanceSlider value];
    noOfChild = [self.childslider value];
    creditFlag=1;
    cashFlag=0;
    paypalFlag=1;
    personalcheckFlag=0;
    
    transportFlag = 1;
    nonsmokerFlag=1;
    kidFlag=0;
    petFlag=0;
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Profile Settings";
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)btnCreditCardAction:(id)sender {
    if(creditFlag==1){
        creditFlag=0;
        [self.btnCredit setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        creditFlag=1;
        [self.btnCredit setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}

- (IBAction)btnCashAction:(id)sender {
    if(cashFlag==1){
        cashFlag=0;
        [self.btnCash setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        cashFlag=1;
        [self.btnCash setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}

- (IBAction)btnPaypalAction:(id)sender {
    if(paypalFlag==1){
        paypalFlag=0;
        [self.btnPaypal setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        paypalFlag=1;
        [self.btnPaypal setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnPersonalCheckaction:(id)sender {
    if(personalcheckFlag==1){
        personalcheckFlag=0;
        [self.btnPersonalcheck setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        personalcheckFlag=1;
        [self.btnPersonalcheck setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btnTransPortAction:(id)sender {
    if(transportFlag==1){
        transportFlag=0;
        [self.btnTransport setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        transportFlag=1;
        [self.btnTransport setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnNonsmokerAction:(id)sender {
    if(nonsmokerFlag==1){
        nonsmokerFlag=0;
        [self.btnNonsmoker setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        nonsmokerFlag=1;
        [self.btnNonsmoker setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btnKidAction:(id)sender {
    if(kidFlag==1){
        kidFlag=0;
        [self.btnkid setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        kidFlag=1;
        [self.btnkid setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}

- (IBAction)btnPetAction:(id)sender {
    if(petFlag==1){
        petFlag=0;
        [self.btnPet setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        petFlag=1;
        [self.btnPet setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnNextAction:(id)sender {
   if ([UtiltiyManager isReachableToInternet]) {
    [activity showView];
    NSString *strPayment = [NSString stringWithFormat:@"accept_credit_card=%ld&accept_cash=%ld&accept_paypal=%ld&accept_checks=%ld&have_transportation=%ld&non_smoker=%ld&care_sick_kids=%ld&comfortable_with_pets=%ld&distance_willing_travel=%ld&no_of_child_handle=%ld&userid=%@",(long)creditFlag,(long)cashFlag,(long)paypalFlag,(long)personalcheckFlag,(long)transportFlag,(long)nonsmokerFlag,(long)kidFlag,(long)petFlag,(long)noOfdist,(long)noOfChild,self.strUser];
    [AppServiceManager updatePaymentInfo:strPayment onCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            NSDictionary *dict = object;
            NSInteger success =[[dict valueForKey:@"success"]integerValue];
            if(success==1){
                [self performSegueWithIdentifier:@"segueAvailability" sender:self];
            }
         }else{
           
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
   
}

- (IBAction)DistanceChange:(UISlider*)sender {
     NSLog(@"slider value = %f", sender.value);
    
}

- (IBAction)ChildrenChange:(id)sender {
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"segueAvailability"]) {
        SitterAvailability *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.strUser = self.strUser;
    }
}



@end
