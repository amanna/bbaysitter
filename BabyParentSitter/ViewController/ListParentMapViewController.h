//
//  ListParentMapViewController.h
//  BabySitterHawaii
//
//  Created by debashisandria on 28/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#define METERS_PER_MILE 1609.344
@interface ListParentMapViewController : UIViewController <CLLocationManagerDelegate, MKAnnotation, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;


@end
