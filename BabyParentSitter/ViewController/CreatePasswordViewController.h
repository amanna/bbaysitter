//
//  CreatePasswordViewController.h
//  BabySitterHawaii
//
//  Created by debashisandria on 21/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTextField.h"
#define kOFFSET_FOR_KEYBOARD 80.0
@interface CreatePasswordViewController : UIViewController <UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet CTextField *email;
@property (weak, nonatomic) IBOutlet CTextField *firstName;
@property (weak, nonatomic) IBOutlet CTextField *lastName;
@property (weak, nonatomic) IBOutlet CTextField *bday;
@property (weak, nonatomic) IBOutlet CTextField *password;
@property (weak, nonatomic) IBOutlet UIButton *btnMale;
@property (weak, nonatomic) IBOutlet UIButton *btnFemale;
- (IBAction)btnMaleAction:(id)sender;
- (IBAction)btnFemaleAction:(id)sender;

@property (weak, nonatomic) NSString *_email;
@property (weak, nonatomic) NSString *_firstName;
@property (weak, nonatomic) NSString *_lastName;
@property (weak, nonatomic) NSString *_bday;
@property (weak, nonatomic) NSString *_gender;
@property (weak, nonatomic) NSString *strUserType;
@property (weak, nonatomic) NSString *fbId;
@property(nonatomic,strong)NSString *strUid;
@property(nonatomic,strong)NSString *imageUrl;
- (IBAction)btnTermAction:(id)sender;
- (IBAction)btnPrivacyAction:(id)sender;


@end
