//
//  ParentProfileVC.m
//  BabyParentSitter
//
//  Created by Amrita on 27/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "ParentProfileVC.h"
#import "UtiltiyManager.h"
#import "Reachability.h"
#import "AppServiceManager.h"
#import "CDSideBarController.h"
#import "POAcvityView.h"
#import "ParentJobs.h"
#import "SettingsVC.h"
#import "ChatContactListVC.h"
#import "DashboardVCParent.h"
#import "Job.h"
#import "Parent.h"
#import "FindMatchCell.h"
#import "JobDetailsVC.h"
#import "AppDelegate.h"
@interface ParentProfileVC ()<UITableViewDataSource,UITableViewDelegate>{
    CDSideBarController *sideBar;
    NSArray *nameList;
    POAcvityView *activity;
    AppDelegate *appdelegate;
    __weak IBOutlet UITableView *tbljob;
    
    __weak IBOutlet UILabel *lblParentNmae;
}
//@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
//@property (weak, nonatomic) IBOutlet UILabel *topLabelText;
@property(nonatomic)NSMutableArray *arrJobs;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblJobPost;

@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UILabel *lblKid;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintTbl;


@end

@implementation ParentProfileVC
-(void)leftMenuclicked{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)menuButtonClicked:(int)index
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    if(index==1){
        DashboardVCParent *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVCParent"];
        dashboard.strUid = self.strUid;
        dashboard.segmentedControl.selectedSegmentIndex=1;
        [self showViewController:dashboard sender:self];
    }else if (index==2){
        ParentJobs *dashboard = [story instantiateViewControllerWithIdentifier:@"ParentJobs"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
    }else if (index==0){
        
       
    }else if (index==3){
        SettingsVC *dashboard = [story instantiateViewControllerWithIdentifier:@"SettingsVC"];
        [self.navigationController pushViewController:dashboard animated:YES];
        //        dashboard.strUid = self.strUid;
        //        dashboard.strUserType = self.strUserType;
        //[self presentViewController:dashboard animated:YES completion:nil];
        // [self showViewController:dashboard sender:self];
    }else{
        ChatContactListVC *dashboard = [story instantiateViewControllerWithIdentifier:@"ChatContactListVC"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
        // [self performSegueWithIdentifier:@"segueChatMsg" sender:self];
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
   appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.arrJobs = [[NSMutableArray alloc]init];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    //[activity showView];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.hidesBackButton = YES;
 
    
    UIImage *buttonImageRight = [UIImage imageNamed:@"Message"];
    UIButton *buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonRight setImage:buttonImageRight forState:UIControlStateNormal];
    buttonRight.frame = CGRectMake(0, 0, buttonImageRight.size.width, buttonImageRight.size.height);
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonRight];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Profile";
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    NSString *strUname = appdelegate.strUserName;
    nameList = @[strUname, @"Sitter", @"My Jobs",@"My Settings", @"Messages"];
    
    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
    sideBar.delegate = self;

    
      [activity showView];
    if([UtiltiyManager isReachableToInternet]){
        [AppServiceManager getParentProfile:self.strUid onCompletion:^(id object,id object1, NSError *error) {
            if(object){
                Parent *parent = object;
                self.lblKid.text = parent.strAddress;
                lblParentNmae.text = [NSString stringWithFormat:@"%@ %@",parent.strFname,parent.strLname];
                self.lblLocation.text = parent.strAddress;
                self.arrJobs = object1;
               // self.profileImg.image
                if(self.arrJobs.count > 0){
                   [tbljob reloadData];
                }else{
                    self.heightConstraintTbl.constant = 0;
                    self.lblJobPost.text = @"NO Jobs Posted";
                }
                
                [activity hideView];
                
            }else{
                 [UtiltiyManager showAlertWithMessage:@"Some Problem Occurs!!Please try again later"];
            }
        }];
    }else{
        [UtiltiyManager showAlertWithMessage:@"NO INTERNET CONNECTION"];
    }
    // Do any additional setup after loading the view.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [sideBar insertMenuButtonOnView:self.navigationController.view atPosition: CGPointMake(15, 25)];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
   return self.arrJobs.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FindMatchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Job *job = [self.arrJobs objectAtIndex:indexPath.row];
    cell.lblJobTitle.text = job.strTitle;
    cell.lblJobType.text = job.strSitterReqType;
    cell.lblJobLoc.text = job.strLocation;
    cell.hourrate.text = job.strHourlyRate;
    
    return cell;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
