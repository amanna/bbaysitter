//
//  SitterAdditionalVC.h
//  BabyParentSitter
//
//  Created by Amrita on 19/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SitterAdditionalVC : UIViewController<UICollectionViewDataSource, UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *colletionView;
@property (strong,nonatomic)NSString *strUserType;
@property (strong,nonatomic)NSString *strUser;

@end
