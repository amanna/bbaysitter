//
//  AddrParentCountVC.h
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#define METERS_PER_MILE 1609.344
@interface AddrParentCountVC : UIViewController<CLLocationManagerDelegate, MKAnnotation, MKMapViewDelegate>
@property(nonatomic,strong)NSString *strCount;
@property(nonatomic,strong)NSString *strUid;
@property(nonatomic,strong)NSString *strUserType;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
- (IBAction)btnGpsClickedAction:(id)sender;

@end
