//
//  JobPostStep1.m
//  BabySitterHawaii
//
//  Created by Amrita on 08/05/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "JobPostStep1.h"
#import "DatepickerPop.h"
#import "AppServiceManager.h"
#import "LanguageVc.h"
#import "UtiltiyManager.h"
#import "JobPostStep2.h"
#import "POAcvityView.h"
#define btnTag 5000
@interface JobPostStep1 (){
    NSInteger transportFlag;
    NSInteger nonsmokerFlag;
    NSInteger carekidFlag;
    NSInteger comfortkidFlag;
    NSInteger twinFlag;
    NSInteger spclneedFlag;
    
    NSInteger creditFlag;
    NSInteger cashFlag;
    NSInteger checkFlag;
    NSInteger paypalFlag;
    POAcvityView *activity;
}
- (IBAction)btnstepperPlusAction:(id)sender;
- (IBAction)btnStepperMinusAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblToddler;
@property (weak, nonatomic) IBOutlet UILabel *lblaerlyage;
@property (weak, nonatomic) IBOutlet UILabel *lblelementryage;
@property (weak, nonatomic) IBOutlet UILabel *lblPreteensage;
@property (weak, nonatomic) IBOutlet UILabel *lblteenager;

@property (weak, nonatomic) IBOutlet UIButton *btnTransport;
- (IBAction)btnTransportAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNonSmoker;
- (IBAction)btnNonsmokerAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCareKid;
- (IBAction)btnCareKidAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnComfortWithKid;
- (IBAction)btnComfortKidAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTwin;
- (IBAction)btnTwinAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSpclNeed;
- (IBAction)btnSpclAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnCredit;
- (IBAction)btnCreditAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnPaycheck;
- (IBAction)btnPaycheckAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btncash;

- (IBAction)btnCashAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnPaypal;
- (IBAction)btnPaypalAction:(id)sender;
- (IBAction)btnNextAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewOneTime;
@property (weak, nonatomic) IBOutlet UIView *viewRecurring;
- (IBAction)segmentAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblstrtTime;
@property (weak, nonatomic) IBOutlet UILabel *lblendtime;
- (IBAction)btnstrttimeAction:(id)sender;
- (IBAction)btnEndTimeAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *availview;
@property (weak, nonatomic) IBOutlet UIView *topview;
@property (weak, nonatomic) IBOutlet UIView *leftview;
@property(nonatomic)NSMutableArray *arrSelected;
@property(nonatomic)DatepickerPop *popUp;
@property(nonatomic)LanguageVc *popUpLang;
@property (weak, nonatomic) IBOutlet UIButton *btnAddLang;
- (IBAction)btnAddLangAction:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *htConstraintView;
@property(nonatomic)NSString *strLang;
@property(nonatomic)NSString *strLangNmae;
@property(nonatomic)NSString *strSitterType;
@property(nonatomic)NSString *strJobId;
@property (weak, nonatomic) IBOutlet UITextField *txtLang;

@end

@implementation JobPostStep1
-(void)viewDidAppear:(BOOL)animated{
     self.htConstraintView.constant = 0;
    //left View
    NSMutableArray *arr = [[NSMutableArray alloc]initWithObjects:@"6AM-9PM",@"9AM-12PM",@"12AM-3PM",@"3PM-6PM",@"6PM-9PM",@"9PM-12AM",@"12AM-6AM", nil];
    CGFloat lpointOfx = 0.0;
    CGFloat lpointOfy = 0.0;
    for(int i =1; i<8;i++){
        UILabel *lblLeft = [[UILabel alloc]initWithFrame:CGRectMake(lpointOfx, lpointOfy, self.leftview.frame.size.width, 29)];
        lblLeft.textColor = [UIColor darkGrayColor];
        lblLeft.font =[UIFont fontWithName:@"OpenSans-Light" size:13.0];
        lblLeft.textAlignment = NSTextAlignmentRight;
        lblLeft.text = [arr objectAtIndex:i-1];
        [self.leftview addSubview:lblLeft];
        lpointOfy = lpointOfy + 29;
        
    }
    
    
    
    //Top View
    NSMutableArray *arr2 = [[NSMutableArray alloc]initWithObjects:@"S",@"M",@"T",@"W",@"T",@"F",@"S", nil];
    CGFloat tpointOfx = 0.0;
    CGFloat tpointOfy = 0.0;
    for(int i =1; i<=7;i++){
        UILabel *lbTop = [[UILabel alloc]initWithFrame:CGRectMake(tpointOfx, tpointOfy, 29 , 19)];
        if(i==1)
        {
            lbTop.textColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        }else{
            lbTop.textColor = [UIColor grayColor];
        }
        lbTop.font =[UIFont fontWithName:@"OpenSans-Light" size:13.0];
        
        lbTop.text = [arr2 objectAtIndex:i-1];
        lbTop.textAlignment = NSTextAlignmentCenter;
        [self.topview addSubview:lbTop];
        tpointOfx = tpointOfx + 29;
        
    }
    //availView
    
    
    CGFloat pointOfx = 0.0;
    CGFloat pointOfy = 0.0;
    
    for(int i=1; i<=7;i ++){
        for(int j=1; j<=7; j++){
            
            UIButton *small = [UIButton buttonWithType:UIButtonTypeCustom];
            small.frame =CGRectMake(pointOfx, pointOfy, 29, 29);
            small.backgroundColor = [UIColor clearColor];
            small.tag = btnTag + (i- 1)*7 + j;
            [small addTarget:self action:@selector(btnSmallAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.availview addSubview:small];
            
            
            
            [self.availview addSubview:small];
            
            //set border on four side
            CGSize mainViewSize = small.bounds.size;
            CGFloat borderWidth = 1;
            UIColor *borderColor = [UIColor lightGrayColor];
            UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, borderWidth, mainViewSize.height)];
            UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(mainViewSize.width - borderWidth, 0, borderWidth, mainViewSize.height)];
            
            UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainViewSize.width, borderWidth)];
            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0,mainViewSize.height - borderWidth,  mainViewSize.width, borderWidth)];
            leftView.opaque = YES;
            rightView.opaque = YES;
            topView.opaque = YES;
            bottomView.opaque = YES;
            leftView.backgroundColor = borderColor;
            rightView.backgroundColor = borderColor;
            topView.backgroundColor = borderColor;
            bottomView.backgroundColor = borderColor;
            // for bonus points, set the views' autoresizing mask so they'll stay with the edges:
            leftView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
            rightView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;
            topView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
            bottomView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
            if(j==1){
                
                [small addSubview:leftView];
                [small addSubview:rightView];
                [small addSubview:topView];
                
                
                if(i==7){
                    
                    [small addSubview:bottomView];
                    
                }
                
            }else {
                [small addSubview:rightView];
                [small addSubview:topView];
                if(i==7){
                    
                    [small addSubview:bottomView];
                    
                }
            }
            
            
            pointOfx = pointOfx + 29;
            
        }
        pointOfx=0;
        pointOfy = pointOfy + 29;
    }
}
-(void)btnSmallAction:(UIButton*)btn{
    NSString *strTag = [NSString stringWithFormat:@"%d",btn.tag];
    
    if ([[btn backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"check.png"]]){
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        [self.arrSelected removeObject:strTag];
        
    } else{
        [btn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        [self.arrSelected addObject:strTag];
    }
    
}
-(void)leftMenuclicked{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    for ( id subview in self.navigationController.view.subviews) {
        if ( [subview isKindOfClass:[UIButton class]] ) {
            //do your code
            UIButton *b = (UIButton*)subview;
            [b removeFromSuperview];
        }
    }
    self.navigationItem.hidesBackButton = YES;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.txtLang.leftView = paddingView;
     self.txtLang.leftViewMode = UITextFieldViewModeAlways;
     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.strSitterType = @"onetime";
     UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    self.htConstraintView.constant = 0;
    self.viewRecurring.hidden = YES;
    UIStoryboard *dash = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    self.popUp = [dash instantiateViewControllerWithIdentifier:@"DatepickerPop"];
    self.popUpLang = [dash instantiateViewControllerWithIdentifier:@"LanguageVc"];
    self.arrSelected = [[NSMutableArray alloc]init];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Post a Job";
    
    //Add back menu
    UIImage *buttonImageleft = [UIImage imageNamed:@"backicon"];
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLeft setImage:buttonImageleft forState:UIControlStateNormal];
    buttonLeft.frame = CGRectMake(0, 0, buttonImageleft.size.width, buttonImageleft.size.height);
    [buttonLeft addTarget:self action:@selector(leftMenuclicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonLeft];
    self.navigationItem.leftBarButtonItem = leftBarItem;

    
    transportFlag=1;
    nonsmokerFlag=1;
    carekidFlag=0;
    comfortkidFlag=0;
    twinFlag=1;
    spclneedFlag=1;
    
    creditFlag=1;
    cashFlag=1;
    checkFlag=0;
    paypalFlag=0;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"EEE.MMM dd hh:mm a"];
    self.lblstrtTime.text = [dateFormatter stringFromDate:[NSDate date]];
    NSDate *nextDate = [[NSDate date] dateByAddingTimeInterval:(24*3*3600)];
    self.lblendtime.text = [dateFormatter stringFromDate:nextDate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btnTransportAction:(id)sender {
    if(transportFlag==1){
        transportFlag=0;
        [self.btnTransport setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        transportFlag=1;
        [self.btnTransport setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnNonsmokerAction:(id)sender {
    if(nonsmokerFlag==1){
        nonsmokerFlag=0;
        [self.btnNonSmoker setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        nonsmokerFlag=1;
        [self.btnNonSmoker setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnCareKidAction:(id)sender {
    if(carekidFlag==1){
        carekidFlag=0;
        [self.btnCareKid setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        carekidFlag=1;
        [self.btnCareKid setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnComfortKidAction:(id)sender {
    if(comfortkidFlag==1){
        comfortkidFlag=0;
        [self.btnComfortWithKid setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        comfortkidFlag=1;
        [self.btnComfortWithKid setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnTwinAction:(id)sender {
    if(twinFlag==1){
        twinFlag=0;
        [self.btnTwin setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        twinFlag=1;
        [self.btnTwin setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnSpclAction:(id)sender {
    if(spclneedFlag==1){
        spclneedFlag=0;
        [self.btnSpclNeed setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        spclneedFlag=1;
        [self.btnSpclNeed setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnCreditAction:(id)sender {
    if(creditFlag==1){
        creditFlag=0;
        [self.btnCredit setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        creditFlag=1;
        [self.btnCredit setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnPaycheckAction:(id)sender {
    if(checkFlag==1){
        checkFlag=0;
        [self.btnPaycheck setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        checkFlag=1;
        [self.btnPaycheck setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnCashAction:(id)sender {
    if(cashFlag==1){
        cashFlag=0;
        [self.btncash setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        cashFlag=1;
        [self.btncash setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnPaypalAction:(id)sender {
    if(paypalFlag==1){
        paypalFlag=0;
        [self.btnPaypal setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        paypalFlag=1;
        [self.btnPaypal setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}

- (IBAction)btnNextAction:(id)sender {
    [self postJob];
    //[self performSegueWithIdentifier:@"seguejobpoststep2" sender:self];
}
- (void)postJob{
//    parent_id(type int),sitter_type(text ex: onetime or recuring),start_time(YYYY-mm-dd),end_time(same),todders_no(int),early_school_no(int),elementry_school_no(int),pre_teens_no(int),teens_no(int),own_transport(ex 0 or 1),non_smoker(ex 0 or 1),sick_kids(ex 0 or 1),comf_pets(0 or 1),twins(0 or 1),special_needs(0 or 1),languages(text),credit_card(0 or 1),cash(0 or 1),checks(0 or 1),paypal(0 or 1)
    
    
//    {
//        jobid = 1;
//        msg = "job post Successfully done";
//        "parent_id" = 2;
//        success = 1;
//    }
    [activity showView];
    self.strLang=@"2";
    NSString *strtTime = [self formatDateString:self.lblstrtTime.text];
    NSString *endTime = [self formatDateString:self.lblendtime.text];
    NSString *strJobInfo = [NSString stringWithFormat:@"parent_id=%@&sitter_type=%@&start_time=%@&end_time=%@&todders_no=%@&early_school_no=%@&elementry_school_no=%@&pre_teens_no=%@&teens_no=%@&own_transport=%d&non_smoker=%d&sick_kids=%d&comf_pets=%d&twins=%d&special_needs=%d&languages=%@&credit_card=%d&cash=%d&cash=%d&paypal=%d",self.strUser,self.strSitterType,strtTime,endTime,self.lblToddler.text,self.lblaerlyage.text,self.lblelementryage.text,self.lblPreteensage.text,self.lblteenager.text,transportFlag,nonsmokerFlag,carekidFlag,comfortkidFlag,twinFlag,spclneedFlag,self.strLang,creditFlag,cashFlag,checkFlag,paypalFlag ];
    
    if([UtiltiyManager isReachableToInternet]){
        [AppServiceManager PostJobStep1:strJobInfo onCompletion:^(id object, NSError *error) {
            if(object){
                NSInteger success = [[object valueForKey:@"success"]integerValue];
                if(success==1){
                    self.strUser = [object valueForKey:@"parent_id"];
                    self.strJobId = [object valueForKey:@"jobid"];
                    [activity hideView];
                    [self performSegueWithIdentifier:@"seguejobpoststep2" sender:self];
                }
            }else{
                
            }
        }];
    }else{
        [UtiltiyManager showAlertWithMessage:@"No Internet Connection"];
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"seguejobpoststep2"]) {
        JobPostStep2 *rs =segue.destinationViewController;
        rs.strUid = self.strUser;
        rs.strJobId = self.strJobId;
    }
}
//stepper action
- (IBAction)btnstepperPlusAction:(id)sender {
    UIButton *btn = (UIButton*)sender;
    if(btn.tag == 1000){
        NSInteger k = [self.lblToddler.text integerValue];
        k = k+1;
        self.lblToddler.text = [NSString stringWithFormat:@"%ld",(long)k];
    
    }else if (btn.tag == 2000){
        NSInteger k = [self.lblaerlyage.text integerValue];
        k = k+1;
        self.lblaerlyage.text = [NSString stringWithFormat:@"%ld",(long)k];
        
    }else if (btn.tag == 3000){
        NSInteger k = [self.lblelementryage.text integerValue];
        k = k+1;
        self.lblelementryage.text = [NSString stringWithFormat:@"%ld",(long)k];
        
    }else if (btn.tag == 4000){
        NSInteger k = [self.lblPreteensage.text integerValue];
        k = k+1;
        self.lblPreteensage.text = [NSString stringWithFormat:@"%ld",(long)k];
        
    }else if (btn.tag == 6000){
        NSInteger k = [self.lblteenager.text integerValue];
        k = k+1;
        self.lblteenager.text = [NSString stringWithFormat:@"%ld",(long)k];
        
    }
}

- (IBAction)btnStepperMinusAction:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    if(btn.tag == 1001){
        NSInteger k = [self.lblToddler.text integerValue];
        if(k!=0){
            k = k-1;
            self.lblToddler.text = [NSString stringWithFormat:@"%ld",(long)k];
        }
        
        
    }else if (btn.tag == 2001){
        NSInteger k = [self.lblaerlyage.text integerValue];
        if(k!=0){
        k = k-1;
        self.lblaerlyage.text = [NSString stringWithFormat:@"%ld",(long)k];
        }
        
    }else if (btn.tag == 3001){
        NSInteger k = [self.lblelementryage.text integerValue];
        if(k!=0){
        k = k-1;
        self.lblelementryage.text = [NSString stringWithFormat:@"%ld",(long)k];
        }
        
    }else if (btn.tag == 4001){
        NSInteger k = [self.lblPreteensage.text integerValue];
        if(k!=0){
        k = k-1;
        self.lblPreteensage.text = [NSString stringWithFormat:@"%ld",(long)k];
        }
        
    }else if (btn.tag == 6001){
        NSInteger k = [self.lblteenager.text integerValue];
        if(k!=0){
        k = k-1;
        self.lblteenager.text = [NSString stringWithFormat:@"%ld",(long)k];
        }
        
    }

}
- (IBAction)segmentAction:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    if(segmentedControl.selectedSegmentIndex == 0)
    {
        self.htConstraintView.constant = 0;
        self.strSitterType = @"onetime";
        
        
    }
    else if(segmentedControl.selectedSegmentIndex == 1)
    {
       
        self.htConstraintView.constant = 223;
        self.viewRecurring.hidden = NO;
        self.strSitterType = @"recuring";
    }
    
}
- (IBAction)btnstrttimeAction:(id)sender {
    [self.view addSubview:self.popUp.view];
    [self.popUp refreshUIWithDatasource:nil onCompletion:^(id object, NSUInteger eventPop) {
        if(eventPop==kActionDone){
            [self.popUp.view removeFromSuperview];
            self.lblstrtTime.text = object;
            
        }else{
            [self.popUp.view removeFromSuperview];
            
        }
    }];
}

- (IBAction)btnEndTimeAction:(id)sender {
    [self.view addSubview:self.popUp.view];
    [self.popUp refreshUIWithDatasource:nil onCompletion:^(id object, NSUInteger eventPop) {
        if(eventPop==kActionDone){
            [self.popUp.view removeFromSuperview];
            self.lblendtime.text = object;
            
        }else{
            [self.popUp.view removeFromSuperview];
            
        }
    }];

}

// Formats the date chosen with the date picker.
- (NSString *)formatDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"EEE,MM dd HH:mm z"];
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}
- (NSString *)formatDateString:(NSString *)strDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    [dateFormatter setDateFormat:@"EEE.MMM dd hh:mm a"];
    NSDate *formattedDate = [dateFormatter dateFromString:strDate];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *strDat = [dateFormatter stringFromDate:formattedDate];
    return strDat;
}
- (IBAction)btnAddLangAction:(id)sender {
    [self.view addSubview:self.popUpLang.view];
    self.navigationController.navigationBarHidden=YES;
    [self.popUpLang refreshUIWithDatasource:nil onCompletion:^(id object,id object1, NSUInteger eventPop) {
        if(eventPop==kActionSave){
            [self.popUpLang.view removeFromSuperview];
            self.navigationController.navigationBarHidden=NO;
            self.strLang = object;
            self.strLangNmae = object1;
            self.txtLang.text = self.strLangNmae;
        }else{
            [self.popUpLang.view removeFromSuperview];
             self.navigationController.navigationBarHidden=NO;
            
        }
    }];

}
@end
