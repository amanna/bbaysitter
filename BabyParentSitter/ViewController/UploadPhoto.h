//
//  UploadPhoto.h
//  BabySitterHawaii
//
//  Created by Amrita on 30/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POAcvityView.h"
#import "AppServiceManager.h"
@interface UploadPhoto : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,strong)NSString *strUser;
@property (strong,nonatomic)NSString *strUserType;
- (IBAction)plusBtnAction:(id)sender;

@end
