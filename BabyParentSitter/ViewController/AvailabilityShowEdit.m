//
//  AvailabilityShowEdit.m
//  BabyParentSitter
//
//  Created by Amrita on 31/05/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "AvailabilityShowEdit.h"
#define btnTag 5000
@interface AvailabilityShowEdit ()
@property (weak, nonatomic) IBOutlet UIView *topview;
@property (weak, nonatomic) IBOutlet UIView *leftview;
@property (weak, nonatomic) IBOutlet UIView *availview;
@property (weak, nonatomic) IBOutlet UIButton *btnsave;
- (IBAction)btnsaveaction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnclose;
- (IBAction)btncloseaction:(id)sender;
@property(nonatomic)NSMutableArray *arrSelected;
@end

@implementation AvailabilityShowEdit
-(void)viewDidAppear:(BOOL)animated{
    //left View
    NSMutableArray *arr = [[NSMutableArray alloc]initWithObjects:@"6AM-9PM",@"9AM-12PM",@"12AM-3PM",@"3PM-6PM",@"6PM-9PM",@"9PM-12AM",@"12AM-6AM", nil];
    CGFloat lpointOfx = 0.0;
    CGFloat lpointOfy = 0.0;
    for(int i =1; i<8;i++){
        UILabel *lblLeft = [[UILabel alloc]initWithFrame:CGRectMake(lpointOfx, lpointOfy, self.leftview.frame.size.width, 30)];
        lblLeft.textColor = [UIColor darkGrayColor];
        lblLeft.font =[UIFont fontWithName:@"OpenSans-Light" size:13.0];
        lblLeft.textAlignment = NSTextAlignmentRight;
        lblLeft.text = [arr objectAtIndex:i-1];
        [self.leftview addSubview:lblLeft];
        lpointOfy = lpointOfy + 30;
        
    }
    
    
    
    //Top View
    NSMutableArray *arr2 = [[NSMutableArray alloc]initWithObjects:@"S",@"M",@"T",@"W",@"T",@"F",@"S", nil];
    CGFloat tpointOfx = 0.0;
    CGFloat tpointOfy = 0.0;
    for(int i =1; i<=7;i++){
        UILabel *lbTop = [[UILabel alloc]initWithFrame:CGRectMake(tpointOfx, tpointOfy, 30 , 30)];
        if(i==1)
        {
            lbTop.textColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        }else{
            lbTop.textColor = [UIColor grayColor];
        }
        lbTop.font =[UIFont fontWithName:@"OpenSans-Light" size:13.0];
        
        lbTop.text = [arr2 objectAtIndex:i-1];
        lbTop.textAlignment = NSTextAlignmentCenter;
        [self.topview addSubview:lbTop];
        tpointOfx = tpointOfx + 30;
        
    }
    //availView
    
    
    CGFloat pointOfx = 0.0;
    CGFloat pointOfy = 0.0;
    
    for(int i=1; i<=7;i ++){
        for(int j=1; j<=7; j++){
            
            UIButton *small = [UIButton buttonWithType:UIButtonTypeCustom];
            small.frame =CGRectMake(pointOfx, pointOfy, 30, 30);
            small.backgroundColor = [UIColor clearColor];
            small.tag = btnTag + (i- 1)*7 + j;
            
            //
            NSString *string = @"hello bla blah";
            if ([string containsString:@"bla"]) {
                NSLog(@"string contains bla!");
            } else {
                NSLog(@"string does not contain bla");
            }
            
            
            
            if(isEdit==YES){
            [small addTarget:self action:@selector(btnSmallAction:) forControlEvents:UIControlEventTouchUpInside];
            }
            [self.availview addSubview:small];
            
            
            
            [self.availview addSubview:small];
            
            //set border on four side
            CGSize mainViewSize = small.bounds.size;
            CGFloat borderWidth = 1;
            UIColor *borderColor = [UIColor lightGrayColor];
            UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, borderWidth, mainViewSize.height)];
            UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(mainViewSize.width - borderWidth, 0, borderWidth, mainViewSize.height)];
            
            UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainViewSize.width, borderWidth)];
            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0,mainViewSize.height - borderWidth,  mainViewSize.width, borderWidth)];
            leftView.opaque = YES;
            rightView.opaque = YES;
            topView.opaque = YES;
            bottomView.opaque = YES;
            leftView.backgroundColor = borderColor;
            rightView.backgroundColor = borderColor;
            topView.backgroundColor = borderColor;
            bottomView.backgroundColor = borderColor;
            // for bonus points, set the views' autoresizing mask so they'll stay with the edges:
            leftView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
            rightView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;
            topView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
            bottomView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
            if(j==1){
                
                [small addSubview:leftView];
                [small addSubview:rightView];
                [small addSubview:topView];
                
                
                if(i==7){
                    
                    [small addSubview:bottomView];
                    
                }
                
            }else {
                [small addSubview:rightView];
                [small addSubview:topView];
                if(i==7){
                    
                    [small addSubview:bottomView];
                    
                }
            }
            
            
            pointOfx = pointOfx + 30;
            
        }
        pointOfx=0;
        pointOfy = pointOfy + 30;
    }
    
    
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrSelected = [[NSMutableArray alloc]init];
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.8];
    // Do any additional setup after loading the view.
}
-(void)btnSmallAction:(UIButton*)btn{
    NSString *strTag = [NSString stringWithFormat:@"%ld",(long)btn.tag];
    
    if ([[btn backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"check.png"]]){
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        [self.arrSelected removeObject:strTag];
        
    } else{
        [btn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        [self.arrSelected addObject:strTag];
    }
    
}
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandler)eventhandler{
    handler = eventhandler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnsaveaction:(id)sender {
    handler(nil,kActionSave);
}
- (IBAction)btncloseaction:(id)sender {
    handler(nil,kActionCncl);
}
@end
