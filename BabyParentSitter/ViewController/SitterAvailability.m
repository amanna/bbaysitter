//
//  SitterAvailability.m
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//
#define btnTag 5000
#import "SitterAvailability.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "SitterExperienceVC.h"
#import "UtiltiyManager.h"
@interface SitterAvailability (){
   
    POAcvityView *activity;
    NSInteger fulltimeFlag;
    NSInteger dateNightFlag;
    NSInteger weekendFlag;
    NSInteger bforeschoolFlag;
    NSInteger afterschoolFlag;
     NSInteger availableFlag;
    
}
- (IBAction)btnNextAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *avilView;
@property (weak, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UIButton *btnFullTime;
- (IBAction)btnFullTimeAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btndateNight;

- (IBAction)btndateNightAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnWeekend;
- (IBAction)btnWeekendAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBeforeSchool;

- (IBAction)btnBeforeSchoolAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnAfter;
- (IBAction)btnAfterschoolAction:(id)sender;

@property(nonatomic)NSMutableArray *arrSelected;

@property (weak, nonatomic) IBOutlet UIButton *btnavail;
- (IBAction)btnavailAction:(id)sender;


@end

@implementation SitterAvailability
-(void)viewDidAppear:(BOOL)animated{
    //left View
    NSMutableArray *arr = [[NSMutableArray alloc]initWithObjects:@"6AM-9PM",@"9AM-12PM",@"12AM-3PM",@"3PM-6PM",@"6PM-9PM",@"9PM-12AM",@"12AM-6AM", nil];
    CGFloat lpointOfx = 0.0;
    CGFloat lpointOfy = 0.0;
    for(int i =1; i<8;i++){
        UILabel *lblLeft = [[UILabel alloc]initWithFrame:CGRectMake(lpointOfx, lpointOfy, self.leftView.frame.size.width, 30)];
        lblLeft.textColor = [UIColor darkGrayColor];
        lblLeft.font =[UIFont fontWithName:@"OpenSans-Light" size:13.0];
        lblLeft.textAlignment = NSTextAlignmentRight;
        lblLeft.text = [arr objectAtIndex:i-1];
        [self.leftView addSubview:lblLeft];
        lpointOfy = lpointOfy + 30;
        
    }
    
    
    
    //Top View
    NSMutableArray *arr2 = [[NSMutableArray alloc]initWithObjects:@"S",@"M",@"T",@"W",@"T",@"F",@"S", nil];
    CGFloat tpointOfx = 0.0;
    CGFloat tpointOfy = 0.0;
    for(int i =1; i<=7;i++){
        UILabel *lbTop = [[UILabel alloc]initWithFrame:CGRectMake(tpointOfx, tpointOfy, 30 , 30)];
        if(i==1)
        {
            lbTop.textColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        }else{
            lbTop.textColor = [UIColor grayColor];
        }
        lbTop.font =[UIFont fontWithName:@"OpenSans-Light" size:13.0];
        
        lbTop.text = [arr2 objectAtIndex:i-1];
        lbTop.textAlignment = NSTextAlignmentCenter;
        [self.topView addSubview:lbTop];
        tpointOfx = tpointOfx + 30;
        
    }
    //availView
    
    
    CGFloat pointOfx = 0.0;
    CGFloat pointOfy = 0.0;
    
    for(int i=1; i<=7;i ++){
        for(int j=1; j<=7; j++){
            
            UIButton *small = [UIButton buttonWithType:UIButtonTypeCustom];
            small.frame =CGRectMake(pointOfx, pointOfy, 30, 30);
            small.backgroundColor = [UIColor clearColor];
            small.tag = btnTag + (i- 1)*7 + j;
            [small addTarget:self action:@selector(btnSmallAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.avilView addSubview:small];
            
            
            
            [self.avilView addSubview:small];
            
            //set border on four side
            CGSize mainViewSize = small.bounds.size;
            CGFloat borderWidth = 1;
            UIColor *borderColor = [UIColor lightGrayColor];
            UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, borderWidth, mainViewSize.height)];
            UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(mainViewSize.width - borderWidth, 0, borderWidth, mainViewSize.height)];
            
            UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainViewSize.width, borderWidth)];
            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0,mainViewSize.height - borderWidth,  mainViewSize.width, borderWidth)];
            leftView.opaque = YES;
            rightView.opaque = YES;
            topView.opaque = YES;
            bottomView.opaque = YES;
            leftView.backgroundColor = borderColor;
            rightView.backgroundColor = borderColor;
            topView.backgroundColor = borderColor;
            bottomView.backgroundColor = borderColor;
            // for bonus points, set the views' autoresizing mask so they'll stay with the edges:
            leftView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
            rightView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;
            topView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
            bottomView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
            if(j==1){
                
                [small addSubview:leftView];
                [small addSubview:rightView];
                [small addSubview:topView];
                
                
                if(i==7){
                    
                    [small addSubview:bottomView];
                    
                }
                
            }else {
                [small addSubview:rightView];
                [small addSubview:topView];
                if(i==7){
                    
                    [small addSubview:bottomView];
                    
                }
            }
            
            
            pointOfx = pointOfx + 30;
            
        }
        pointOfx=0;
        pointOfy = pointOfy + 30;
    }
    
    
    
    

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrSelected = [[NSMutableArray alloc]init];
    fulltimeFlag=1;
    dateNightFlag=1;
    weekendFlag=0;
    bforeschoolFlag=0;
    afterschoolFlag=0;
    availableFlag = 0;
    
    
    
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];

    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Availability";    // Do any additional setup after loading the view.
}

-(void)btnSmallAction:(UIButton*)btn{
    NSString *strTag = [NSString stringWithFormat:@"%d",btn.tag];
    
    if ([[btn backgroundImageForState:UIControlStateNormal] isEqual:[UIImage imageNamed:@"check.png"]]){
        [btn setBackgroundImage:nil forState:UIControlStateNormal];
        [self.arrSelected removeObject:strTag];
        
    } else{
        [btn setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
        [self.arrSelected addObject:strTag];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnNextAction:(id)sender {
    //available_fulltime(type bool),available_nights(type bool),available_weekends(type bool),available_before_school(type bool),available_after_school(type bool),available_for_backup(type bool),userid(type=int),sitter_available(type text)
    
    NSString *strAvailable= [self.arrSelected componentsJoinedByString:@","];
    
   
    if ([UtiltiyManager isReachableToInternet]) {
        [activity showView];
        NSString *strPayment = [NSString stringWithFormat:@"available_fulltime=%ld&available_nights=%ld&available_weekends=%ld&available_before_school=%ld&available_after_school=%ld&available_for_backup=%ld&userid=%@&sitter_available=%@",(long)fulltimeFlag,(long)dateNightFlag,(long)weekendFlag,(long)bforeschoolFlag,(long)afterschoolFlag,(long)availableFlag,self.strUser,strAvailable];
        [AppServiceManager updateAvailability:strPayment onCompletion:^(id object, NSError *error) {
            [activity hideView];
            if(object){
                NSDictionary *dict = object;
                NSInteger success =[[dict valueForKey:@"success"]integerValue];
                if(success==1){
                    [self performSegueWithIdentifier:@"segueExp" sender:self];
                }
                
            }else{
                
            }
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"segueExp"]) {
        SitterExperienceVC *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.strUser = self.strUser;
    }
}


- (IBAction)btnFullTimeAction:(id)sender {
    if(fulltimeFlag==1){
        fulltimeFlag=0;
        [self.btnFullTime setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        fulltimeFlag=1;
        [self.btnFullTime setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btndateNightAction:(id)sender {
    if(dateNightFlag==1){
        dateNightFlag=0;
        [self.btndateNight setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        dateNightFlag=1;
        [self.btndateNight setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btnWeekendAction:(id)sender {
    if(weekendFlag==1){
        weekendFlag=0;
        [self.btnWeekend setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        weekendFlag=1;
        [self.btnWeekend setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btnBeforeSchoolAction:(id)sender {
    if(bforeschoolFlag==1){
        bforeschoolFlag=0;
        [self.btnBeforeSchool setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        bforeschoolFlag=1;
        [self.btnBeforeSchool setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btnAfterschoolAction:(id)sender {
    if(afterschoolFlag==1){
        afterschoolFlag=0;
        [self.btnAfter setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        afterschoolFlag=1;
        [self.btnAfter setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btnavailAction:(id)sender {
    if(availableFlag==1){
        availableFlag=0;
        [self.btnavail setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        availableFlag=1;
        [self.btnavail setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
@end
