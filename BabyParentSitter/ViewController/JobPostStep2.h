//
//  JobPostStep2.h
//  BabySitterHawaii
//
//  Created by Amrita on 09/05/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobPostStep2 : UIViewController
@property (strong,nonatomic)NSString *strUserType;
@property (strong,nonatomic)NSString *strUid;
@property (strong,nonatomic)NSString *strJobId;
@end
