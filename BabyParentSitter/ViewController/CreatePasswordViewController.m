//
//  CreatePasswordViewController.m
//  BabySitterHawaii
//
//  Created by debashisandria on 21/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "CreatePasswordViewController.h"
#import "AppServiceManager.h"
#import "POAcvityView.h"
#import "PopUpVC.h"
#import "AddrParentCountVC.h"
#import "AppDelegate.h"
@interface CreatePasswordViewController () {
    BOOL genderFlag;
    POAcvityView *activity;
    NSString *strDob;
      AppDelegate *appdelegate;
    
}
@property(nonatomic)PopUpVC *popUp;
@property(nonatomic) NSString *strDob;
@end

@implementation CreatePasswordViewController
@synthesize email, firstName, lastName, bday, password, btnMale, btnFemale;
- (void)viewDidLoad {
    [super viewDidLoad];
     appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    self.popUp = [main instantiateViewControllerWithIdentifier:@"PopUpVC"];

     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Create Account";

    
    //Add back menu
    UIImage *buttonImageleft = [UIImage imageNamed:@"backN"];
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLeft setImage:buttonImageleft forState:UIControlStateNormal];
    buttonLeft.frame = CGRectMake(0, 0, 45,20);
    [buttonLeft addTarget:self action:@selector(leftMenuclicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonLeft];
    self.navigationItem.leftBarButtonItem = leftBarItem;

    // Do any additional setup after loading the view.
    email.text = self._email;
    firstName.text = self._firstName;
    lastName.text = self._lastName;
    bday.text = self._bday;
    
    if([self._gender  isEqual: @"male"]){
        [self toggleMaleFemale];
        genderFlag = 1;
    } else {
        genderFlag = 0;
        [self toggleMaleFemale];
        
    }
    
    
}
- (void)leftMenuclicked{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnNextAction:(id)sender {
    [self methodRegistration];
}
- (void)convetdateToString{
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [dateFormat dateFromString:self.bday.text];
    NSDateFormatter *dateFormat1 = [[NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    self.strDob = [dateFormat1 stringFromDate:date];
    
    
}

- (void)methodRegistration{
    [self convetdateToString];
        [activity showView];
        if([self.strUserType isEqualToString:@"parent"]){
            [AppServiceManager registrationParentFB:self.email.text andFirstname:self.firstName.text andLastname:self.lastName.text andPassword:self.password.text andDob:self.strDob andFB:self.fbId onCompletion:^(id object, NSError *error) {
                [activity hideView];
                if(object){
                    NSDictionary *dict = object;
                    NSInteger success =[[dict valueForKey:@"success"]integerValue];
                    if(success==1){
                        [self.view addSubview:self.popUp.view];
                        self.strUid = [dict valueForKey:@"userid"];
                        appdelegate.strUserId = [dict valueForKey:@"userid"];
                        appdelegate.strUserName = [NSString stringWithFormat:@"%@ %@",self.firstName.text,self.lastName.text];
                        appdelegate.strUserType = @"Parent";
                        appdelegate.strUserImage = self.imageUrl;

                        
                        [self.popUp refreshUIWithDatasource:nil onCompletion:^(id object, NSUInteger eventPop) {
                            if(eventPop==kActionOk){
                                [self.popUp.view removeFromSuperview];
                                [self performSegueWithIdentifier:@"mapsegue" sender:self];
                            }else{
                                [self.popUp.view removeFromSuperview];
                                
                            }
                        }];
                    }else if (success==2){
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"This Email is already in use" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];

                    }
                    
                }else{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some problem occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
                
            }];
        }else{
            NSString *strGender;
            if(genderFlag==1){
                strGender = @"male";
            }else{
                strGender = @"female";
            }
            [AppServiceManager registrationSitterFB:self.email.text andFirstname:self.firstName.text andLastname:self.lastName.text andPassword:self.password.text andDob:self.strDob  andGender:strGender andFB:self.fbId onCompletion:^(id object, NSError *error) {
                [activity hideView];
                if(object){
                    NSDictionary *dict = object;
                    NSInteger success =[[dict valueForKey:@"success"]integerValue];
                    if(success==1){
                        //Pop Up appear
                        [self.view addSubview:self.popUp.view];
                        self.strUid = [dict valueForKey:@"userid"];
                        
                        appdelegate.strUserId = [dict valueForKey:@"userid"];
                        appdelegate.strUserName = [NSString stringWithFormat:@"%@ %@",self.firstName.text,self.lastName.text];
                        
                        appdelegate.strUserType = @"Sitter";
                        appdelegate.strUserImage = self.imageUrl;
                        
                        [self.popUp refreshUIWithDatasource:nil onCompletion:^(id object, NSUInteger eventPop) {
                            if(eventPop==kActionOk){
                                [self.popUp.view removeFromSuperview];
                                [self performSegueWithIdentifier:@"mapsegue" sender:self];
                            }else{
                                [self.popUp.view removeFromSuperview];
                                
                            }
                        }];
                    }else if (success==2){
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"This Email is already in use" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                        [alert show];
                    }
                    
                }else{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some problem occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
                
            }];
        }
        
    
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"mapsegue"]) {
        AddrParentCountVC *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.strUid = self.strUid;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)viewDidAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}



- (IBAction)btnMaleAction:(id)sender {
     genderFlag = 1;
    [self toggleMaleFemale];
   
    
    
}

- (IBAction)btnFemaleAction:(id)sender {
     genderFlag = 0;
    [self toggleMaleFemale];
   
    
}

- (void)toggleMaleFemale {
    [btnMale setBackgroundImage:[UIImage imageNamed:@"male"] forState:UIControlStateNormal];
    [btnFemale setBackgroundImage:[UIImage imageNamed:@"female"] forState:UIControlStateNormal];
    
    if (genderFlag) {
        
        [btnMale setBackgroundImage:[UIImage imageNamed:@"male_checked"] forState:UIControlStateNormal];
    } else {
       
        [btnFemale setBackgroundImage:[UIImage imageNamed:@"female_checked"] forState:UIControlStateNormal];
    }
}

-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0)
    {
        [self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    NSLog(@"%f",sender.frame.origin.y);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self setViewMovedUp:NO];
    return true;
}
- (IBAction)btnTermAction:(id)sender {
}

- (IBAction)btnPrivacyAction:(id)sender {
}
@end
