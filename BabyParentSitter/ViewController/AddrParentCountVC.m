//
//  AddrParentCountVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "AddrParentCountVC.h"
#import "SitterPaymentVC.h"
#import "MyCustomAnnotation.h"
#import <CoreLocation/CoreLocation.h>
#import "AppServiceManager.h"
#import "POAcvityView.h"
#import "UtiltiyManager.h"
#import "ParentMenuVC.h"
@interface AddrParentCountVC ()<MKMapViewDelegate>{
     CLLocationManager *locationManager;
    POAcvityView *activity;
}
- (IBAction)btnnextAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UILabel *lblUserType;
@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) NSString *strLatt;
@property (strong, nonatomic) NSString *strLongi;

@end

@implementation AddrParentCountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.lblCount.text = self.strCount;
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Home Address";
    
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
   
  if([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
      [locationManager requestWhenInUseAuthorization];
      // Or [self.locationManager requestWhenInUseAuthorization];
  }
    if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
        // Or [self.locationManager requestWhenInUseAuthorization];
    }
   [locationManager startUpdatingLocation];
    
    
    if ( locationManager.location.coordinate.latitude != 0 && locationManager.location.coordinate.longitude != 0 )
    {
        self.strLatt = [NSString stringWithFormat:@"%.8f",locationManager.location.coordinate.latitude];
        self.strLongi = [NSString stringWithFormat:@"%.8f",locationManager.location.coordinate.longitude];

        
    }else{
        self.strLatt = @"21.282778";
        self.strLongi = @"-157.829444";
    }
        _mapView.delegate = self;
    [locationManager stopUpdatingLocation];
    [self findAddrss];

    // Do any additional setup after loading the view.
}
// MKMapViewDelegate Methods

- (void)findAddrss{
    if ([UtiltiyManager isReachableToInternet]) {
    [activity showView];
    [AppServiceManager findAddress:self.strLatt andLon:self.strLongi onCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            NSDictionary *dict = object;
            NSInteger success =[[dict valueForKey:@"success"]integerValue];
            if(success==1){
                self.txtAddress.text = [dict valueForKey:@"address"];
                
                [AppServiceManager updateAddress:self.strUid andAddress:self.txtAddress.text andLatt:self.strLatt andLong:self.strLongi onCompletion:^(id object, NSError *error) {
                       [activity hideView];
                       if(object){
                           NSDictionary *dict = object;
                           NSInteger success =[[dict valueForKey:@"success"]integerValue];
                           if(success==1){
                               self.strCount = [dict valueForKey:@"nearby"];
                               
                           }
                       }else{
                           NSLog(@"error=%@",error.debugDescription);
                       }
                
                   }];

                }
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some problem occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            
        }
    }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
        
    }
    //map update
    
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = locationManager.location.coordinate.latitude;
    zoomLocation.longitude= locationManager.location.coordinate.longitude;
    
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    
    /*MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
     point.coordinate = newLocation.coordinate;
     point.title = @"Where am I?";
     point.subtitle = @"I'm here!!!";*/
    
    MyCustomAnnotation *point = [[MyCustomAnnotation alloc]initWithLocation:self.location.coordinate];
    
    
    [self.mapView addAnnotation:point];
    
    [locationManager stopUpdatingLocation];
}
- (IBAction)btnGpsClickedAction:(id)sender {
    [locationManager startUpdatingLocation];
    self.strLatt = [NSString stringWithFormat:@"%.8f",locationManager.location.coordinate.latitude];
    self.strLongi = [NSString stringWithFormat:@"%.8f",locationManager.location.coordinate.longitude];
    _mapView.delegate = self;
    [self findAddrss];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.location = locations.lastObject;
    NSLog(@"%@", locations);
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = self.location.coordinate.latitude;
    zoomLocation.longitude= self.location.coordinate.longitude;

    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    
    /*MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
     point.coordinate = newLocation.coordinate;
     point.title = @"Where am I?";
     point.subtitle = @"I'm here!!!";*/
    
    MyCustomAnnotation *point = [[MyCustomAnnotation alloc]initWithLocation:self.location.coordinate];
    
    
    [self.mapView addAnnotation:point];
    
    [locationManager stopUpdatingLocation];
    

}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = newLocation.coordinate.latitude;
    zoomLocation.longitude= newLocation.coordinate.longitude;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    
    /*MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
     point.coordinate = newLocation.coordinate;
     point.title = @"Where am I?";
     point.subtitle = @"I'm here!!!";*/
    
    MyCustomAnnotation *point = [[MyCustomAnnotation alloc]initWithLocation:newLocation.coordinate];
    
    
    [self.mapView addAnnotation:point];
    
    [locationManager stopUpdatingLocation];
    
    
    
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MyCustomAnnotation class]])
    {
        static NSString *reuseId = @"ann";
        MKAnnotationView *av = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
        if (av == nil)
        {
            av = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        }
        else
        {
            av.annotation = annotation;
        }
        
        
        av.image = [UIImage imageNamed:@"mappin"];
        av.canShowCallout= NO;
        
        /*UIView *vw = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
         vw.backgroundColor = [UIColor redColor];
         UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 50, 50)];
         label.numberOfLines = 4;
         label.text = @"hello\nhow are you\nfine";
         [vw addSubview:label];
         av.leftCalloutAccessoryView = vw;*/
        return av;
    }
    
    //return nil (default view) if annotation is not our custom type
    return nil;
}
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    if(![view.annotation isKindOfClass:[MKUserLocation class]]) {
        /*CGSize  calloutSize = CGSizeMake(100.0, 80.0);
         UIView *calloutView = [[UIView alloc] initWithFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y-calloutSize.height, calloutSize.width, calloutSize.height)];
         calloutView.backgroundColor = [UIColor whiteColor];
         UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
         button.frame = CGRectMake(5.0, 5.0, calloutSize.width - 10.0, calloutSize.height - 10.0);
         [button setTitle:@"OK" forState:UIControlStateNormal];
         [button addTarget:self action:@selector(checkin) forControlEvents:UIControlEventTouchUpInside];
         [calloutView addSubview:button];
         [view.superview addSubview:calloutView];*/
        
        UIView *firstViewUIView = [[[NSBundle mainBundle] loadNibNamed:@"MapAnnotationCallout" owner:self options:nil] firstObject];
        firstViewUIView.frame = CGRectMake((-firstViewUIView.frame.size.width / 2) + 10, -firstViewUIView.frame.size.height-10, 190, 110);
        UILabel * label;
        UIView *sc= (UIView *)[firstViewUIView viewWithTag:500];
        for (NSObject *view in sc.subviews)
        {
            if ([view isKindOfClass:[UIView class]])
            {
                label = (UILabel *)[sc viewWithTag:400];
                NSLog(@"%@",label.text);
                label.text = [NSString stringWithFormat:@"%@",self.strCount];
                //here you get your label
            }
           
        }
        [view addSubview:firstViewUIView];
        
    }
    
}

- (IBAction)btnnextAction:(id)sender {
    if([self.strUserType isEqualToString:@"parent"]){
        [self performSegueWithIdentifier:@"segueParentDas" sender:self];
    }else{
       [self performSegueWithIdentifier:@"seguePayment" sender:self];
    }
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"seguePayment"]) {
        SitterPaymentVC *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.strUser = self.strUid;
    }else{
        ParentMenuVC *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.struid = self.strUid;
    }
}
- (void)viewWillAppear:(BOOL)animated {
    
//    // 1
//    CLLocationCoordinate2D zoomLocation;
//    zoomLocation.latitude = 39.281516;
//    zoomLocation.longitude= -76.580806;
//    
//    // 2
//    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
//    
//    // 3
//    [_mapView setRegion:viewRegion animated:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.txtAddress resignFirstResponder];
    return true;
}

@end
