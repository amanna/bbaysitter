//
//  LoginVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "LoginVC.h"
#import "CTextField.h"
#import "AppServiceManager.h"
#import "POAcvityView.h"
#import "UtiltiyManager.h"
#import "DashboardVCSitter.h"
#import "DashboardVCParent.h"
#import "AppDelegate.h"
@interface LoginVC (){
    POAcvityView *activity;
    AppDelegate *appdelegate;
}
- (IBAction)btnFbLoginAction:(id)sender;
@property (weak, nonatomic) IBOutlet CTextField *txtEmail;
@property (weak, nonatomic) IBOutlet CTextField *txtPass;
- (IBAction)btnSubmitAction:(id)sender;
- (IBAction)btnPassClickAction:(id)sender;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
     self.navigationController.navigationBarHidden = NO;
     self.navigationItem.hidesBackButton = YES;
    self.navigationItem.title = @"Login";
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    [self.txtEmail becomeFirstResponder];
    self.txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtPass.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtEmail.leftPadding = 10.0;
    self.txtPass.leftPadding = 10.0;
    
    //Add back menu
    UIImage *buttonImageleft = [UIImage imageNamed:@"backN"];
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLeft setImage:buttonImageleft forState:UIControlStateNormal];
    buttonLeft.frame = CGRectMake(0, 0, 45, 20);
    [buttonLeft addTarget:self action:@selector(leftMenuclicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonLeft];
    self.navigationItem.leftBarButtonItem = leftBarItem;

    // Do any additional setup after loading the view.
}
- (void)leftMenuclicked{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField==self.txtEmail){
        [self.txtPass becomeFirstResponder];
        [self.txtEmail resignFirstResponder];
    }else{
        [self.txtPass resignFirstResponder];
        
    }
    
    return true;
}

- (IBAction)btnFbLoginAction:(id)sender {
     [activity showView];
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        
        // If the session state is not any of the two "open" states when the button is clicked
    } else {
        // Open a session showing the user the login UI
        // You must ALWAYS ask for public_profile permissions when opening a session
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email",@"user_location",@"user_birthday"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error) {
             
             NSLog(@"Logging in");
             
             [FBRequestConnection startWithGraphPath:@"/me"
                                          parameters:nil
                                          HTTPMethod:@"GET"
                                   completionHandler:^(
                                                       FBRequestConnection *connection,
                                                       NSDictionary *result,
                                                       NSError *error
                                                       ) {
                                       /* handle the result */
                                       NSLog(@"%@",result);
                                       
                                       NSString *fbId = [result objectForKey:@"id"];
                                       [AppServiceManager loginWithFB:fbId onCompletion:^(id object, NSError *error) {
                                            [activity hideView];
                                           if(object){
                                               NSDictionary *dict = object;
                                               NSInteger success =[[dict valueForKey:@"success"]integerValue];
                                               if(success==1){
                                                   self.strUid = [dict valueForKey:@"userid"];
                                                   appdelegate.strUserId = [dict valueForKey:@"userid"];
                                                   appdelegate.strUserName = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"User_Firstname"],[dict valueForKey:@"User_Lastname"]];
                                                   appdelegate.strUserImage = [dict valueForKey:@"profile_image"];
                                                   appdelegate.strUserType = [dict valueForKey:@"usertype"];
                                                   
                                                   UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
                                                   
                                                   if([appdelegate.strUserType isEqualToString:@"Parent"]){
                                                       DashboardVCParent *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVCParent"];
                                                       dashboard.strUid = self.strUid;
                                                       [self showViewController:dashboard sender:self];
                                                   }else{
                                                       DashboardVCSitter *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVC"];
                                                       dashboard.strUid = self.strUid;
                                                       [self showViewController:dashboard sender:self];
                                                   }
                                                   
                                               }else{
                                                   UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Invalid User Name Or Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                                   [alert show];
                                                   
                                               }
                                           }else{
                                               UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                                               [alert show];
                                           }

                                       }];
                                       
                                       
                                   }];
             
             
         }];
    }

}
- (IBAction)btnSubmitAction:(id)sender {
    if([self.txtEmail.text isEqualToString:@""]){
        [UtiltiyManager showAlertWithMessage:@"Please enter Email"];
    }else if ([self.txtPass.text isEqualToString:@""]){
         [UtiltiyManager showAlertWithMessage:@"Please enter Password"];
    }else{
    if ([UtiltiyManager isReachableToInternet]) {
    [activity showView];
    [AppServiceManager loginWith:self.txtEmail.text andPass:self.txtPass.text onCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            NSDictionary *dict = object;
            
            NSInteger success =[[dict valueForKey:@"success"]integerValue];
            if(success==1){
               
                self.strUid = [dict valueForKey:@"userid"];
                appdelegate.strUserId = self.strUid;
                appdelegate.strUserName = [NSString stringWithFormat:@"%@ %@",[dict valueForKey:@"User_Firstname"],[dict valueForKey:@"User_Lastname"]];
                appdelegate.strUserImage = [dict valueForKey:@"profile_image"];
                  appdelegate.strUserType = [dict valueForKey:@"usertype"];
  
                UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
                
                if([appdelegate.strUserType isEqualToString:@"Parent"]){
                    DashboardVCParent *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVCParent"];
                    dashboard.strUid = self.strUid;
                    [self showViewController:dashboard sender:self];
                }else{
                    DashboardVCSitter *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVC"];
                    dashboard.strUid = self.strUid;
                    [self showViewController:dashboard sender:self];
                }

            }else{
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Invalid User Name Or Password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];

            }
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
        }
    }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];
    }
    }
  
    
//    self.strUid = @"10";
//    self.strUSerType = @"Sitter";
//    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
//    
//    if([self.strUSerType isEqualToString:@"Parent"]){
//        DashboardVCParent *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVCParent"];
//        dashboard.strUid = self.strUid;
//        [self showViewController:dashboard sender:self];
//    }else{
//        DashboardVCSitter *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVC"];
//         dashboard.strUid = self.strUid;
//         [self showViewController:dashboard sender:self];
//    }
    
    
   

}

- (IBAction)btnPassClickAction:(id)sender {
    [self performSegueWithIdentifier:@"seguePass" sender:self];
}
@end
