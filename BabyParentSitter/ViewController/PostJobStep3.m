//
//  ViewController.m
//  Screen2
//
//  Created by Star Mobileoid2 Technologies  on 24/05/15.
//  Copyright (c) 2015 Naresh Jain. All rights reserved.
//

#import "PostJobStep3.h"
#import "HeartCell.h"
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
#import "Additional.h"
#import "POAcvityView.h"
#import "Additional.h"
#import "UploadPhoto.h"
@interface PostJobStep3 (){
    NSArray *arrDataSrc;
    NSMutableDictionary *imageName;
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *localRate;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property(nonatomic)NSString *baseHourly;
@property(nonatomic)NSMutableArray *arrSelected;
@end

@implementation PostJobStep3

- (IBAction)detailButtonAction:(id)sender {
}

- (void)viewDidLoad {
    self.arrSelected = [[NSMutableArray alloc]init];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.colletionView.userInteractionEnabled = YES;
    
//    arrDataSrc = [NSArray arrayWithObjects:@"Tutoring", @"Transportation", @"Laundry", @"Cooking", @"Errands", @"School", @"Cleaning", @"Pet Care", @"Tutoring", @"Transportation", @"Laundry", @"Cooking", @"Errands", @"School", @"Cleaning", @"Pet Care", nil];
  
    imageName = [[NSMutableDictionary alloc]init];
    
    UIImage *buttonImage = [UIImage imageNamed:@"backicon"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(leftMenuclicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = leftBarItem;
     NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    UITapGestureRecognizer *lpgr
    = [[UITapGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress)];
    lpgr.cancelsTouchesInView = NO;
    [self.colletionView addGestureRecognizer:lpgr];
    
    //
//    {"zipcode_data":[{"id":"2","zip":"11","base_hourly":"12","start_time":"00:00:00","end_time":"00:00:00","hourly_increase":"12","total":"12","service_id":"2"}],"userid":"1"}
//    
    //Find Additional services and Base hourly rate
    [activity showView];
    if([UtiltiyManager isReachableToInternet]){
    [AppServiceManager AdditionalService:^(id object, NSError *error) {
        if(object){
            arrDataSrc = object;
            [self.colletionView reloadData];
            [AppServiceManager baseHourlyRate:self.strUser onCompletion:^(id object, NSError *error) {
                if(object){
                    NSMutableArray *arr = [object valueForKey:@"zipcode_data"];
                    NSMutableArray *dict = [arr objectAtIndex:0];
                    self.baseHourly = [dict valueForKey:@"base_hourly"];
                  //  $ 12 / Hour
                    self.priceLabel.text = [NSString stringWithFormat:@"$ %@ / Hour",self.baseHourly];
                    [activity hideView];
                    
                }
            }];
            
        }
    }];
    }else{
        [UtiltiyManager showAlertWithMessage:@"No Internet connection"];
    }
   
    
    [super viewDidLoad];
}
- (void)handleLongPress{
    
}
-(void)leftMenuclicked{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrDataSrc count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    HeartCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Additional *additional = [arrDataSrc objectAtIndex:indexPath.row];
    BOOL isFound = NO;
    cell.lblheartTitle.text= additional.strSName;
    if(self.arrSelected.count > 0){
              for(Additional *add in self.arrSelected){
            if(add == additional){
                isFound = YES;
                break;
            }
        }
    }
    
    if(isFound==NO){
        cell.imgheart.image = [UIImage imageNamed:@"offheart"];
    }else{
        cell.imgheart.image = [UIImage imageNamed:@"heart"];
    }
    
//    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
//    UILabel *label = (UILabel *)[cell viewWithTag:101];
    
//    BOOL yesKey = [[imageName objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]] boolValue];
//
//    if(yesKey == YES)
//    {
//    imageView.image = [UIImage imageNamed:@"heart"];
//    }
//    
//    else{
//    imageView.image = [UIImage imageNamed:@"offheart"];
//    }
//    
//    label.text = arrDataSrc[indexPath.row];
    cell.userInteractionEnabled = YES;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    Additional *add = [arrDataSrc objectAtIndex:indexPath.row];
    BOOL isFound = NO;
    if(self.arrSelected.count > 0){
        for (int i=0; i<self.arrSelected.count; i++) {
            Additional *add1 = [self.arrSelected objectAtIndex:i];
            if(add1 == add){
                isFound=YES;
                break;
            }
        }
    }
    if(isFound==NO){
        [self.arrSelected addObject:add];
    }else{
       [self.arrSelected removeObject:add];
    }
    [self.colletionView reloadData];
       // [self.colletionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
}
- (IBAction)nextButtonAction:(id)sender {
    NSString *strAdditional;
    NSMutableArray *arrAdditional = [[NSMutableArray alloc]init];
    for (int i=0; i< self.arrSelected.count;i++) {
        Additional *sdd = [self.arrSelected objectAtIndex:i];
        [arrAdditional addObject:sdd.strSid];
    }
    strAdditional = [arrAdditional componentsJoinedByString:@","];
    if([UtiltiyManager isReachableToInternet]){
    [AppServiceManager updateHourlyRate:self.strUser withAdditional:strAdditional andJobId:self.strJobId onCompletion:^(id object, NSError *error) {
        if(object){
            [self performSegueWithIdentifier:@"uploadPhotoParent" sender:self];
        }
    }];
    }else{
        [UtiltiyManager showAlertWithMessage:@"NO Internet connection"];
    }
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"uploadPhotoParent"]) {
        UploadPhoto *rs =segue.destinationViewController;
        rs.strUser = self.strUser;
        rs.strUserType = @"parent";
    }
}
@end