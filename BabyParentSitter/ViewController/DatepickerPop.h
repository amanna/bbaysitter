//
//  DatepickerPop.h
//  BabyParentSitter
//
//  Created by Amrita on 03/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypePopAction{
    kActionDone=0,
    kActionCancel=1
}eventPop;
typedef void (^EventCompletionHandler)(id object,NSUInteger eventPop);
@interface DatepickerPop : UIViewController{
    EventCompletionHandler handler;
}
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandler)eventhandler;
@end
