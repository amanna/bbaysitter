//
//  PopUpBio.h
//  BabyParentSitter
//
//  Created by Amrita on 30/08/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypePopBio{
   kActionClose=1
}eventPopBio;
typedef void (^EventCompletionHandler)(id object,NSUInteger eventPopBio);

@interface PopUpBio : UIViewController{
     EventCompletionHandler handler;
}
- (void)refreshUIWithDatasource:(id)datasource onCompletion:(EventCompletionHandler)eventhandler;
@end
