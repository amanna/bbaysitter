//
//  SingleCell.h
//  BabyParentSitter
//
//  Created by Amrita on 26/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblLeft;
@property (weak, nonatomic) IBOutlet UILabel *lblRight;

@end
