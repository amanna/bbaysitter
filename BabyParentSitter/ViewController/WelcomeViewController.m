//
//  WelcomeViewController.m
//  BabySitterHawaii
//
//  Created by debashisandria on 17/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "WelcomeViewController.h"
#import <QuartzCore/QuartzCore.h>
@interface WelcomeViewController ()

@end

@implementation WelcomeViewController
@synthesize name, image_url, age, address, gender;
@synthesize profile_image, lbldesc, lblName;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Welcome!";
    
    //Add back menu
    UIImage *buttonImageleft = [UIImage imageNamed:@"backN"];
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLeft setImage:buttonImageleft forState:UIControlStateNormal];
    buttonLeft.frame = CGRectMake(0, 0, 45, 20);
    [buttonLeft addTarget:self action:@selector(leftMenuclicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonLeft];
    self.navigationItem.leftBarButtonItem = leftBarItem;

    
    profile_image.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:image_url]]];
    
    profile_image.clipsToBounds = YES;
    profile_image.layer.cornerRadius = 15.0;
    profile_image.layer.borderWidth = 8.0;
    profile_image.layer.borderColor = [UIColor whiteColor].CGColor;
    lblName.text = name;
   gender = [gender capitalizedString];
    lbldesc.text = [NSString stringWithFormat:@"%@, %@ - %@", gender, age, address];
    
}
- (void)leftMenuclicked{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnSitterAction:(id)sender {
   self.strUserType = @"sitter";
    [self performSegueWithIdentifier:@"createpassword" sender:self];
}
- (IBAction)btnParentAction:(id)sender {
    self.strUserType = @"parent";
    [self performSegueWithIdentifier:@"createPasswordParent" sender:self];
}

- (IBAction)logout:(id)sender {
    [FBSession.activeSession closeAndClearTokenInformation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)prefersStatusBarHidden {
    return NO;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"createpassword"]) {
        CreatePasswordViewController *dest = [segue destinationViewController];
        dest.strUserType = self.strUserType;
        dest._email = self.email;
        dest._firstName = self.firstName;
        dest._lastName = self.lastName;
        dest._bday = self.bday;
        dest._gender = self.gender;
        dest.fbId = self.fbId;
        dest.imageUrl = self.image_url;
    }else{
        CreatePasswordViewController *dest = [segue destinationViewController];
        dest._email = self.email;
        dest._firstName = self.firstName;
        dest._lastName = self.lastName;
        dest._bday = self.bday;
        dest._gender = self.gender;
        dest.strUserType = self.strUserType;
        dest.fbId = self.fbId;
        dest.imageUrl = self.image_url;

    }
}



@end
