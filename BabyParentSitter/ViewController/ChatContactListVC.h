//
//  ChatContactListVC.h
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatContactListVC : UIViewController
@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strUserType;
@end
