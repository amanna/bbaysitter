//
//  DashboardVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 11/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "DashboardVCSitter.h"
#import "CDSideBarController.h"
#import "SettingsVC.h"
#import "ChatContactListVC.h"
#import "SitterProfileVC.h"
#import "MyPaymentsVC.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "AppDelegate.h"
#import "UtiltiyManager.h"
#import "Job.h"
@interface DashboardVCSitter ()<UITableViewDataSource,UITableViewDelegate,CDSideBarControllerDelegate>{
    CDSideBarController *sideBar;
    NSArray *nameList;
    AppDelegate *appdelegate;
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UILabel *topLabelText;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property(nonatomic)NSMutableArray *arrJobs;
@property (weak, nonatomic) IBOutlet UITableView *tblJobs;

@end




@implementation DashboardVCSitter

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tblJobs.separatorStyle = UITableViewCellSeparatorStyleNone;
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.navigationItem.hidesBackButton = YES;
//    UIImage *buttonImageRight = [UIImage imageNamed:@"Message"];
//    UIButton *buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
//    [buttonRight setImage:buttonImageRight forState:UIControlStateNormal];
//    buttonRight.frame = CGRectMake(0, 0, buttonImageRight.size.width, buttonImageRight.size.height);
//    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonRight];
//    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
   // [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    NSString *strUname = appdelegate.strUserName;
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    nameList = @[strUname, @"Find Jobs", @"Payments",@"My Settings", @"Messages"];
    
    
    
    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
    sideBar.delegate = self;
    
    self.arrJobs = [[NSMutableArray alloc]init];
    
     //[activity showView];
    if([UtiltiyManager isReachableToInternet]){
        [AppServiceManager JobsList:appdelegate.strUserId withType:@"best" onCompletion:^(id object, NSError *error) {
            if(object){
                
            }else{
                
            }
        }];
    }else{
        [UtiltiyManager showAlertWithMessage:@"No Internet connnection"];
    }
    
    
    // Do any additional setup after loading the view.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [sideBar insertMenuButtonOnView:self.navigationController.view atPosition: CGPointMake(15, 25)];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - UITableView datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        
    }
    
    UIImageView *profileImage = (UIImageView *)[cell viewWithTag:100];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:101];
    UILabel *locationLabel = (UILabel *)[cell viewWithTag:102];
    UILabel *priceLabel = (UILabel *)[cell viewWithTag:103];
    UILabel *dateLabel = (UILabel *)[cell viewWithTag:104];
    UILabel *typeLabel = (UILabel *)[cell viewWithTag:105];
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:106];
    
    profileImage.image = [UIImage imageNamed:@"lady"];
    profileImage.layer.cornerRadius = profileImage.frame.size.width / 2;
    profileImage.clipsToBounds = YES;
    titleLabel.text = @"Need a Date Night Sitter";
    locationLabel.text = @"Honolulu, 1.2 mi |";
    priceLabel.text = @"$ 15-25/HR";
    dateLabel.text = @"One-Time: Starts February 3rd";
    typeLabel.text = @"NEW";
    imageView.image = [UIImage imageNamed:@"BestMatch"];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap)];
    singleTap.cancelsTouchesInView = NO;
    [cell addGestureRecognizer:singleTap];
    return cell;
}
- (void)singleTap{
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  
    [self performSegueWithIdentifier:@"segueJobDetails" sender:self];
    
}

#pragma mark -
#pragma mark - CDSideBarController delegate

- (void)menuButtonClicked:(int)index
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    if(index==1){
        self.segmentedControl.selectedSegmentIndex=1;
    }else if (index==2){
        MyPaymentsVC *dashboard = [story instantiateViewControllerWithIdentifier:@"MyPaymentsVC"];
       // dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
    }else if (index==0){
        SitterProfileVC *dashboard = [story instantiateViewControllerWithIdentifier:@"SitterProfileVC"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
    }else if (index==3){
        SettingsVC *dashboard = [story instantiateViewControllerWithIdentifier:@"SettingsVC"];
        dashboard.strUid = self.strUid;
        dashboard.strUserType = self.strUserType;
        [self showViewController:dashboard sender:self];
    }else{
        ChatContactListVC *dashboard = [story instantiateViewControllerWithIdentifier:@"ChatContactListVC"];
        dashboard.strUserType = self.strUserType;
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
    }

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
