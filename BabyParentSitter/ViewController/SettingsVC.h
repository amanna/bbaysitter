//
//  SettingsVC.h
//  BabyParentSitter
//
//  Created by Amrita on 28/05/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UIViewController
@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strUserType;
@end
