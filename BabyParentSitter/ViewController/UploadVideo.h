//
//  UploadVideo.h
//  BabySitterHawaii
//
//  Created by Amrita on 30/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "POAcvityView.h"
#import "AppServiceManager.h"

@interface UploadVideo : UIViewController
@property(nonatomic,strong)NSString *strUser;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
- (IBAction)btnSkipAction:(id)sender;
- (IBAction)btnPlusAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *vidcam;
@property (strong,nonatomic)NSString *strUserType;
@end
