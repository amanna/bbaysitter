//
//  Job.h
//  BabyParentSitter
//
//  Created by Amrita on 06/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Job : NSObject
@property(nonatomic)NSString *strJobId;
@property(nonatomic)NSString *strJobStrtDt;
@property(nonatomic)NSString *strJobEndDt;
@property(nonatomic)NSString *strSitterReqType;

@property(nonatomic)NSString *strTitle;
@property(nonatomic)NSString *strDesc;
@property(nonatomic)NSString *strHourlyRate;
@property(nonatomic)NSString *strVideoUrl;

@property(nonatomic)NSString *strLocation;
@end
