//
//  SendMsgVC.h
//  BabyParentSitter
//
//  Created by Amrita on 29/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendMsgVC : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblLoc;
@property (weak, nonatomic) IBOutlet UILabel *lblHour;
@property (weak, nonatomic) IBOutlet UILabel *lblStrtDt;
- (IBAction)btnSendMsgAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtMsg;
@property(nonatomic)NSString *strUserType;
@property(nonatomic)NSString *strUid;
@end
