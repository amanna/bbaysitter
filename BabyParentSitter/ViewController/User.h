//
//  User.h
//  BabyParentSitter
//
//  Created by Amrita on 06/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strEmail;
@property(nonatomic)NSString *strPass;
@property(nonatomic)NSString *strDob;
@property(nonatomic)NSString *strUserType;
@property(nonatomic)NSString *strAddress;
@property(nonatomic)NSString *strVideoUrl;
@property(nonatomic)NSString *strPhotoUrl;
@property(nonatomic)NSString *strVideoThumb;
@property(nonatomic)NSString *strRegisterDt;
@end
