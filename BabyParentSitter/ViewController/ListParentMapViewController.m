//
//  ListParentMapViewController.m
//  BabySitterHawaii
//
//  Created by debashisandria on 28/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "ListParentMapViewController.h"
#import "MyCustomAnnotation.h"
@interface ListParentMapViewController ()

@end

@implementation ListParentMapViewController{
    CLLocationManager *locationManager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; //whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
    [locationManager requestWhenInUseAuthorization];
    
    _mapView.delegate = self;
    
}




- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = newLocation.coordinate.latitude;
    zoomLocation.longitude= newLocation.coordinate.longitude;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
    
    /*MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = newLocation.coordinate;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";*/
    
    MyCustomAnnotation *point = [[MyCustomAnnotation alloc]initWithLocation:newLocation.coordinate];
    
    
    [self.mapView addAnnotation:point];
    
    [locationManager stopUpdatingLocation];
    
    
    
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MyCustomAnnotation class]])
    {
        static NSString *reuseId = @"ann";
        MKAnnotationView *av = [mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
        if (av == nil)
        {
            av = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        }
        else
        {
            av.annotation = annotation;
        }
        
        
        av.image = [UIImage imageNamed:@"mappin"];
        //av.canShowCallout= YES;
        
        /*UIView *vw = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        vw.backgroundColor = [UIColor redColor];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 50, 50)];
        label.numberOfLines = 4;
        label.text = @"hello\nhow are you\nfine";
        [vw addSubview:label];
        av.leftCalloutAccessoryView = vw;*/
        return av;
    }
    
    //return nil (default view) if annotation is not our custom type
    return nil;
}
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    if(![view.annotation isKindOfClass:[MKUserLocation class]]) {
        /*CGSize  calloutSize = CGSizeMake(100.0, 80.0);
        UIView *calloutView = [[UIView alloc] initWithFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y-calloutSize.height, calloutSize.width, calloutSize.height)];
        calloutView.backgroundColor = [UIColor whiteColor];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        button.frame = CGRectMake(5.0, 5.0, calloutSize.width - 10.0, calloutSize.height - 10.0);
        [button setTitle:@"OK" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(checkin) forControlEvents:UIControlEventTouchUpInside];
        [calloutView addSubview:button];
        [view.superview addSubview:calloutView];*/
        
        UIView *firstViewUIView = [[[NSBundle mainBundle] loadNibNamed:@"MapAnnotationCallout" owner:self options:nil] firstObject];
        firstViewUIView.frame = CGRectMake((-firstViewUIView.frame.size.width / 2) + 10, -firstViewUIView.frame.size.height-10, 190, 110);
        [view addSubview:firstViewUIView];
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillAppear:(BOOL)animated {
    
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 39.281516;
    zoomLocation.longitude= -76.580806;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    
    // 3
    [_mapView setRegion:viewRegion animated:YES];
}

@end
