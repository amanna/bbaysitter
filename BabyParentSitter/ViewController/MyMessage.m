//
//  MyMessage.m
//  BabyParentSitter
//
//  Created by Amrita on 28/05/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "MyMessage.h"
#import "CDSideBarController.h"
@interface MyMessage ()<UITableViewDataSource,UITableViewDelegate,CDSideBarControllerDelegate>{
    CDSideBarController *sideBar;
    NSArray *nameList;
}
@property(nonatomic)NSMutableArray *arrMsg;
@end

@implementation MyMessage

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.hidesBackButton = YES;
    self.arrMsg = [[NSMutableArray alloc]init];
//    UIImage *buttonImageRight = [UIImage imageNamed:@"Message"];
//    UIButton *buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
//    [buttonRight setImage:buttonImageRight forState:UIControlStateNormal];
//    buttonRight.frame = CGRectMake(0, 0, buttonImageRight.size.width, buttonImageRight.size.height);
//    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonRight];
//    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"My Messages";
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    
    nameList = @[@"John Doe", @"Find Jobs", @"Payments",@"My Settings", @"Messages"];
    
    
    
    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
    sideBar.delegate = self;
    

    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
