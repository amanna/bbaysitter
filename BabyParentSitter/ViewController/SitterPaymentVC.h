//
//  SitterPaymentVC.h
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANPopoverSlider.h"
@interface SitterPaymentVC : UIViewController
@property(nonatomic,strong)NSString *strUser;
@property (weak, nonatomic) IBOutlet ANPopoverSlider *distanceSlider;
@property (weak, nonatomic) IBOutlet UIScrollView *myScroll;
@property (weak, nonatomic) IBOutlet ANPopoverSlider *childslider;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (strong,nonatomic)NSString *strUserType;
@end
