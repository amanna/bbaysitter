//
//  SitterExperienceVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "SitterExperienceVC.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "ANPopoverSlider.h"
#import "UtiltiyManager.h"
#import "SitterBioVC.h"
#import "LanguageVc.h"
@interface SitterExperienceVC ()<UITextFieldDelegate>{
    POAcvityView *activity;
    NSInteger noOfExp;
    
    NSInteger highschoolFlag;
    NSInteger hsdegreeFlag;
    NSInteger collegeFlag;
    NSInteger collgedegreeFlag;
    NSInteger graduateFlag;
    NSInteger graduatedegreeFlag;
    
    NSInteger twinFlag;
    NSInteger spclneedFlag;
    
    NSInteger toddlerFlag;
    NSInteger earlyFlag;
    NSInteger elementaryFlag;
    NSInteger teenagerFlag;
    
    CGFloat widthLanView;
    CGFloat heightLanView;
}

- (IBAction)btnnextAction:(id)sender;
@property (weak, nonatomic) IBOutlet UISlider *roundSlider;
@property (weak, nonatomic) IBOutlet UIScrollView *myScroll;
@property (weak, nonatomic) IBOutlet ANPopoverSlider *experienceSlider;
- (IBAction)experiencechange:(id)sender;
@property(nonatomic)NSString *strLang;
@property(nonatomic)NSString *strLangNmae;

//School

@property (weak, nonatomic) IBOutlet UIButton *btnSomHighSchool;
- (IBAction)btnSomeHighScholAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnHSdegree;
- (IBAction)btnHSDegreeAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSomeCollege;
- (IBAction)btnSomeCollegeAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnCllgdegree;
- (IBAction)btnClgDegreeAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnGraduateschll;
- (IBAction)btngraduateschollAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnGraduateDegree;
- (IBAction)btnGraduateDegAction:(id)sender;
//Twinspecial need
@property (weak, nonatomic) IBOutlet UIButton *btnTwin;
- (IBAction)btnTwinAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnSpclNeed;
- (IBAction)btnSpclNeedAction:(id)sender;



//Age Group
@property (weak, nonatomic) IBOutlet UIButton *btnToddler;
- (IBAction)btnToddlerAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnEarlyAge;
- (IBAction)btnEarlyAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnElementaryAge;
- (IBAction)btnElementaryAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTeenager;
- (IBAction)btnTeenagerAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtLan;
- (IBAction)btnAddLanAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewLanguage;

@property(nonatomic)NSString *strLan;
@property(nonatomic)LanguageVc *popUpLang;
@end

@implementation SitterExperienceVC
- (void)viewDidAppear:(BOOL)animated{
    widthLanView = self.view.frame.size.width;
    heightLanView = self.view.frame.size.height;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    UIStoryboard *dash = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    self.popUpLang = [dash instantiateViewControllerWithIdentifier:@"LanguageVc"];
    self.experienceSlider.sliderUnit = sliderInteger;
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    noOfExp = [self.experienceSlider value];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Profile Settings";    // Do any additional setup after loading the view.

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnnextAction:(id)sender {
   self.strLan = self.lan.text;
   self.strLan = @"English";
  NSString *strExp = [NSString stringWithFormat:@"no_of_years=%ld&care_todders=%ld&care_earlyschool_age=%ld&care_elementryschool_age=%ld&care_teen=%ld&experience_twins=%ld&experience_special_need=%ld&education_school=%ld&school_degree=%ld&education_college=%ld&college_degree=%ld&graduate_school=%ld&graduate_school_degree=%ld&languages=%@&userid=%@",(long)noOfExp,(long)toddlerFlag,(long)earlyFlag,(long)elementaryFlag,(long)teenagerFlag,(long)twinFlag,(long)spclneedFlag,(long)highschoolFlag,(long)hsdegreeFlag,(long)collegeFlag,(long)collgedegreeFlag,(long)graduateFlag,(long)graduatedegreeFlag,self.strLan,self.strUser];
   if ([UtiltiyManager isReachableToInternet]) {
   [activity showView];
  [AppServiceManager updateExpInfo:strExp onCompletion:^(id object, NSError *error) {
      [activity hideView];
      if(object){
          NSDictionary *dict = object;
          NSInteger success =[[dict valueForKey:@"success"]integerValue];
          if(success==1){
              [self performSegueWithIdentifier:@"segueBio" sender:self];
          }
      }else{
          UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
          [alert show];

      }
  }];
   }else{
       UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
       [alert show];
   }
    //[self performSegueWithIdentifier:@"segueBio" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"segueBio"]) {
        SitterBioVC *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.strUser = self.strUser;
    }
}
- (IBAction)experiencechange:(id)sender {
}

///Highest Level of education
- (IBAction)btnSomeHighScholAction:(id)sender {
    if(highschoolFlag==0){
        highschoolFlag=1;
        [self.btnSomHighSchool setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
        hsdegreeFlag=0;
        collegeFlag=0;
        collgedegreeFlag=0;
        graduateFlag=0;
        graduatedegreeFlag=0;
        [self.btnHSdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnSomeCollege setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnCllgdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateschll setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateDegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btnHSDegreeAction:(id)sender {
    if(hsdegreeFlag==0){
        hsdegreeFlag=1;
        [self.btnHSdegree setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
        highschoolFlag=0;
        collegeFlag=0;
        collgedegreeFlag=0;
        graduateFlag=0;
        graduatedegreeFlag=0;
        
        [self.btnSomHighSchool setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnSomeCollege setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnCllgdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateschll setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateDegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];

        
    }
}
- (IBAction)btnSomeCollegeAction:(id)sender {
    if(collegeFlag==0){
        collegeFlag=1;
        [self.btnSomeCollege setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
        highschoolFlag=0;
        hsdegreeFlag=0;
        collgedegreeFlag=0;
        graduateFlag=0;
        graduatedegreeFlag=0;
        
        [self.btnSomHighSchool setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnHSdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnCllgdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateschll setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateDegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];

        
    }
}
- (IBAction)btnClgDegreeAction:(id)sender {
    if(collgedegreeFlag==0){
         collgedegreeFlag=1;
        [self.btnCllgdegree setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
        highschoolFlag=0;
        hsdegreeFlag=0;
        collegeFlag=0;
        graduateFlag=0;
        graduatedegreeFlag=0;
        
        [self.btnSomHighSchool setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnHSdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnSomeCollege setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateschll setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateDegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btngraduateschollAction:(id)sender {
    if(graduateFlag==0){
        graduateFlag=1;
        [self.btnGraduateschll setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
        highschoolFlag=0;
        hsdegreeFlag=0;
        collegeFlag=0;
        collgedegreeFlag=0;
        graduatedegreeFlag=0;
        
        [self.btnSomHighSchool setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnHSdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnSomeCollege setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnCllgdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateDegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
    }
}
- (IBAction)btnGraduateDegAction:(id)sender {
    if(graduatedegreeFlag==0){
        graduatedegreeFlag=1;
        [self.btnGraduateDegree setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
        
        highschoolFlag=0;
        hsdegreeFlag=0;
        collegeFlag=0;
        collgedegreeFlag=0;
        graduateFlag=0;
        
        [self.btnSomHighSchool setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnHSdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnSomeCollege setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnCllgdegree setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        [self.btnGraduateschll setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
    }
}



//Additional Experience
- (IBAction)btnTwinAction:(id)sender {
    if(twinFlag==1){
        twinFlag=0;
        [self.btnTwin setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        twinFlag=1;
        [self.btnTwin setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
    
}
- (IBAction)btnSpclNeedAction:(id)sender {
    if(spclneedFlag==1){
        spclneedFlag=0;
        [self.btnSpclNeed setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        spclneedFlag=1;
        [self.btnSpclNeed setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }
}

//Age group
- (IBAction)btnToddlerAction:(id)sender {
    if(toddlerFlag==1){
        toddlerFlag=0;
        [self.btnToddler setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        toddlerFlag=1;
        [self.btnToddler setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnEarlyAction:(id)sender {
    if(earlyFlag==1){
        earlyFlag=0;
        [self.btnEarlyAge setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        earlyFlag=1;
        [self.btnEarlyAge setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnElementaryAction:(id)sender {
    if(elementaryFlag==1){
        elementaryFlag=0;
        [self.btnElementaryAge setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        elementaryFlag=1;
        [self.btnElementaryAge setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnTeenagerAction:(id)sender {
    if(teenagerFlag==1){
        teenagerFlag=0;
        [self.btnTeenager setBackgroundImage:[UIImage imageNamed:@"cross1"] forState:UIControlStateNormal];
        
    }else{
        teenagerFlag=1;
        [self.btnTeenager setBackgroundImage:[UIImage imageNamed:@"check1"] forState:UIControlStateNormal];
    }

}
- (IBAction)btnAddLanAction:(id)sender {
    [self.view addSubview:self.popUpLang.view];
    self.navigationController.navigationBarHidden=YES;
    [self.popUpLang refreshUIWithDatasource:nil onCompletion:^(id object,id object1, NSUInteger eventPop) {
        if(eventPop==kActionSave){
            [self.popUpLang.view removeFromSuperview];
            self.navigationController.navigationBarHidden=NO;
            self.strLang = object;
            self.strLangNmae = object1;
            
            self.txtLan.text = self.strLangNmae;
        }else{
            [self.popUpLang.view removeFromSuperview];
            self.navigationController.navigationBarHidden=NO;
            
        }
    }];
    

    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
@end
