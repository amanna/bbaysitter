//
//  SitterBioVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 17/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "SitterBioVC.h"
#import <QuartzCore/QuartzCore.h>
#import "UtiltiyManager.h"
#import "UploadPhoto.h"
#import "AppServiceManager.h"
@interface SitterBioVC (){
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UITextView *txtview;
- (IBAction)btnnextAction:(id)sender;

@end

@implementation SitterBioVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.txtview.autocorrectionType = UITextAutocorrectionTypeNo;
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Profile Bio";    // Do any additional setup after loading the view.
   // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnnextAction:(id)sender {
    if ([UtiltiyManager isReachableToInternet]) {
    NSString *strBioInfo = [NSString stringWithFormat:@"userid=%@&sitter_bio=%@",self.strUser,self.txtview.text];
    [AppServiceManager updateBioInfo:strBioInfo onCompletion:^(id object, NSError *error) {
        if(object){
            NSDictionary *dict = object;
            NSInteger success =[[dict valueForKey:@"success"]integerValue];
            if(success==1){
                [self performSegueWithIdentifier:@"seguePhoto" sender:self];
            }
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    
    //[self performSegueWithIdentifier:@"seguePhoto" sender:self];
     
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"seguePhoto"]) {
        UploadPhoto *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.strUser = self.strUser;
    }
}

@end
