//
//  AddressInputVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 18/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "AddressInputVC.h"
#import "CTextField.h"
#import "AppServiceManager.h"
#import <CoreLocation/CoreLocation.h>
#import "POAcvityView.h"
#import "AddrParentCountVC.h"
@interface AddressInputVC ()<CLLocationManagerDelegate>{
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet CTextField *txtAddress;
- (IBAction)btnGpsAction:(id)sender;
- (IBAction)btnSubmitAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnGps;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSString *strCount;
@property (strong, nonatomic) CLLocation *location;
@property (strong, nonatomic) NSString *strLatt;
@property (strong, nonatomic) NSString *strLongi;
@end

@implementation AddressInputVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    
    self.locationManager = [[CLLocationManager alloc] init];
     self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    // Code to check if the app can respond to the new selector found in iOS 8. If so, request it.
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
        // Or [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
   
    self.location = [[CLLocation alloc] init];
     self.txtAddress.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.txtAddress becomeFirstResponder];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    

     self.navigationItem.title = @"Home Address";
    // Do any additional setup after loading the view.
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.location = locations.lastObject;
    NSLog(@"%@", self.location.description);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnGpsAction:(id)sender {
   self.strLatt = [NSString stringWithFormat:@"%.8f",self.location.coordinate.latitude];
     self.strLongi = [NSString stringWithFormat:@"%.8f",self.location.coordinate.longitude];
//    self.strLatt = @"22.463156";
//    self.strLongi = @"88.396294";
    [activity showView];
  [AppServiceManager findAddress:self.strLatt andLon:self.strLongi onCompletion:^(id object, NSError *error) {
      [activity hideView];
      if(object){
          NSDictionary *dict = object;
          NSInteger success =[[dict valueForKey:@"success"]integerValue];
          if(success==1){
              self.txtAddress.text = [dict valueForKey:@"address"];
          }
      }else{
         
          
      }
  }];
    
}

- (IBAction)btnSubmitAction:(id)sender {
//    [activity showView];
//    self.strUid = @"14";
//    [AppServiceManager updateAddress:self.strUid andAddress:self.txtAddress.text andLatt:self.strLatt andLong:self.strLongi onCompletion:^(id object, NSError *error) {
//        [activity hideView];
//        if(object){
//            NSDictionary *dict = object;
//            NSInteger success =[[dict valueForKey:@"success"]integerValue];
//            if(success==1){
//                [self performSegueWithIdentifier:@"segueParentcount" sender:self];
//                self.strCount = [dict valueForKey:@"nearby"];
//                
//            }
//        }else{
//            
//        }
//
//    }];
         [self performSegueWithIdentifier:@"segueParentcount" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"segueParentcount"]) {
        AddrParentCountVC *rs =segue.destinationViewController;
        rs.strCount = self.strCount;
        rs.strUid = self.strUid;
    } 
}
@end
