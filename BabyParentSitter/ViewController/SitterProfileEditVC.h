//
//  SitterProfileEditVC.h
//  BabyParentSitter
//
//  Created by Amrita on 26/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sitter.h"
@interface SitterProfileEditVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblpreference;
@property (weak, nonatomic) IBOutlet UITableView *tblExperience;
@property (weak, nonatomic) IBOutlet UITableView *tblLang;
@property (weak, nonatomic) IBOutlet UITableView *tblqualfication;
@property (weak, nonatomic) IBOutlet UITableView *tblEducation;
@property(nonatomic,strong)NSMutableArray *arrPreference;
@property(nonatomic,strong)NSMutableArray *arrExperience;
@property(nonatomic,strong)NSMutableArray *arrLang;
@property(nonatomic,strong)NSMutableArray *arrqualfication;
@property(nonatomic,strong)NSMutableArray *arrEducation;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblExp;
@property(nonatomic)Sitter *sitter;

@end
