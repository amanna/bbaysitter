//
//  JobPostStep1.h
//  BabySitterHawaii
//
//  Created by Amrita on 08/05/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobPostStep1 : UIViewController

@property (strong,nonatomic)NSString *strUserType;
@property (strong,nonatomic)NSString *strUser;
@end
