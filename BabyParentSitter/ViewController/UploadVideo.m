//
//  UploadVideo.m
//  BabySitterHawaii
//
//  Created by Amrita on 30/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "UploadVideo.h"
#import <MediaPlayer/MediaPlayer.h>
#import <QuartzCore/QuartzCore.h>
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
#import "POAcvityView.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "DashboardVCSitter.h"
#import "VidPlay.h"
#import "SitterAdditionalVC.h"
@interface UploadVideo ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>{
    POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UIButton *btnpaly;
- (IBAction)btnplayAction:(id)sender;
@property(nonatomic)NSString *strVid;

@end

@implementation UploadVideo

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = NO;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Profile Video";    // Do any additional setup after loading the view.

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSkipAction:(id)sender {
    //segueAdditional
     [self performSegueWithIdentifier:@"segueAdditional" sender:self];
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    DashboardVCSitter *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVC"];
    [self showViewController:dashboard sender:self];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"segueAdditional"]) {
        SitterAdditionalVC *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.strUser = self.strUser;
    }
}
- (IBAction)btnPlusAction:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, nil];
    [self presentViewController:picker animated:YES completion:NULL];
}
-(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}
#pragma mark ImagePicker delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:YES completion:^{
        if ([UtiltiyManager isReachableToInternet]) {
            [activity showView];
            NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
            NSURL* movieURL;
            if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
                NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
                // NSLog(@"%@",moviePath);
                movieURL=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            }

            NSData *videoData = [NSData dataWithContentsOfURL:movieURL];
            
            MPMoviePlayerController *mp = [[MPMoviePlayerController alloc]
                                           initWithContentURL:movieURL];
            mp.shouldAutoplay = NO;
            mp.initialPlaybackTime = .13;
            mp.currentPlaybackTime = .23;
            // get the thumbnail
            UIImage *thumbnail = [mp thumbnailImageAtTime:.25
                                               timeOption:MPMovieTimeOptionNearestKeyFrame];
            // clean up the movie player
            [mp stop];
            
            self.vidcam.image = thumbnail;
            self.vidcam.layer.cornerRadius = self.vidcam.frame.size.width / 2;
            self.vidcam.clipsToBounds = YES;

            NSString *urlString = @"http://www.efriendz.in/Anirbansite/index.php/user_registration/user_video_upload";
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:[NSURL URLWithString:urlString]];
            [request setHTTPMethod:@"POST"];
            
            
            NSMutableData *body = [NSMutableData data];
            
            NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            int len = 64;
            //Creates a random boundary
            NSMutableString *boundary = [NSMutableString stringWithCapacity: len];
            
            for (int i=0; i<len; i++) {
                [boundary appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
            }
            [request setValue:[NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
            NSData *boundaryData = [[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding];
            [body appendData:boundaryData];
            
            
            //int i = 101;
            NSString *str = [self randomStringWithLength:4];
            
            [body appendData: [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"profile_video\"; filename=\"%@\"\r\n\r\n",    [NSString stringWithFormat:@"myVideo11%@.mov",str]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:videoData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            //thumb image
            NSData *imageData = UIImageJPEGRepresentation(self.vidcam.image,0.5f);
            [body appendData: [[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"thumb_image\"; filename=\"%@\"\r\n\r\n",    [NSString stringWithFormat:@"image11%@.jpg",str]] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[NSData dataWithData:imageData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"uid\"\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            
            [body appendData:[[NSString stringWithFormat:@"%@" , self.strUser] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [request setHTTPBody:body];
            
            NSOperationQueue *mainqueue = [NSOperationQueue mainQueue];
            
            [NSURLConnection sendAsynchronousRequest:request queue:mainqueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                if(data)
                {
                    //NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                    NSDictionary *res = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
                    
                    NSInteger success = [[res objectForKey:@"success"]integerValue];
                    self.strVid = [res valueForKey:@""];
                    [activity hideView];
                    self.btnpaly.hidden = NO;
                    [self.btnSkip setBackgroundImage:[UIImage imageNamed:@"next"] forState:UIControlStateNormal];
                    
                    
                }else{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                }
                
            }];
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"No Internet Connection!!Please try again later" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
        
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnplayAction:(id)sender {
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    VidPlay *dashboard = [story instantiateViewControllerWithIdentifier:@"VidPlay"];
    dashboard.strVid = self.strVid;
    [self presentViewController:dashboard animated:YES completion:nil];
    
}
@end
