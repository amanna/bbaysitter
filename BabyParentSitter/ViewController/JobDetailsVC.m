//
//  JobDetailsVC.m
//  BabyParentSitter
//
//  Created by Amrita on 11/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "JobDetailsVC.h"
#import "AppServiceManager.h"
#import "SingleCell.h"
#import "POAcvityView.h"
#define btnTag 5000
@interface JobDetailsVC ()
@property(nonatomic,strong)NSMutableArray *arrPreference;
@property(nonatomic,strong)NSMutableArray *arrAge;
@property(nonatomic,strong)NSMutableArray *arrSpcl;

@property(nonatomic)NSMutableArray *arrPreferenceRight;
@property(nonatomic)NSMutableArray *arrAgeRight;
@property(nonatomic)NSMutableArray *arrSpclRight;
@property(nonatomic)NSMutableArray *arrLangRight;
@end

@implementation JobDetailsVC
-(void)viewDidAppear:(BOOL)animated{
    //left View
    NSMutableArray *arr = [[NSMutableArray alloc]initWithObjects:@"6AM-9PM",@"9AM-12PM",@"12AM-3PM",@"3PM-6PM",@"6PM-9PM",@"9PM-12AM",@"12AM-6AM", nil];
    CGFloat lpointOfx = 0.0;
    CGFloat lpointOfy = 0.0;
    for(int i =1; i<8;i++){
        UILabel *lblLeft = [[UILabel alloc]initWithFrame:CGRectMake(lpointOfx, lpointOfy, self.leftView.frame.size.width, 30)];
        lblLeft.textColor = [UIColor darkGrayColor];
        lblLeft.font =[UIFont fontWithName:@"OpenSans-Light" size:13.0];
        lblLeft.textAlignment = NSTextAlignmentRight;
        lblLeft.text = [arr objectAtIndex:i-1];
        [self.leftView addSubview:lblLeft];
        lpointOfy = lpointOfy + 30;
        
    }
    
    
    
    //Top View
    NSMutableArray *arr2 = [[NSMutableArray alloc]initWithObjects:@"S",@"M",@"T",@"W",@"T",@"F",@"S", nil];
    CGFloat tpointOfx = 0.0;
    CGFloat tpointOfy = 0.0;
    for(int i =1; i<=7;i++){
        UILabel *lbTop = [[UILabel alloc]initWithFrame:CGRectMake(tpointOfx, tpointOfy, 30 , 30)];
        if(i==1)
        {
            lbTop.textColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        }else{
            lbTop.textColor = [UIColor grayColor];
        }
        lbTop.font =[UIFont fontWithName:@"OpenSans-Light" size:13.0];
        
        lbTop.text = [arr2 objectAtIndex:i-1];
        lbTop.textAlignment = NSTextAlignmentCenter;
        [self.topView addSubview:lbTop];
        tpointOfx = tpointOfx + 30;
        
    }
    //availView
    
    
    CGFloat pointOfx = 0.0;
    CGFloat pointOfy = 0.0;
    
    for(int i=1; i<=7;i ++){
        for(int j=1; j<=7; j++){
            
            UIButton *small = [UIButton buttonWithType:UIButtonTypeCustom];
            small.frame =CGRectMake(pointOfx, pointOfy, 30, 30);
            small.backgroundColor = [UIColor clearColor];
            small.tag = btnTag + (i- 1)*7 + j;
            [small addTarget:self action:@selector(btnSmallAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.avilView addSubview:small];
            
            
            
            [self.avilView addSubview:small];
            
            //set border on four side
            CGSize mainViewSize = small.bounds.size;
            CGFloat borderWidth = 1;
            UIColor *borderColor = [UIColor lightGrayColor];
            UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, borderWidth, mainViewSize.height)];
            UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(mainViewSize.width - borderWidth, 0, borderWidth, mainViewSize.height)];
            
            UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainViewSize.width, borderWidth)];
            UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0,mainViewSize.height - borderWidth,  mainViewSize.width, borderWidth)];
            leftView.opaque = YES;
            rightView.opaque = YES;
            topView.opaque = YES;
            bottomView.opaque = YES;
            leftView.backgroundColor = borderColor;
            rightView.backgroundColor = borderColor;
            topView.backgroundColor = borderColor;
            bottomView.backgroundColor = borderColor;
            // for bonus points, set the views' autoresizing mask so they'll stay with the edges:
            leftView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
            rightView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin;
            topView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin;
            bottomView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
            if(j==1){
                
                [small addSubview:leftView];
                [small addSubview:rightView];
                [small addSubview:topView];
                
                
                if(i==7){
                    
                    [small addSubview:bottomView];
                    
                }
                
            }else {
                [small addSubview:rightView];
                [small addSubview:topView];
                if(i==7){
                    
                    [small addSubview:bottomView];
                    
                }
            }
            
            
            pointOfx = pointOfx + 30;
            
        }
        pointOfx=0;
        pointOfy = pointOfy + 30;
    }
    
    
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.jobdetailstextview.scrollEnabled = NO;
   [self.viewMilesShow setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.5]];
    for ( id subview in self.navigationController.view.subviews) {
        if ( [subview isKindOfClass:[UIButton class]] ) {
            //do your code
            UIButton *b = (UIButton*)subview;
            [b removeFromSuperview];
        }
    }
    self.navigationItem.hidesBackButton = YES;
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Job Description";
    
    //Add back menu
    UIImage *buttonImageleft = [UIImage imageNamed:@"backN"];
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLeft setImage:buttonImageleft forState:UIControlStateNormal];
    buttonLeft.frame = CGRectMake(0, 0, 45, 20);
    [buttonLeft addTarget:self action:@selector(leftMenuclicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonLeft];
    self.navigationItem.leftBarButtonItem = leftBarItem;
    
    //Tableview rounded corner
    self.tblPreferences.layer.cornerRadius = 5;
    self.tblAge.layer.cornerRadius = 5;
    self.tblLanguage.layer.cornerRadius = 5;
    self.tblSpclCare.layer.cornerRadius = 5;
   
    self.arrPreferenceRight = [[NSMutableArray alloc]init];
    self.arrAgeRight = [[NSMutableArray alloc]init];
    self.arrSpclRight = [[NSMutableArray alloc]init];
    
    self.arrPreference = [[NSMutableArray alloc]initWithObjects:@"Has own transporation",@"Non smoker",@"Will care for sick kids",@"Comfortable with Pets",@"Accepts Credit Card",@"Accepts Paypal",@"Accept Personal Cheque",@"Accept Cash",nil];
    
     self.arrAge = [[NSMutableArray alloc]initWithObjects:@"Toddlers(0-6 months)",@"Early School Age(7 months - 3 years)",@"Elementry School Age(4-6 years)",@"Pre-Teens(7-11 years)",@"Teenagers(12+ years)",nil];
    self.arrSpcl = [[NSMutableArray alloc]initWithObjects:@"Twins/Multiples",@"Special Needs", nil];
    //scrollView Additional services
    NSUInteger i;
    int xCoord=0;
    int yCoord=5;
    int buttonWidth=100;
    int buttonHeight=self.aScrollView.frame.size.height;
    int buffer = 10;
    for (i = 1; i <= 5; i++)
    {
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aButton.tag = 2300 + i;
        aButton.frame     = CGRectMake(xCoord, yCoord,buttonWidth,buttonHeight );
        //Add heart image to btn
        
        UIImageView *imgHeart = [[UIImageView alloc]initWithFrame:CGRectMake(24,(self.aScrollView.frame.size.height - 49)/2 - 15 ,49 , 49)];
        imgHeart.image =[UIImage imageNamed:@"heart"];
        [aButton addSubview:imgHeart];
        
        UILabel * lblHeart = [[UILabel alloc]initWithFrame:CGRectMake(0, imgHeart.frame.origin.y+ imgHeart.frame.size.height + 10, buttonWidth, 21)];
        lblHeart.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        lblHeart.text = @"Date Nights";
        lblHeart.textColor = [UIColor darkGrayColor];
        lblHeart.textAlignment = NSTextAlignmentCenter;
        [aButton addSubview:lblHeart];
        
        
        [self.aScrollView addSubview:aButton];
        xCoord += buttonWidth + buffer;
    }
    [self.aScrollView setContentSize:CGSizeMake(700, yCoord)];
    




    // Do any additional setup after loading the view.
}
- (void)leftMenuclicked{
    [self.navigationController popViewControllerAnimated:YES];
}
//- (void)viewDidLayoutSubviews
//{
//    [self.aScrollView setContentSize:CGSizeMake(0, _jobdetailstextview.frame.origin.y+_jobdetailstextview.frame.size.height+15)];
//#pragma profile pic image view customize
//    _profilepic.layer.cornerRadius=10.0;
//    _profilepic.layer.borderColor=[UIColor whiteColor].CGColor;
//    _profilepic.layer.borderWidth=5.0;
//    
//    
//#pragma settextview image
//    _jobdetailstextview.layer.cornerRadius=5.0;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag==100){
        return self.arrPreference.count;
    }else if (tableView.tag==101){
        return self.arrAge.count;
    }else if (tableView.tag==102){
        return 2;
       // return self.arrLangRight.count;
    }else{
         return self.arrSpcl.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag==100){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrPreference objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
         cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        if([[self.arrPreferenceRight objectAtIndex:indexPath.row] isEqualToString:@"1"]){
//            cell.accessoryType = UITableViewCellAccessoryCheckmark;
//        }else{
//            cell.accessoryType = UITableViewCellAccessoryNone;
//        }
        return cell;
   }else if (tableView.tag==101){
          SingleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
          cell.lblLeft.text =[self.arrAge objectAtIndex:indexPath.row];
           cell.lblLeft.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
          cell.lblRight.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
         // cell.lblRight.text = [self.arrAgeRight objectAtIndex:indexPath.row];
          cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
          return cell;

    }else if (tableView.tag==102){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        //cell.textLabel.text = [self.arrLangRight objectAtIndex:indexPath.row];
        cell.textLabel.text = @"English";
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        return cell;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrSpcl objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        return cell;
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnApplyAction:(id)sender {
    [self performSegueWithIdentifier:@"segueApplyJob" sender:self];
}
- (IBAction)btnReadmoreAction:(id)sender {
}
@end
