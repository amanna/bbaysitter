//
//  HomeViewController.m
//  BabySitterHawaii
//
//  Created by debashisandria on 13/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "HomeViewController.h"
#import <FacebookSDK/FacebookSDK.h>
@interface HomeViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingspace;

@end

@implementation HomeViewController {
    NSString *image_url, *name, *address, *age, *email, *gender, *fistName, *lastName, *bday,*fbID;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.loginSeg.selectedSegmentIndex = -1;
    self.leadingspace.constant = 0;
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
     self.loginSeg.selectedSegmentIndex = -1;
    self.scr.contentSize = CGSizeMake(self.viewFirst.frame.size.width*2, 0);
    
    for ( id subview in self.navigationController.view.subviews) {
        if ( [subview isKindOfClass:[UIButton class]] ) {
            //do your code
            UIButton *b = (UIButton*)subview;
            [b removeFromSuperview];
        }
    }

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    static NSInteger previousPage = 0;
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pc.currentPage = page;
    if (previousPage != page) {
        // Page has changed, do your thing!
        // ...
        // Finally, update previous page
        previousPage = page;
    }
}

- (IBAction)loginWithFB:(id)sender {
    
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        
        // Close the session and remove the access token from the cache
        // The session state handler (in the app delegate) will be called automatically
        [FBSession.activeSession closeAndClearTokenInformation];
        
        // If the session state is not any of the two "open" states when the button is clicked
    } else {
        // Open a session showing the user the login UI
        // You must ALWAYS ask for public_profile permissions when opening a session
        [FBSession openActiveSessionWithReadPermissions:@[@"public_profile",@"email",@"user_location",@"user_birthday"]
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error) {
             
             NSLog(@"Logging in");
             
             [FBRequestConnection startWithGraphPath:@"/me"
                                          parameters:nil
                                          HTTPMethod:@"GET"
                                   completionHandler:^(
                                                       FBRequestConnection *connection,
                                                       NSDictionary *result,
                                                       NSError *error
                                                       ) {
                                       /* handle the result */
                                       NSLog(@"%@",result);
                                       
                                       address = [[result objectForKey:@"location"] objectForKey:@"name"];
                                       name = [result objectForKey:@"name"];
                                       
                                       
                                       
                                       NSDateFormatter * dateFormatter = [[NSDateFormatter alloc]init];
                                       [dateFormatter setDateFormat:@"dd/mm/yyyy"];
                                       
                                       NSDate *toDate = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[result objectForKey:@"birthday"]]];
                                       
                                       
                                       NSDate *currentDate = [dateFormatter dateFromString:[dateFormatter stringFromDate:[NSDate date]]];
                                       
                                       NSTimeInterval secondsBetween = [currentDate timeIntervalSinceDate:toDate];
                                       
                                       age = [NSString stringWithFormat:@"%d", (int)secondsBetween/(60*60*24*365)];
                                       
                                       gender = [result objectForKey:@"gender"];
                                       
                                       image_url = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=99999999", [result objectForKey:@"id"]];
                                       
                                       bday = [result objectForKey:@"birthday"];
                                       fistName = [result objectForKey:@"first_name"];
                                       lastName = [result objectForKey:@"last_name"];
                                       email = [result objectForKey:@"email"];
                                       fbID = [result objectForKey:@"id"];
                                       
                                       [self performSegueWithIdentifier:@"login" sender:self];
                                       
                                   }];
             
             
         }];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"login"]) {
        WelcomeViewController * wcv = [segue destinationViewController];
        
        wcv.fbId = fbID;
        wcv.image_url = image_url;
        wcv.age = age;
        wcv.name = name;
        wcv.address = address;
        wcv.gender = gender;
        wcv.email = email;
        wcv.bday = bday;
        wcv.firstName = fistName;
        wcv.lastName = lastName;
        
    }
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}





- (IBAction)SignAction:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    if(segmentedControl.selectedSegmentIndex == 1)
    {
             [self performSegueWithIdentifier:@"segueLogin" sender:self];
    }
    else if(segmentedControl.selectedSegmentIndex == 0)
    {
        [self performSegueWithIdentifier:@"registerEmail" sender:self];
    }
    
}
@end
