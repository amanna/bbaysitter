//
//  ParentMenuVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 06/05/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "ParentMenuVC.h"
#import "JobPostStep1.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
#import "DashboardVCParent.h"

@interface ParentMenuVC ()
- (IBAction)btnPostJobAction:(id)sender;
- (IBAction)btnbrowseJobAction:(id)sender;

@end

@implementation ParentMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnPostJobAction:(id)sender {
    
    [self performSegueWithIdentifier:@"segueJobPost1" sender:self];
}

- (IBAction)btnbrowseJobAction:(id)sender {
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    DashboardVCParent *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVCParent"];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"segueJobPost1"]) {
        JobPostStep1 *rs =segue.destinationViewController;
        rs.strUserType = self.strUserType;
        rs.strUser = self.struid;
    }
}

@end
