//
//  CustomAnnotation.h
//  BabySitterHawaii
//
//  Created by debashisandria on 28/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
@interface MyCustomAnnotation : NSObject <MKAnnotation> {
    CLLocationCoordinate2D coordinate;
    NSString *title;
}
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
- (id)initWithLocation:(CLLocationCoordinate2D)coord;
- (id)initWithLocationAndTitle:(CLLocationCoordinate2D)coord titile:(NSString *)titleString;
// Other methods and properties.
@end
