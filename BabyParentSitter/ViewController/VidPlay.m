//
//  VidPlay.m
//  BabyParentSitter
//
//  Created by Amrita on 09/06/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "VidPlay.h"
#import <MediaPlayer/MediaPlayer.h>
@interface VidPlay ()
- (IBAction)btncloseAction:(id)sender;

@end

@implementation VidPlay

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    MPMoviePlayerController *theMoviPlayer;
    NSURL *urlString=[NSURL URLWithString:self.strVid];
    theMoviPlayer = [[MPMoviePlayerController alloc] initWithContentURL:urlString];
    theMoviPlayer.scalingMode = MPMovieScalingModeFill;
    theMoviPlayer.view.frame = CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:theMoviPlayer.view];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btncloseAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
