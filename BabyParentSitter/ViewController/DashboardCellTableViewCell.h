//
//  DashboardCellTableViewCell.h
//  BabySitterHawaii
//
//  Created by Amrita on 11/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgRound;
@property (weak, nonatomic) IBOutlet UILabel *lblNew;
@property (weak, nonatomic) IBOutlet UILabel *lblNeed;
@property (weak, nonatomic) IBOutlet UILabel *lblDivider;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblOneTime;

@end
