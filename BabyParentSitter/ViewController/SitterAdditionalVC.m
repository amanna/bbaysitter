//
//  SitterAdditionalVC.m
//  BabyParentSitter
//
//  Created by Amrita on 19/07/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "SitterAdditionalVC.h"
#import "POAcvityView.h"
#import "UtiltiyManager.h"
#import "AppServiceManager.h"
#import "HeartCell.h"
#import "Additional.h"
#import "DashboardVCSitter.h"
@interface SitterAdditionalVC (){
    NSArray *arrDataSrc;
    NSMutableDictionary *imageName;
    POAcvityView *activity;
}
@property(nonatomic)NSMutableArray *arrSelected;
@end

@implementation SitterAdditionalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrSelected = [[NSMutableArray alloc]init];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.colletionView.userInteractionEnabled = YES;
    
    
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    
    [activity showView];
    if([UtiltiyManager isReachableToInternet]){
        [AppServiceManager AdditionalService:^(id object, NSError *error) {
            if(object){
                arrDataSrc = object;
                [self.colletionView reloadData];
                [activity hideView];
            }
        }];
    }else{
        [UtiltiyManager showAlertWithMessage:@"No Internet connection"];
    }


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrDataSrc count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    
    HeartCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    Additional *additional = [arrDataSrc objectAtIndex:indexPath.row];
    BOOL isFound = NO;
    cell.lblheartTitle.text= additional.strSName;
    if(self.arrSelected.count > 0){
        for(Additional *add in self.arrSelected){
            if(add == additional){
                isFound = YES;
                break;
            }
        }
    }
    
    if(isFound==NO){
        cell.imgheart.image = [UIImage imageNamed:@"offheart"];
    }else{
        cell.imgheart.image = [UIImage imageNamed:@"heart"];
    }
    
    //    UIImageView *imageView = (UIImageView *)[cell viewWithTag:100];
    //    UILabel *label = (UILabel *)[cell viewWithTag:101];
    
    //    BOOL yesKey = [[imageName objectForKey:[NSString stringWithFormat:@"%d",indexPath.row]] boolValue];
    //
    //    if(yesKey == YES)
    //    {
    //    imageView.image = [UIImage imageNamed:@"heart"];
    //    }
    //
    //    else{
    //    imageView.image = [UIImage imageNamed:@"offheart"];
    //    }
    //
    //    label.text = arrDataSrc[indexPath.row];
    cell.userInteractionEnabled = YES;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    Additional *add = [arrDataSrc objectAtIndex:indexPath.row];
    BOOL isFound = NO;
    if(self.arrSelected.count > 0){
        for (int i=0; i<self.arrSelected.count; i++) {
            Additional *add1 = [self.arrSelected objectAtIndex:i];
            if(add1 == add){
                isFound=YES;
                break;
            }
        }
    }
    if(isFound==NO){
        [self.arrSelected addObject:add];
    }else{
        [self.arrSelected removeObject:add];
    }
    [self.colletionView reloadData];
    // [self.colletionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
}
- (IBAction)nextButtonAction:(id)sender {
    NSString *strAdditional;
    NSMutableArray *arrAdditional = [[NSMutableArray alloc]init];
    for (int i=0; i< self.arrSelected.count;i++) {
        Additional *sdd = [self.arrSelected objectAtIndex:i];
        [arrAdditional addObject:sdd.strSid];
    }
    strAdditional = [arrAdditional componentsJoinedByString:@","];
    if([UtiltiyManager isReachableToInternet]){
//        [AppServiceManager updateHourlyRate:self.strUser withAdditional:strAdditional andJobId:self.strJobId onCompletion:^(id object, NSError *error) {
//            if(object){
//                [self performSegueWithIdentifier:@"uploadPhotoParent" sender:self];
//            }
//        }];
        
        
        // Get the storyboard named secondStoryBoard from the main bundle:
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
        DashboardVCSitter *dashboard = [story instantiateViewControllerWithIdentifier:@"DashboardVC"];
        //UINavigationController *root = [[UINavigationController alloc]initWithRootViewController:dashboard];
        
        [self showViewController:dashboard sender:self];
    }else{
        [UtiltiyManager showAlertWithMessage:@"NO Internet connection"];
    }
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"uploadPhotoParent"]) {
//        UploadPhoto *rs =segue.destinationViewController;
//        rs.strUser = self.strUser;
//        rs.strUserType = @"parent";
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
