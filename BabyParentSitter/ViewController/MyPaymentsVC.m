//
//  MyPaymentsVC.m
//  BabySitterHawaii
//
//  Created by Amrita on 15/04/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "MyPaymentsVC.h"
#import "MyPaymentCell.h"
#import "CDSideBarController.h"
@interface MyPaymentsVC ()<UITableViewDataSource,UITableViewDelegate,CDSideBarControllerDelegate>{
    CDSideBarController *sideBar;
    NSArray *nameList;
}

@end

@implementation MyPaymentsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage* image3 = [UIImage imageNamed:@"menu.png"];
    CGRect frameimg = CGRectMake(0, 0, image3.size.width, image3.size.height);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(backToPrev)
         forControlEvents:UIControlEventTouchUpInside];
    [someButton setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    self.navigationItem.title = @"My Payments";
    
    UIImage* image4 = [UIImage imageNamed:@"arrow.png"];
    CGRect frameimg4 = CGRectMake(0, 0, image4.size.width, image4.size.height);
    UIButton *someButton1 = [[UIButton alloc] initWithFrame:frameimg4];
    [someButton1 setBackgroundImage:image4 forState:UIControlStateNormal];
    [someButton1 addTarget:self action:@selector(nextTo)
          forControlEvents:UIControlEventTouchUpInside];
    [someButton1 setShowsTouchWhenHighlighted:YES];
    UIBarButtonItem *mailbutton1 =[[UIBarButtonItem alloc] initWithCustomView:someButton1];
    self.navigationItem.rightBarButtonItem=mailbutton1;
    // Do any additional setup after loading the view.
    
    
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    nameList = @[@"John Doe", @"Find Jobs", @"Payments",@"My Settings", @"Messages"];
    
    
    
    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
    sideBar.delegate = self;

}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [sideBar insertMenuButtonOnView:self.navigationController.view atPosition: CGPointMake(15, 25)];
}

- (void)backToPrev{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)nextTo{
    
    [self performSegueWithIdentifier:@"sitterbio" sender:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 19;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier5 = @"Cell";
    MyPaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier5];
    if (!cell) {
        cell = [[MyPaymentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier5];
    }
   
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
