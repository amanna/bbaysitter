//
//  CTextField.h
//  BabySitterHawaii
//
//  Created by debashisandria on 20/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTextField : UITextField
@property float leftPadding;
@end
