

#import <Foundation/Foundation.h>
typedef void (^ServiceCompletionHandler)(id object, NSError *error);
typedef void (^ServiceCompletionHandler2)(id object,id object2, NSError *error);
@interface AppServiceManager : NSObject
+ (void)loginWith:(NSString*)strUser andPass:(NSString*)strPass onCompletion:(ServiceCompletionHandler)handler;


+ (void)loginWithFB:(NSString*)strFb onCompletion:(ServiceCompletionHandler)handler;
//email(type=text),firstname(type=text),lastname(type=text),password(type=text),date_of_birth(type=date('YYYY-mm-dd'))
+ (void)registrationParent:(NSString*)strEmail andFirstname:(NSString*)strFirstname andLastname:(NSString*)strLastname andPassword:(NSString*)strPass andDob:(NSString*)strDob onCompletion:(ServiceCompletionHandler)handler;

+ (void)registrationParentFB:(NSString*)strEmail andFirstname:(NSString*)strFirstname andLastname:(NSString*)strLastname andPassword:(NSString*)strPass andDob:(NSString*)strDob andFB:(NSString*)strFB onCompletion:(ServiceCompletionHandler)handler;



+ (void)registrationSitter:(NSString*)strEmail andFirstname:(NSString*)strFirstname andLastname:(NSString*)strLastname andPassword:(NSString*)strPass andDob:(NSString*)strDob andGender:(NSString*)strGender onCompletion:(ServiceCompletionHandler)handler;
+ (void)registrationSitterFB:(NSString*)strEmail andFirstname:(NSString*)strFirstname andLastname:(NSString*)strLastname andPassword:(NSString*)strPass andDob:(NSString*)strDob andGender:(NSString*)strGender andFB:(NSString*)strFB onCompletion:(ServiceCompletionHandler)handler;

+ (void)findAddress:(NSString*)strLatt andLon: (NSString*)strLon onCompletion:(ServiceCompletionHandler)handler;


+ (void)updateAddress:(NSString*)strUid andAddress:(NSString*)strAddress andLatt:(NSString*)strLatt
              andLong:(NSString*)strLong  onCompletion:(ServiceCompletionHandler)handler;


+ (void)updatePaymentInfo:(NSString*)strPaymentInfo onCompletion:(ServiceCompletionHandler)handler;

+ (void)updateBioInfo:(NSString*)strBioInfo onCompletion:(ServiceCompletionHandler)handler;
+ (void)forgotPass:(NSString*)strEmail  onCompletion:(ServiceCompletionHandler)handler;
+ (void)updateAvailability:(NSString*)strBioInfo  onCompletion:(ServiceCompletionHandler)handler;

+ (void)updateExpInfo:(NSString*)strExp onCompletion:(ServiceCompletionHandler)handler;
+ (void)uploadPost:(NSString *)strUid withData:(NSData*)data onCompletion:(ServiceCompletionHandler)handler;

+ (void)updateAdditionalInfo:(NSString*)strAdditional onCompletion:(ServiceCompletionHandler)handler;

//job post
+ (void)PostJobStep1:(NSString*)strJobInfo onCompletion:(ServiceCompletionHandler)handler;
+ (void)PostJobStep2:(NSString*)strJobInfo onCompletion:(ServiceCompletionHandler)handler;
+ (void)PostJobStep3:(NSString*)strJobInfo onCompletion:(ServiceCompletionHandler)handler;


//SitterProfile
+ (void)getSitterProfile:(NSString*)strSitter onCompletion:(ServiceCompletionHandler)handler;
//SitterProfile Update
+ (void)updateSitterProfile:(NSString*)strSitterU onCompletion:(ServiceCompletionHandler)handler;
//All Language

+ (void)FindAllLanguage:(ServiceCompletionHandler)handler;
+ (void)AdditionalService:(ServiceCompletionHandler)handler;
+ (void)baseHourlyRate:(NSString*)strUser onCompletion:(ServiceCompletionHandler)handler;
+ (void)updateHourlyRate:(NSString*)strUser withAdditional:(NSString*)strAdditional andJobId:(NSString*)strjob onCompletion:(ServiceCompletionHandler)handler;

+ (void)getParentProfile:(NSString*)strUser onCompletion:(ServiceCompletionHandler2)handler;
+ (void)getParentJobs:(NSString*)strUser andJobType:(NSString*)strJobType onCompletion:(ServiceCompletionHandler)handler;
//Job details
+ (void)jobDetails:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler;

//Make faourite
+ (void)makeFavByParent:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler;
//Check faourite
+ (void)isfavouSitter:(NSString*)strParentSitter onCompletion:(ServiceCompletionHandler)handler;
//Sitter List
+ (void)SitterList:(NSString*)strMsginfo withType:(NSString*)strType onCompletion:(ServiceCompletionHandler)handler;

//ApplyJob
+ (void)applyJobBySitter:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler;
//JobList
+ (void)JobsList:(NSString*)strMsginfo withType:(NSString*)strType onCompletion:(ServiceCompletionHandler)handler;


//Messages
+ (void)sendmsgBySitter:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler;
+ (void)sendmsgByParent:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler;

//selectSitter
+ (void)selectSitter:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler;

//list Message
+ (void)getMsgBySitter:(NSString*)strMsgInfo onCompletion:(ServiceCompletionHandler)handler;
+ (void)getMsgByParent:(NSString*)strMsgInfo onCompletion:(ServiceCompletionHandler)handler;
@end
