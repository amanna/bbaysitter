
#import "AppServiceManager.h"
#import "SSRestManager.h"
#import "SSJsonResponseHandler.h"
#import "AppConstants.h"
#import "Additional.h"
#import "Sitter.h"
#import "Parent.h"
#import "Job.h"
#import "Msg.h"
@implementation AppServiceManager
+ (void)loginWith:(NSString*)strUser andPass:(NSString*)strPass onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@login",kBaseURL];
    httpBody = [NSString stringWithFormat:@"email=%@&password=%@",strUser,strPass];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)loginWithFB:(NSString*)strFb onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@user_login/login_fb",kBaseURL];
    httpBody = [NSString stringWithFormat:@"fbid=%@",strFb];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)registrationParent:(NSString*)strEmail andFirstname:(NSString*)strFirstname andLastname:(NSString*)strLastname andPassword:(NSString*)strPass andDob:(NSString*)strDob onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@parentsignup",kBaseURL];
   // email(type=text),firstname(type=text),lastname(type=text),password(type=text),date_of_birth(type=date('YYYY-mm-dd'))
    httpBody = [NSString stringWithFormat:@"email=%@&firstname=%@&lastname=%@&password=%@&date_of_birth=%@",strEmail,strFirstname,strLastname,strPass,strDob];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)registrationParentFB:(NSString*)strEmail andFirstname:(NSString*)strFirstname andLastname:(NSString*)strLastname andPassword:(NSString*)strPass andDob:(NSString*)strDob andFB:(NSString*)strFB onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@user_registration/parent_add_facebook",kBaseURL];
    // email(type=text),firstname(type=text),lastname(type=text),password(type=text),date_of_birth(type=date('YYYY-mm-dd'))
    httpBody = [NSString stringWithFormat:@"email=%@&firstname=%@&lastname=%@&password=%@&date_of_birth=%@&facebook_id=%@",strEmail,strFirstname,strLastname,strPass,strDob,strFB];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}

+ (void)registrationSitter:(NSString*)strEmail andFirstname:(NSString*)strFirstname andLastname:(NSString*)strLastname andPassword:(NSString*)strPass andDob:(NSString*)strDob andGender:(NSString*)strGender onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@sittersignup",kBaseURL];
    // email(type=text),firstname(type=text),lastname(type=text),password(type=text),date_of_birth(type=date('YYYY-mm-dd'))
    httpBody = [NSString stringWithFormat:@"email=%@&firstname=%@&lastname=%@&password=%@&date_of_birth=%@&gender=%@",strEmail,strFirstname,strLastname,strPass,strDob,strGender];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}

+ (void)registrationSitterFB:(NSString*)strEmail andFirstname:(NSString*)strFirstname andLastname:(NSString*)strLastname andPassword:(NSString*)strPass andDob:(NSString*)strDob andGender:(NSString*)strGender andFB:(NSString*)strFB onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
   urlString = [NSString stringWithFormat:@"%@user_registration/sitter_add_facebook",kBaseURL];
    // email(type=text),firstname(type=text),lastname(type=text),password(type=text),date_of_birth(type=date('YYYY-mm-dd'))
    httpBody = [NSString stringWithFormat:@"email=%@&firstname=%@&lastname=%@&password=%@&date_of_birth=%@&facebook_id=%@",strEmail,strFirstname,strLastname,strPass,strDob,strFB];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}


+ (void)findAddress:(NSString*)strLatt andLon: (NSString*)strLon onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@findaddress",kBaseURL];
    httpBody = [NSString stringWithFormat:@"lat=%@&long=%@",strLatt,strLon];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}


+ (void)updateAddress:(NSString*)strUid andAddress:(NSString*)strAddress andLatt:(NSString*)strLatt
              andLong:(NSString*)strLong onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@updateaddress",kBaseURL];
    httpBody = [NSString stringWithFormat:@"lat=%@&long=%@&address=%@&userid=%@",strLatt,strLong,strAddress,strUid];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)updatePaymentInfo:(NSString*)strPaymentInfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@sitter-profileinfo",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strPaymentInfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)updateAvailability:(NSString*)strBioInfo  onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@sitter-available",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strBioInfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)updateBioInfo:(NSString*)strBioInfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@update-bio",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strBioInfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)updateExpInfo:(NSString*)strExp onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@update-experience",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strExp];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)forgotPass:(NSString*)strEmail  onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@forget-password",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strEmail];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)uploadPost:(NSString *)strUid withData:(NSData*)data onCompletion:(ServiceCompletionHandler)handler{
//    NSData *data;
//    if (post.videoURL) {
//        data = [NSData dataWithContentsOfURL:post.videoURL];
//    }else {
//        data = UIImageJPEGRepresentation (post.image,0.9);
//    }
//    
    NSString *url = [NSString stringWithFormat:@"%@post",kBaseURL];
    NSString *userId =  [[NSUserDefaults standardUserDefaults] objectForKey:kUserID];
    NSDictionary *paramsDict = @{@"uid": strUid};
    [SSRestManager postFileData:data withBaseURL:url withFileName:@"testing.jpg" fileKey:@"post_image" otherData:paramsDict onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        handler(nil,error);
    }];

}
+ (void)PostJobStep1:(NSString*)strJobInfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@job-post",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strJobInfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)PostJobStep2:(NSString*)strJobInfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@job-description",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strJobInfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)FindAllLanguage:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@language_view/language",kBaseURL];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            NSMutableArray *arrLang = [[NSMutableArray alloc]init];
            arrLang = [json valueForKey:@"Language"];
            handler (arrLang,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)AdditionalService:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@charges/aditional_charges",kBaseURL];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            NSMutableArray *arrAdditional = [[NSMutableArray alloc]init];
            NSMutableArray *arr = [json valueForKey:@"aditional_charges"];
            for (int i=0; i< arr.count; i++) {
                NSDictionary *dict = [arr objectAtIndex:i];
                Additional *add = [[Additional alloc]init];
                add.strSid = [dict valueForKey:@"sid"];
                add.strSName = [dict valueForKey:@"service_name"];
                add.strSUrl = [dict valueForKey:@"icon_url"];
                [arrAdditional addObject:add];
            }
            handler (arrAdditional,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)baseHourlyRate:(NSString*)strUser onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@charges/hourly_zip_rate",kBaseURL];
    httpBody = [NSString stringWithFormat:@"userid=%@&User_Type=%@",strUser,@"Parent"];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)updateHourlyRate:(NSString*)strUser withAdditional:(NSString*)strAdditional andJobId:(NSString*)strjob onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@charges/jobhourly_priceupdate",kBaseURL];
    httpBody = [NSString stringWithFormat:@"uid=%@&straadditional=%@&jobid=%@",strUser,strAdditional,strjob];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)getParentProfile:(NSString*)strUser onCompletion:(ServiceCompletionHandler2)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@charges/jobpost_byparent",kBaseURL];
    httpBody = [NSString stringWithFormat:@"parentid=%@",strUser];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            NSDictionary *dict = [[json valueForKey:@"parent_profile"] objectAtIndex:0];
            Parent *parent = [[Parent alloc]init];
            parent.strUid = [dict valueForKey:@"User_ID"];
            parent.strFname =[dict valueForKey:@"User_Firstname"];
            parent.strLname =[dict valueForKey:@"User_Lastname"];
            parent.strAddress =[dict valueForKey:@"address"];
            parent.strKid =[dict valueForKey:@"FB_ID"];
            parent.strProfileImg = [dict valueForKey:@"profile_image"];
            NSMutableArray *arrJobs = [[NSMutableArray alloc]init];
            NSMutableArray *arrJob = [[NSMutableArray alloc]init];
            arrJob = [json valueForKey:@"all_jobs"];
            if(arrJob.count > 0){
            for(int i=0; i<arrJob.count; i++){
                NSDictionary *dict1 = [arrJob objectAtIndex:i];
                Job *job = [[Job alloc]init];
                job.strJobId = [dict1 valueForKey:@"Job_id"];
                job.strSitterReqType = [dict1 valueForKey:@"sitter_requirement_type"];
                job.strLocation = [dict1 valueForKey:@"job_location"];
                job.strTitle = [dict1 valueForKey:@"job_title"];
                job.strDesc = [dict1 valueForKey:@"job_desc"];
                job.strHourlyRate = [dict1 valueForKey:@"job_price"];
                [arrJobs addObject:job];
            }
                handler (parent,arrJobs,nil);
            }else{
                handler (parent,arrJobs,nil);
            }
            
        } onError:^(NSError *error) {
            handler (nil,nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,nil,error);
        }
    }];

}
+ (void)getParentJobs:(NSString*)strUser andJobType:(NSString*)strJobType onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    //Active,Expired,Completed
    urlString = [NSString stringWithFormat:@"%@charges/myjob",kBaseURL];
    httpBody = [NSString stringWithFormat:@"parentid=%@&jobtype=%@",strUser,strJobType];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            NSMutableArray *arrjobs = [[NSMutableArray alloc]init];
            NSMutableArray *arrJob = [json valueForKey:@"all_jobs"];
            if(arrJob.count > 0){
                for(int i=0; i<arrJob.count; i++){
                    NSDictionary *dict1 = [arrJob objectAtIndex:i];
                    Job *job = [[Job alloc]init];
                    job.strJobId = [dict1 valueForKey:@"Job_id"];
                    job.strSitterReqType = [dict1 valueForKey:@"sitter_requirement_type"];
                    job.strLocation = [dict1 valueForKey:@"job_location"];
                    job.strTitle = [dict1 valueForKey:@"job_title"];
                    job.strDesc = [dict1 valueForKey:@"job_desc"];
                    job.strHourlyRate = [dict1 valueForKey:@"job_price"];
                    [arrjobs addObject:job];
                }
                handler (arrjobs,nil);
            }else{
                handler (arrjobs,nil);
            }

        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
//Job Details
+ (void)jobDetails:(NSString*)strJobId onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@charges/jobdetails",kBaseURL];
    httpBody = [NSString stringWithFormat:@"jobid=%@",strJobId];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];
    

}

//Make Fav
+ (void)makeFavByParent:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    //    NSString *strMsg = [NSString stringWithFormat:@"parentid=%@&sitterid=%@",]
    urlString = [NSString stringWithFormat:@"%@charges/favourite_sitter",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strMsginfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}

+ (void)isfavouSitter:(NSString*)strParentSitter onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    //    NSString *strMsg = [NSString stringWithFormat:@"parentid=%@&sitterid=%@",]
    urlString = [NSString stringWithFormat:@"%@charges/is_favorite_sitter",kBaseURL];
    //httpBody = [NSString stringWithFormat:@"sitterid=%@&parentid=%@",strMsginfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}

//Sitter List
+ (void)SitterList:(NSString*)strMsginfo withType:(NSString*)strType onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    if([strType isEqualToString:@"location"]){
         urlString = [NSString stringWithFormat:@"%@charges/find_sitter",kBaseURL];
    }else if ([strType isEqualToString:@"new"]){
       // recent_register_sitter
         urlString = [NSString stringWithFormat:@"%@charges/get_sitter",kBaseURL];
    }else if ([strType isEqualToString:@"fav"]){
         urlString = [NSString stringWithFormat:@"%@charges/favorite_sitterlist",kBaseURL];
    }
    httpBody = [NSString stringWithFormat:@"%@",strMsginfo];
   restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            NSMutableArray *arrAdditional = [[NSMutableArray alloc]init];
            NSMutableArray *arr = [json valueForKey:@"recent_sitter"];
            for (int i=0; i< arr.count; i++) {
                NSDictionary *dict = [arr objectAtIndex:i];
                Sitter *sitter = [[Sitter alloc]init];
                sitter.strUid = [dict valueForKey:@"User_ID"];
                sitter.strFname = [dict valueForKey:@"User_Firstname"];
                sitter.strLname = [dict valueForKey:@"User_Lastname"];
                sitter.strAddress = [dict valueForKey:@"icon_url"];
                sitter.strProfileImage = [dict valueForKey:@"profile_image"];
                sitter.strExperience = [dict valueForKey:@"icon_url"];
                [arrAdditional addObject:sitter];
            }
            handler (arrAdditional,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)updateSitterProfile:(NSString*)strSitterU onCompletion:(ServiceCompletionHandler)handler{
    
}
//Jobs List
+ (void)JobsList:(NSString*)strSitterId withType:(NSString*)strType onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    NSString *strMsg = [NSString stringWithFormat:@"sitterid=%@",strSitterId];
    if([strType isEqualToString:@"best"]){
        urlString = [NSString stringWithFormat:@"%@charges/newjob_list",kBaseURL];
    }else if([strType isEqualToString:@"new"]){
        urlString = [NSString stringWithFormat:@"%@charges/newjob_list",kBaseURL];
    }else{
         urlString = [NSString stringWithFormat:@"%@charges/newjob_list",kBaseURL];
    }
    httpBody = strMsg;
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            NSMutableArray *arrjobs = [[NSMutableArray alloc]init];
            NSMutableArray *arrJob = [json valueForKey:@"all_jobs"];
            if(arrJob.count > 0){
                for(int i=0; i<arrJob.count; i++){
                    NSDictionary *dict1 = [arrJob objectAtIndex:i];
                    Job *job = [[Job alloc]init];
                    job.strJobId = [dict1 valueForKey:@"Job_id"];
                    job.strSitterReqType = [dict1 valueForKey:@"sitter_requirement_type"];
                    job.strLocation = [dict1 valueForKey:@"job_location"];
                    job.strTitle = [dict1 valueForKey:@"job_title"];
                    job.strDesc = [dict1 valueForKey:@"job_desc"];
                    job.strHourlyRate = [dict1 valueForKey:@"job_price"];
                    [arrjobs addObject:job];
                }
                handler (arrjobs,nil);
            }else{
                handler (arrjobs,nil);
            }

           
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}



//Apply Job
+ (void)applyJobBySitter:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    //    NSString *strMsg = [NSString stringWithFormat:@"jobid=%@&sitterid=%@",]
    urlString = [NSString stringWithFormat:@"%@charges/apply_jobs",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strMsginfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}

//Mesggage
+ (void)sendmsgBySitter:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    urlString = [NSString stringWithFormat:@"%@charges/sendmsgbysitter",kBaseURL];
//    NSString *strMsg = [NSString stringWithFormat:@"jobid=%@&sitterid=%@&msg=%@",]
    httpBody = [NSString stringWithFormat:@"%@",strMsginfo];
    
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)sendmsgByParent:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString;
    NSString *httpBody;
     //NSString *strMsg = [NSString stringWithFormat:@"jobid=%@&sitterid=%@&parentid=%@&msg=%@",]
    urlString = [NSString stringWithFormat:@"%@charges/sendmsgbyparent",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strMsginfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)selectSitter:(NSString*)strMsginfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    //NSString *strMsg = [NSString stringWithFormat:@"jobid=%@&sitterid=%@&parentid=%@",]
    urlString = [NSString stringWithFormat:@"%@charges/select_sitter",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strMsginfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)getMsgBySitter:(NSString*)strMsgInfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    //NSString *strMsg = [NSString stringWithFormat:@"sitterid=%@",]
    urlString = [NSString stringWithFormat:@"%@charges/message_listby_sitter",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strMsgInfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}
+ (void)getMsgByParent:(NSString*)strMsgInfo onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    //NSString *strMsg = [NSString stringWithFormat:@"parentid=%@",]
    urlString = [NSString stringWithFormat:@"%@charges/message_listby_parent",kBaseURL];
    httpBody = [NSString stringWithFormat:@"%@",strMsgInfo];
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *json) {
            handler (json,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];

}

//
+ (void)getSitterProfile:(NSString*)strSitter onCompletion:(ServiceCompletionHandler)handler{
    SSRestManager *restManager = [[SSRestManager alloc]init];
    restManager.httpMethod = @"POST";
    restManager.contentType = @"application/x-www-form-urlencoded";
    NSString *urlString ;
    NSString *httpBody;
    NSString *strMsg = [NSString stringWithFormat:@"sitter_id=%@",strSitter];
    urlString = [NSString stringWithFormat:@"%@sitter-details",kBaseURL];
    httpBody = strMsg;
    restManager.httpBody = httpBody;
    [restManager getHttpResponseWithBaseUrl:urlString onCompletion:^(id responseData, NSURLResponse *reponse) {
        SSJsonResponseHandler *jsonResponseHandler = [[SSJsonResponseHandler alloc] init];
        [jsonResponseHandler getJsonResponseFromData:responseData onCompletion:^(NSDictionary *dict) {
            Sitter *sitter = [[Sitter alloc]init];
            NSLog(@"The entire json from the service::::::::::::::::: %@",dict);
            NSArray *arrUser = [dict valueForKey:@"users"];
            if(arrUser.count > 0)
            {
                 NSDictionary *dictUser = [arrUser objectAtIndex:0];
            //    NSLog(@"This is the value of the disctioanry recd from service:::::::::::::: %@",[arrUser objectAtIndex:0]);
                sitter.strUid=[dictUser valueForKey:@"User_ID"];
                sitter.strEmail = [dictUser valueForKey:@"User_Email_Addr"];
                sitter.strFname = [dictUser valueForKey:@"User_Firstname"];
                sitter.strLname = [dictUser valueForKey:@"User_Lastname"];
                sitter.strAddress = [dictUser valueForKey:@"address"];
                sitter.strProfileImage = [dictUser valueForKey:@"profile_image"];
                sitter.strProfileVideo = [dictUser valueForKey:@"profile_video"];
                sitter.strProfileVideoThumb = [dictUser valueForKey:@"video_thumb_path"];
                sitter.strDrivingComplete = [dictUser valueForKey:@"Driving_complete"];
                sitter.strBackfgroundCheck = [dictUser valueForKey:@"background_check_complete"];
            }
            else
            {
                sitter.strUid=@"";
                sitter.strEmail = @"";
                sitter.strFname = @"";
                sitter.strLname = @"";
                sitter.strAddress = @"";
                sitter.strProfileImage = @"";
                sitter.strProfileVideo = @"";
                sitter.strProfileVideoThumb = @"";
                sitter.strDrivingComplete = @"";
                sitter.strBackfgroundCheck = @"";
            }
            NSArray *arrProfile = [dict valueForKey:@"sitter_profile"];
            if(arrProfile.count > 0){
                 NSDictionary *dictProfile = [arrProfile objectAtIndex:0];
                sitter.strAcceptCredit = [dictProfile valueForKey:@"accept_credit_card"];
                sitter.strAcceptCash = [dictProfile valueForKey:@"accept_cash"];
                sitter.strAcceptCheQue = [dictProfile valueForKey:@"accept_personal_checks"];
                sitter.strAcceptPaypal = [dictProfile valueForKey:@"accept_paypal"];
                sitter.strDistanceTravel = [dictProfile valueForKey:@"distence_willing_travel"];
                sitter.strExperience = [dictProfile valueForKey:@"experience_no_years"];
                sitter.strNoOfChild=[dictProfile valueForKey:@"no_of_children_handle"];
                sitter.strLang = [dictProfile valueForKey:@"languages"];
                sitter.strBio = [dictProfile valueForKey:@"bio"];
                sitter.strtransportable = [dictProfile valueForKey:@"transportable"];
                sitter.strNonsmoker = [dictProfile valueForKey:@"non_smoker"];
                sitter.strCareSickkid = [dictProfile valueForKey:@"care_sick_kids"];
                sitter.strComfortPet = [dictProfile valueForKey:@"comfortable_with_pets"];
                sitter.strToddlers = [dictProfile valueForKey:@"tooders"];
                sitter.strearlySchool = [dictProfile valueForKey:@"early_school"];
                sitter.strelementrySchool = [dictProfile valueForKey:@"elementry_school"];
                sitter.strTeens = [dictProfile valueForKey:@"teens"];
                sitter.strTweensOrMultiple = [dictProfile valueForKey:@"tween_or_multiplechilds"];
                sitter.strSpclNeed = [dictProfile valueForKey:@"special_needs"];
                sitter.strSchoolLabel = [dictProfile valueForKey:@"school_lable"];
                sitter.strHighschoolDegree = [dictProfile valueForKey:@"high_school_degree"];
                sitter.strCollegeLabel = [dictProfile valueForKey:@"college_label"];
                sitter.strCollegeDegree = [dictProfile valueForKey:@"college_degree"];
                sitter.strgraduateLabel = [dictProfile valueForKey:@"graduate_label"];
                sitter.strgraduatedegree = [dictProfile valueForKey:@"graduate_degree"];

            }
            else
            {
                sitter.strAcceptCredit = @"";
                sitter.strAcceptCash = @"";
                sitter.strAcceptCheQue = @"";
                sitter.strAcceptPaypal = @"";
                sitter.strDistanceTravel = @"";
                sitter.strExperience = @"";
                sitter.strLang = @"";
                sitter.strBio = @"";
                sitter.strtransportable = @"";
                sitter.strNonsmoker = @"";
                sitter.strCareSickkid = @"";
                sitter.strComfortPet = @"";
                sitter.strToddlers = @"";
                sitter.strearlySchool = @"";
                sitter.strelementrySchool = @"";
                sitter.strTeens = @"";
                sitter.strTweensOrMultiple = @"";
                sitter.strSpclNeed = @"";
                sitter.strSchoolLabel = @"";
                sitter.strHighschoolDegree = @"";
                sitter.strCollegeLabel = @"";
                sitter.strCollegeDegree = @"";
                sitter.strgraduateLabel = @"";
                sitter.strgraduatedegree = @"";
 
            }
            NSArray *arrAvail = [dict valueForKey:@"sitter_available"];
            if(arrAvail.count > 0){
                 NSDictionary *dictavail = [arrAvail objectAtIndex:0];
                sitter.strAvailableText = [dictavail valueForKey:@"available_text"];
                sitter.strAvailableBackUp = [dictavail valueForKey:@"available_backup"];
                sitter.strFullTime = [dictavail valueForKey:@"fulltime"];
                sitter.strNight = [dictavail valueForKey:@"nights"];
                sitter.strWeekend = [dictavail valueForKey:@"weekends"];
                sitter.strBforeSchll = [dictavail valueForKey:@"before_shool"];
                sitter.strafterSchll = [dictavail valueForKey:@"after_school"];
            }
            else
            {
                sitter.strAvailableText = @"";
                sitter.strAvailableBackUp = @"";
                sitter.strFullTime = @"";
                sitter.strNight = @"";
                sitter.strWeekend = @"";
                sitter.strBforeSchll = @"";
                sitter.strafterSchll = @"";

            }
            NSArray *arrQualification = [dict valueForKey:@"additional_qualification"];
            if(arrQualification.count > 0){
                 NSDictionary *dictQualification = [arrQualification objectAtIndex:0];
                sitter.strcertified_teacher = [dictQualification valueForKey:@"certified_teacher"];
                sitter.strcpr_certified = [dictQualification valueForKey:@"cpr_certtified"];
                sitter.strFirstaid_train = [dictQualification valueForKey:@"firstaid_train"];
                sitter.strSpclNeedcare = [dictQualification valueForKey:@"spcl_need_care"];
            }else
            {
                sitter.strcertified_teacher = @"";
                sitter.strcpr_certified = @"";
                sitter.strFirstaid_train = @"";
                sitter.strSpclNeedcare = @"";

            }
            NSArray *arrAdditional = [dict valueForKey:@"additional_services"];
            if(arrAdditional.count > 0){
                NSDictionary *dictAdditional = [arrAdditional objectAtIndex:0];
                sitter.strcertified_teacher = [dictAdditional valueForKey:@"extra_need"];
            }
            else
            {
                sitter.strcertified_teacher = @"";

            }
           
            handler (sitter,nil);
        } onError:^(NSError *error) {
            handler (nil,error);
        }];
    } onError:^(NSError *error) {
        if (error) {
            handler (nil,error);
        }
    }];
    

}
@end
