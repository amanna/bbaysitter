//
//  SitterProfileVC.m
//  BabyParentSitter
//
//  Created by Amrita on 28/05/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import "SitterProfileVC.h"
#import "SitterProfileEditVC.h"
#import "CDSideBarController.h"
#import "SingleCell.h"
#import "SettingsVC.h"
#import "ChatContactListVC.h"
#import "AppServiceManager.h"
#import "POAcvityView.h"
#import "AppServiceManager.h"
#import "UtiltiyManager.h"
#import "AppDelegate.h"
#import "Sitter.h"
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AVFoundation/AVFoundation.h"
#import "PopUpBio.h"
#import "AvailabilityShowEdit.h"
@interface SitterProfileVC ()<UITableViewDataSource,UITableViewDelegate,CDSideBarControllerDelegate>{
    NSArray *nameList;
    POAcvityView *activity;
    AppDelegate *appdelegate;
    Sitter *sitter;
    MPMoviePlayerController *moviePlayerController;
    UIView *frame_TextView;
    UITextView *txtView;
    UIButton *crossButton;
    MPMoviePlayerViewController *mpvc;
    UIView *popUpCheckAvailibility;
    BOOL isStar1Checked;
    BOOL isStar2Checked;
    BOOL isStar3Checked;
    BOOL isStar4Checked;
    BOOL isStar5Checked;
}
- (IBAction)btnVideoPlayAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
- (IBAction)btnEditClickedAction:(id)sender;
//@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
//@property (weak, nonatomic) IBOutlet UILabel *topLabelText;

@property (weak, nonatomic) IBOutlet UIImageView *imgmyProfile;
@property (weak, nonatomic) IBOutlet UIView *viewName;
@property(nonatomic)PopUpBio *popup;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblHtConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *htAdditional;
@property(nonatomic)AvailabilityShowEdit *popupAvail;
@end

@implementation SitterProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UIStoryboard *dashboard = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    self.popup = [dashboard instantiateViewControllerWithIdentifier:@"PopUpBio"];
    self.popupAvail = [dashboard instantiateViewControllerWithIdentifier:@"AvailabilityShowEdit"];
    isStar1Checked=NO;
    isStar2Checked=NO;
    isStar3Checked=NO;
    isStar4Checked=NO;
    isStar5Checked=NO;
    sitter = [[Sitter alloc]init];
    self.viewName.layer.cornerRadius = 10;
    self.viewName.layer.masksToBounds = YES;
    self.arrExperienceRight = [[NSMutableArray alloc]init];
    self.arrEducationRight = [[NSMutableArray alloc]init];
    self.arrQualificationRight = [[NSMutableArray alloc]init];
    appdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@""];
    self.navigationItem.hidesBackButton = YES;
    UIImage *buttonImageRight = [UIImage imageNamed:@"Message"];
    UIButton *buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonRight setImage:buttonImageRight forState:UIControlStateNormal];
    buttonRight.frame = CGRectMake(0, 0, buttonImageRight.size.width, buttonImageRight.size.height);
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:buttonRight];
    self.navigationItem.rightBarButtonItem = rightBarItem;
    
    NSDictionary *attributes=[NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:130.0/255.0 green:36.0/255.0 blue:227.0/255.0 alpha:1],NSForegroundColorAttributeName,[UIFont fontWithName:@"OpenSans-Light" size:20.0],NSFontAttributeName, nil];
    
    self.navigationController.navigationBar.titleTextAttributes = attributes;
    self.navigationItem.title = @"Profile";
    NSArray *imageList = @[[UIImage imageNamed:@"john"], [UIImage imageNamed:@"find"], [UIImage imageNamed:@"payment"], [UIImage imageNamed:@"settings"],[UIImage imageNamed:@"chat"]];
    nameList = @[@"John Doe", @"Find Jobs", @"Payments",@"My Settings", @"Messages"];
    
    
    
//    sideBar = [[CDSideBarController alloc] initWithImages:imageList labelName:nameList];
//    sideBar.delegate = self;
    

       // Do any additional setup after loading the view.
    //Tableview rounded corner
    self.tblpreference.layer.cornerRadius = 5;
    self.tblEducation.layer.cornerRadius = 5;
    self.tblExperience.layer.cornerRadius = 5;
    self.tblLang.layer.cornerRadius = 5;
    self.tblqualfication.layer.cornerRadius = 5;
    
    //Dunamic height for scroll
    
    self.htScShareFrend.constant = 0;
    self.htShareLabel.constant = 0;
    self.htreview.constant = 0;
    self.constAspect.constant = 0;
    
    //scrollView
    NSUInteger i;
    int xCoord=0;
    int yCoord=5;
    int buttonWidth=100;
    int buttonHeight=self.scrllAdditional.frame.size.height;
    int buffer = 10;
    for (i = 1; i <= 5; i++)
    {
        UIButton *aButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aButton.tag = 2300 + i;
        aButton.frame     = CGRectMake(xCoord, yCoord,buttonWidth,buttonHeight );
        //Add heart image to btn
        
        UIImageView *imgHeart = [[UIImageView alloc]initWithFrame:CGRectMake(24,(self.scrllAdditional.frame.size.height - 49)/2 - 15 ,49 , 49)];
        imgHeart.image =[UIImage imageNamed:@"heart"];
        [aButton addSubview:imgHeart];
        
        UILabel * lblHeart = [[UILabel alloc]initWithFrame:CGRectMake(0, imgHeart.frame.origin.y+ imgHeart.frame.size.height + 10, buttonWidth, 21)];
        lblHeart.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        lblHeart.text = @"Date Nights";
        lblHeart.textColor = [UIColor darkGrayColor];
        lblHeart.textAlignment = NSTextAlignmentCenter;
        [aButton addSubview:lblHeart];
        
        
        [self.scrllAdditional addSubview:aButton];
        xCoord += buttonWidth + buffer;
    }
    [self.scrllAdditional setContentSize:CGSizeMake(700, yCoord)];
    
 if([UtiltiyManager isReachableToInternet]){
      [activity showView];
      [AppServiceManager getSitterProfile:appdelegate.strUserId onCompletion:^(id object, NSError *error)
     {
           [activity hideView];
           if(object){
               sitter = object;
//Logic written by Gaurav
               

//               if ([sitter.strBackfgroundCheck isEqualToString:@"1"])
//               {
//                   [_btnBgCheck setBackgroundImage:[UIImage imageNamed:@"bgshieldbutton"] forState:UIControlStateNormal];
//                   
//               }

               //Display UI
               if (sitter.strProfileImage.length>0)
               {
                   dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                       
                       NSURL *url = [NSURL URLWithString:sitter.strProfileImage];
                       
                       NSData *data = [NSData dataWithContentsOfURL:url];
                       UIImage *image = [UIImage imageWithData:data];
                       self.imgmyProfile.image = image;
                       self.imgmyProfile.layer.cornerRadius = 10;
                       self.imgmyProfile.layer.masksToBounds = YES;
                       
                   });
               }
               if([sitter.strBio isEqualToString:@""]){
                   self.lblHtConstraint.constant = 0;
                   self.htAdditional.constant = 0;
               }
               
               self.lblNickName.text = [NSString stringWithFormat:@"%@ %@",sitter.strFname,sitter.strLname];
               
               self.lblExperience.text = [NSString stringWithFormat:@"%@ Years Experience",sitter.strExperience];
               
               self.lblAge.text = sitter.strAddress;
               
               self.lblAbout.text = sitter.strFname;
               
               self.arrPreference = [[NSMutableArray alloc]initWithObjects:@"Willing to travel",@"Comfortable caring for",@"Has own transporation",@"Accepts credit cards",@"Accepts Cash",@"Accepts Paypal",@"Accept Personal Cheque",@"Non smoker",@"Will care for sick kids", @"Before school",@"After school",@"Weekend babysitting",@"Full Time",@"Date Nights",@"Available For backup",@"Comfortable with pets",nil];
               
               self.arrExperience = [[NSMutableArray alloc]initWithObjects:@"Toddlers (7 mon-3 years)",@"Early School age(4-6 years)",@"Elementry School(7-11 years)",@"Pre-Teens/Teenagers(12- 15+years)",@"Special Needs", @"Twins/Multipules",nil];
               
               
               self.arrqualfication = [[NSMutableArray alloc]initWithObjects:@"Certified Teacher",@"CPR Certified",@"First Aid Training",@"Special Need care",nil];
               self.arrEducation = [[NSMutableArray alloc]initWithObjects:@"Some High School",@"High School degree",@"Some College",@"College Degree",@"Some graduate School",@"Graduate Degree",nil];
               self.arrLang = [[NSMutableArray alloc]initWithObjects:@"English", @"French",nil];

             //Add value to preference array
               
               @try {
                   self.arrPreferenceRight = [[NSMutableArray alloc]initWithObjects:sitter.strDistanceTravel,sitter.strComfortPet,sitter.strtransportable, sitter.strAcceptCredit,sitter.strAcceptCash,sitter.strAcceptPaypal,sitter.strAcceptCheQue,sitter.strNonsmoker,sitter.strCareSickkid,sitter.strBforeSchll,sitter.strafterSchll,sitter.strWeekend,sitter.strFullTime,sitter.strNight,sitter.strAvailableBackUp,sitter.strComfortPet,nil];

               }
               @catch (NSException *exception)
               {
                   NSLog(@"This is the reason for exception:::::::::::::::%@",exception.description);
               }
              
            

               self.arrEducationRight=[[NSMutableArray alloc] initWithObjects:sitter.strSchoolLabel, sitter.strHighschoolDegree,sitter.strCollegeLabel,sitter.strCollegeDegree,sitter.strgraduateLabel,sitter.strgraduatedegree,nil];
              
             self.arrExperienceRight = [[NSMutableArray alloc]initWithObjects:sitter.strToddlers,sitter.strearlySchool,sitter.strelementrySchool,sitter.strTeens ,sitter.strSpclNeed,sitter.strTweensOrMultiple,nil];
               
               
               self.arrQualificationRight=[[NSMutableArray alloc]initWithObjects:sitter.strcertified_teacher,sitter.strcpr_certified,sitter.strFirstaid_train,sitter.strSpclNeedcare, nil];
               
               
               [self.tblEducation reloadData];
               [self.tblExperience reloadData];
               [self.tblLang reloadData];
               [self.tblqualfication reloadData];
               [self.tblpreference reloadData];
           }else{
               [UtiltiyManager showAlertWithMessage:@"Some Problem Occurs!!!please try again later"];
           }
       }];
    }else{
        [UtiltiyManager showAlertWithMessage:@"NO Internet connection"];
    }
    
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark-Data source method of table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag==100){
        return self.arrPreference.count;
    }else if (tableView.tag==101){
        return self.arrExperience.count;
    }else if (tableView.tag==102){
        return self.arrLang.count;
    }else if (tableView.tag==103){
        return self.arrqualfication.count;
    }else{
        return self.arrEducation.count;
    }
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
//[{"User_Email_Addr":"ww@jj.lkk","User_Firstname":"vcvcv","User_Lastname":"vccv","User_DOB":"2015-07-07","User_Gender":"male","address":"","User_ID":"10","accept_credit_card":"1","accept_cash":"0","accept_paypal":"1","distence_willing_travel":"25.00","experience_no_years":"0.00","no_of_children_handle":"3","education":"","languages":"","bio":"","accept_personal_checks":"0","transportable":"1","non_smoker":"1","care_sick_kids":"0","comfortable_with_pets":"0","tooders":"0","early_school":"0","elementry_school":"0","teens":"0","tween_or_multiplechilds":"0","special_needs":"0","school_lable":"0","high_school_degree":"0","college_label":"0","college_degree":"0","graduate_label":"0","graduate_degree":"0","available_text":"5019,5016,5030,5039,5041","available_backup":"0","fulltime":"1","nights":"1","weekends":"0","before_shool":"0","after_school":"0"}]


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag==100){
         SingleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
         cell.lblLeft.text =[self.arrPreference objectAtIndex:indexPath.row];
         if(indexPath.row ==0){
            cell.lblRight.text = sitter.strDistanceTravel;
             
         }else{
             cell.lblRight.text = @"";
             if([[self.arrPreferenceRight objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
             }else{
                cell.accessoryType = UITableViewCellAccessoryNone;
             }
             
         }
         cell.tintColor = [UIColor colorWithRed:128.0/255.0 green:255.0/255.0 blue:0.0/255.0 alpha:1];
         return cell;
    }else if (tableView.tag==101){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrExperience objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:128.0/255.0 green:255.0/255.0 blue:0.0/255.0 alpha:1];
        if([[self.arrExperienceRight objectAtIndex:indexPath.row] isEqualToString:@"1"]){
             cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
             cell.accessoryType = UITableViewCellAccessoryNone;
        }
       
        return cell;
    }else if (tableView.tag==102)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrLang objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:128.0/255.0 green:255.0/255.0 blue:0.0/255.0 alpha:1];
        if ([sitter.strLang isEqualToString:cell.textLabel.text])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;

        }
        return cell;
    }else if (tableView.tag==103){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrqualfication objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:128.0/255.0 green:255.0/255.0 blue:0.0/255.0 alpha:1];
        if ([[self.arrQualificationRight objectAtIndex:indexPath.row] isEqualToString:@"1"])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;

        }
        return cell;
    }else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        cell.textLabel.text = [self.arrEducation objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"OpenSans-Light" size:17.0];
        cell.tintColor = [UIColor colorWithRed:128.0/255.0 green:255.0/255.0 blue:0.0/255.0 alpha:1];
        
        if([[self.arrEducationRight objectAtIndex:indexPath.row]isEqualToString:@"1"])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        return cell;
    }
    
}
- (IBAction)btnEditClickedAction:(id)sender
{
    [self performSegueWithIdentifier:@"segueEdit" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue: %@", segue.identifier);
    
    @try {
        if ([segue.identifier isEqualToString:@"segueEdit"]){
            SitterProfileEditVC *rs =segue.destinationViewController;
            rs.sitter = sitter;
        }
    }
    @catch (NSException *exception)
    {
        
        NSLog(@"This is the exception reason:::::::::::%@",exception.description);
        
    }
    
    
    ///segueEdit
    
//    if ([segue.identifier isEqualToString:@"segueEdit"])
//    {
//                SitterProfileEditVC *rs =segue.destinationViewController;
//               rs.sitter = sitter;
//         }
    
}

- (IBAction)btnReadMoreClickedAction:(id)sender
{
    [self.view addSubview:self.popup.view];
    [self.popup refreshUIWithDatasource:nil onCompletion:^(id object, NSUInteger eventPop) {
        if(eventPop==kActionClose){
            [self.popup.view removeFromSuperview];
            
        }
    }];

}

- (IBAction)viewAvailbilityAction:(id)sender
{
    
    [self.view addSubview:self.popupAvail.view];
    [self.popupAvail refreshUIWithDatasource:nil onCompletion:^(id object, NSUInteger eventPop) {
        if(eventPop==kActionSave){
            [self.popupAvail.view removeFromSuperview];
        }else{
            [self.popup.view removeFromSuperview];
        }
    }];
    
    
}
-(void)removeTextView
{
    //crossButton
    if (popUpCheckAvailibility)
    {
        [popUpCheckAvailibility removeFromSuperview];
 
    }
    [crossButton removeFromSuperview];
    [frame_TextView removeFromSuperview];
    [txtView removeFromSuperview];
    
}
#pragma mark - CDSideBarController delegate

- (void)menuButtonClicked:(int)index
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Dashboard" bundle:nil];
    if(index==1){
        //self.segmentedControl.selectedSegmentIndex=1;
    }else if (index==2)
    {
        
    }else if (index==0){
        SitterProfileVC *dashboard = [story instantiateViewControllerWithIdentifier:@"SitterProfileVC"];
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
    }else if (index==3){
        SettingsVC *dashboard = [story instantiateViewControllerWithIdentifier:@"SettingsVC"];
        dashboard.strUid = self.strUid;
        dashboard.strUserType = self.strUserType;
        [self showViewController:dashboard sender:self];
    }else{
        ChatContactListVC *dashboard = [story instantiateViewControllerWithIdentifier:@"ChatContactListVC"];
        dashboard.strUserType = self.strUserType;
        dashboard.strUid = self.strUid;
        [self showViewController:dashboard sender:self];
    }

    
}
#pragma mark-MEthod for video playing

- (IBAction)btnVideoPlayAction:(id)sender
{
    NSLog(@"This is the world of videos:::::::::::::");
    if ([sitter.strProfileVideo isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"No video available at the moment." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    
    NSURL *fileURL = [NSURL URLWithString:@"http://www.efriendz.in/Anirbansite/uploads/image/myVideo11nN76.mov"];
    mpvc= [[MPMoviePlayerViewController alloc] initWithContentURL:fileURL];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object:[mpvc moviePlayer]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(movieFinishedCallback:) name:MPMoviePlayerDidExitFullscreenNotification object:[mpvc moviePlayer]];
    [self presentMoviePlayerViewControllerAnimated:mpvc];
   }
- (IBAction)clickBackgroundCheck:(id)sender
{
    
}

-(void)movieFinishedCallback:(NSNotification*)aNotification
{
    NSLog(@"MOview ended.");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayerController ];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:moviePlayerController];
    MPMoviePlayerController *player = [aNotification object];
    [player stop];
}
- (IBAction)starclick:(id)sender
{
    
    NSLog(@"%ld",(long)_btnStar1.tag);

    if (!isStar1Checked)
    {
        [_btnStar1 setBackgroundImage:[UIImage imageNamed:@"fullstar"] forState:UIControlStateNormal];
        isStar1Checked=YES;
        return;
    }
    else
    {
        [_btnStar1 setBackgroundImage:[UIImage imageNamed:@"emptystar"] forState:UIControlStateNormal];
        isStar1Checked=NO;
      

    }
}
- (IBAction)starClick2:(id)sender
{
    if (isStar1Checked==YES)
    {
        if (!isStar2Checked)
        {
            [_btnStar2 setBackgroundImage:[UIImage imageNamed:@"fullstar"] forState:UIControlStateNormal];
            isStar2Checked=YES;
            return;
            
        }
        else
        {
            [_btnStar2 setBackgroundImage:[UIImage imageNamed:@"emptystar"] forState:UIControlStateNormal];
            isStar2Checked=NO;
            
        }

    }
   
}

- (IBAction)starClick3:(id)sender
{
    if (isStar1Checked==YES && isStar2Checked==YES)
    {
        if (!isStar3Checked)
        {
            [_btnStar3 setBackgroundImage:[UIImage imageNamed:@"fullstar"] forState:UIControlStateNormal];
            isStar3Checked=YES;
            return;
            
        }
        else
        {
            [_btnStar3 setBackgroundImage:[UIImage imageNamed:@"emptystar"] forState:UIControlStateNormal];
            isStar3Checked=NO;
            
        }

    }
  
}

- (IBAction)starClick4:(id)sender
{
    if (isStar1Checked==YES && isStar2Checked==YES && isStar3Checked==YES )
    {
        if (!isStar4Checked)
        {
            [_star4 setBackgroundImage:[UIImage imageNamed:@"fullstar"] forState:UIControlStateNormal];
            isStar4Checked=YES;
            return;
            
        }
        else
        {
            [_star4 setBackgroundImage:[UIImage imageNamed:@"emptystar"] forState:UIControlStateNormal];
            isStar4Checked=NO;
            
        }
    }
   

}

- (IBAction)starClick5:(id)sender {
    if (isStar1Checked==YES && isStar2Checked==YES && isStar3Checked==YES &&isStar4Checked==YES)
    {
        if (!isStar5Checked)
        {
            [_btnStar5 setBackgroundImage:[UIImage imageNamed:@"fullstar"] forState:UIControlStateNormal];
            isStar5Checked=YES;
            return;
            
            
        }
        else
        {
            [_btnStar5 setBackgroundImage:[UIImage imageNamed:@"emptystar"] forState:UIControlStateNormal];
            isStar5Checked=NO;
        }

    }
 }
@end
