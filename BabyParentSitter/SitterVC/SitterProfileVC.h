//
//  SitterProfileVC.h
//  BabyParentSitter
//
//  Created by Amrita on 28/05/15.
//  Copyright (c) 2015 Rehabdigital. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SitterProfileVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblpreference;
@property (weak, nonatomic) IBOutlet UITableView *tblExperience;
@property (weak, nonatomic) IBOutlet UITableView *tblLang;
@property (weak, nonatomic) IBOutlet UITableView *tblqualfication;
@property (weak, nonatomic) IBOutlet UITableView *tblEducation;
@property(nonatomic,strong)NSMutableArray *arrPreference;
@property(nonatomic,strong)NSMutableArray *arrPreferenceRight;
@property(nonatomic,strong)NSMutableArray *arrExperience;
@property(nonatomic,strong)NSMutableArray *arrExperienceRight;
@property(nonatomic,strong)NSMutableArray *arrQualificationRight;
@property(nonatomic,strong)NSMutableArray *arrLang;
@property(nonatomic,strong)NSMutableArray *arrqualfication;
@property(nonatomic,strong)NSMutableArray *arrEducation;
@property(nonatomic,strong)NSMutableArray *arrEducationRight;
- (IBAction)btnReadMoreClickedAction:(id)sender;
- (IBAction)viewAvailbilityAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollShareFrend;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *htScShareFrend;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *htShareLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *htreview;
@property (weak, nonatomic) IBOutlet UIScrollView *scrllAdditional;
@property (weak, nonatomic) IBOutlet UILabel *lblNickName;
@property (weak, nonatomic) IBOutlet UIView *viewRating;
@property (weak, nonatomic) IBOutlet UILabel *lblExperience;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;

@property (weak, nonatomic) IBOutlet UILabel *lblAbout;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constAspect;

@property(nonatomic)NSString *strUid;
@property(nonatomic)NSString *strUserType;
- (IBAction)clickBackgroundCheck:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnBgCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnStar1;
- (IBAction)starclick:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnStar2;
@property (weak, nonatomic) IBOutlet UIButton *btnStar3;
@property (weak, nonatomic) IBOutlet UIButton *star4;
@property (weak, nonatomic) IBOutlet UIButton *btnStar5;

- (IBAction)starClick2:(id)sender;
- (IBAction)starClick3:(id)sender;
- (IBAction)starClick4:(id)sender;
- (IBAction)starClick5:(id)sender;





@end
